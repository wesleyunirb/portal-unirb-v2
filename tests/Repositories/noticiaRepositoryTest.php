<?php namespace Tests\Repositories;

use App\Models\noticia;
use App\Repositories\noticiaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class noticiaRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var noticiaRepository
     */
    protected $noticiaRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->noticiaRepo = \App::make(noticiaRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_noticia()
    {
        $noticia = factory(noticia::class)->make()->toArray();

        $creatednoticia = $this->noticiaRepo->create($noticia);

        $creatednoticia = $creatednoticia->toArray();
        $this->assertArrayHasKey('id', $creatednoticia);
        $this->assertNotNull($creatednoticia['id'], 'Created noticia must have id specified');
        $this->assertNotNull(noticia::find($creatednoticia['id']), 'noticia with given id must be in DB');
        $this->assertModelData($noticia, $creatednoticia);
    }

    /**
     * @test read
     */
    public function test_read_noticia()
    {
        $noticia = factory(noticia::class)->create();

        $dbnoticia = $this->noticiaRepo->find($noticia->id);

        $dbnoticia = $dbnoticia->toArray();
        $this->assertModelData($noticia->toArray(), $dbnoticia);
    }

    /**
     * @test update
     */
    public function test_update_noticia()
    {
        $noticia = factory(noticia::class)->create();
        $fakenoticia = factory(noticia::class)->make()->toArray();

        $updatednoticia = $this->noticiaRepo->update($fakenoticia, $noticia->id);

        $this->assertModelData($fakenoticia, $updatednoticia->toArray());
        $dbnoticia = $this->noticiaRepo->find($noticia->id);
        $this->assertModelData($fakenoticia, $dbnoticia->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_noticia()
    {
        $noticia = factory(noticia::class)->create();

        $resp = $this->noticiaRepo->delete($noticia->id);

        $this->assertTrue($resp);
        $this->assertNull(noticia::find($noticia->id), 'noticia should not exist in DB');
    }
}
