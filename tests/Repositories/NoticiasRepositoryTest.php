<?php namespace Tests\Repositories;

use App\Models\Noticias;
use App\Repositories\NoticiasRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class NoticiasRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var NoticiasRepository
     */
    protected $noticiasRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->noticiasRepo = \App::make(NoticiasRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_noticias()
    {
        $noticias = factory(Noticias::class)->make()->toArray();

        $createdNoticias = $this->noticiasRepo->create($noticias);

        $createdNoticias = $createdNoticias->toArray();
        $this->assertArrayHasKey('id', $createdNoticias);
        $this->assertNotNull($createdNoticias['id'], 'Created Noticias must have id specified');
        $this->assertNotNull(Noticias::find($createdNoticias['id']), 'Noticias with given id must be in DB');
        $this->assertModelData($noticias, $createdNoticias);
    }

    /**
     * @test read
     */
    public function test_read_noticias()
    {
        $noticias = factory(Noticias::class)->create();

        $dbNoticias = $this->noticiasRepo->find($noticias->id);

        $dbNoticias = $dbNoticias->toArray();
        $this->assertModelData($noticias->toArray(), $dbNoticias);
    }

    /**
     * @test update
     */
    public function test_update_noticias()
    {
        $noticias = factory(Noticias::class)->create();
        $fakeNoticias = factory(Noticias::class)->make()->toArray();

        $updatedNoticias = $this->noticiasRepo->update($fakeNoticias, $noticias->id);

        $this->assertModelData($fakeNoticias, $updatedNoticias->toArray());
        $dbNoticias = $this->noticiasRepo->find($noticias->id);
        $this->assertModelData($fakeNoticias, $dbNoticias->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_noticias()
    {
        $noticias = factory(Noticias::class)->create();

        $resp = $this->noticiasRepo->delete($noticias->id);

        $this->assertTrue($resp);
        $this->assertNull(Noticias::find($noticias->id), 'Noticias should not exist in DB');
    }
}
