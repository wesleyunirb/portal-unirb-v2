<?php namespace Tests\Repositories;

use App\Models\Unidade;
use App\Repositories\UnidadeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class UnidadeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var UnidadeRepository
     */
    protected $unidadeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->unidadeRepo = \App::make(UnidadeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_unidade()
    {
        $unidade = factory(Unidade::class)->make()->toArray();

        $createdUnidade = $this->unidadeRepo->create($unidade);

        $createdUnidade = $createdUnidade->toArray();
        $this->assertArrayHasKey('id', $createdUnidade);
        $this->assertNotNull($createdUnidade['id'], 'Created Unidade must have id specified');
        $this->assertNotNull(Unidade::find($createdUnidade['id']), 'Unidade with given id must be in DB');
        $this->assertModelData($unidade, $createdUnidade);
    }

    /**
     * @test read
     */
    public function test_read_unidade()
    {
        $unidade = factory(Unidade::class)->create();

        $dbUnidade = $this->unidadeRepo->find($unidade->id);

        $dbUnidade = $dbUnidade->toArray();
        $this->assertModelData($unidade->toArray(), $dbUnidade);
    }

    /**
     * @test update
     */
    public function test_update_unidade()
    {
        $unidade = factory(Unidade::class)->create();
        $fakeUnidade = factory(Unidade::class)->make()->toArray();

        $updatedUnidade = $this->unidadeRepo->update($fakeUnidade, $unidade->id);

        $this->assertModelData($fakeUnidade, $updatedUnidade->toArray());
        $dbUnidade = $this->unidadeRepo->find($unidade->id);
        $this->assertModelData($fakeUnidade, $dbUnidade->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_unidade()
    {
        $unidade = factory(Unidade::class)->create();

        $resp = $this->unidadeRepo->delete($unidade->id);

        $this->assertTrue($resp);
        $this->assertNull(Unidade::find($unidade->id), 'Unidade should not exist in DB');
    }
}
