<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Unidade;

class UnidadeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_unidade()
    {
        $unidade = factory(Unidade::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/unidades', $unidade
        );

        $this->assertApiResponse($unidade);
    }

    /**
     * @test
     */
    public function test_read_unidade()
    {
        $unidade = factory(Unidade::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/unidades/'.$unidade->id
        );

        $this->assertApiResponse($unidade->toArray());
    }

    /**
     * @test
     */
    public function test_update_unidade()
    {
        $unidade = factory(Unidade::class)->create();
        $editedUnidade = factory(Unidade::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/unidades/'.$unidade->id,
            $editedUnidade
        );

        $this->assertApiResponse($editedUnidade);
    }

    /**
     * @test
     */
    public function test_delete_unidade()
    {
        $unidade = factory(Unidade::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/unidades/'.$unidade->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/unidades/'.$unidade->id
        );

        $this->response->assertStatus(404);
    }
}
