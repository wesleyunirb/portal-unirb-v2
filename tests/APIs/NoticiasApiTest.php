<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Noticias;

class NoticiasApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_noticias()
    {
        $noticias = factory(Noticias::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/noticias', $noticias
        );

        $this->assertApiResponse($noticias);
    }

    /**
     * @test
     */
    public function test_read_noticias()
    {
        $noticias = factory(Noticias::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/noticias/'.$noticias->id
        );

        $this->assertApiResponse($noticias->toArray());
    }

    /**
     * @test
     */
    public function test_update_noticias()
    {
        $noticias = factory(Noticias::class)->create();
        $editedNoticias = factory(Noticias::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/noticias/'.$noticias->id,
            $editedNoticias
        );

        $this->assertApiResponse($editedNoticias);
    }

    /**
     * @test
     */
    public function test_delete_noticias()
    {
        $noticias = factory(Noticias::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/noticias/'.$noticias->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/noticias/'.$noticias->id
        );

        $this->response->assertStatus(404);
    }
}
