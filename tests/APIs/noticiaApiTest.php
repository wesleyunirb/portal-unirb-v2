<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\noticia;

class noticiaApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_noticia()
    {
        $noticia = factory(noticia::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/noticias', $noticia
        );

        $this->assertApiResponse($noticia);
    }

    /**
     * @test
     */
    public function test_read_noticia()
    {
        $noticia = factory(noticia::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/noticias/'.$noticia->id
        );

        $this->assertApiResponse($noticia->toArray());
    }

    /**
     * @test
     */
    public function test_update_noticia()
    {
        $noticia = factory(noticia::class)->create();
        $editednoticia = factory(noticia::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/noticias/'.$noticia->id,
            $editednoticia
        );

        $this->assertApiResponse($editednoticia);
    }

    /**
     * @test
     */
    public function test_delete_noticia()
    {
        $noticia = factory(noticia::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/noticias/'.$noticia->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/noticias/'.$noticia->id
        );

        $this->response->assertStatus(404);
    }
}
