<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('intro');
});


Route::get('vestibular', function () {
    return view('vestibular');
});

Route::get('creditoestudantil', function () {
    return view('creditoestudantil');
});

Route::get('estagio', function () {
    return view('estagio');
});

Route::get('intercambio', function () {
    return view('intercambio');
});

Route::get('convenios', function () {
    return view('convenios');
});

Route::get('laboratorios', function () {
    return view('laboratorios');
});

Route::get('biblioteca', function () {
    return view('biblioteca');
});

Route::get('linhadotempo', function () {
    return view('linhadotempo');
});

Route::get('posgraduacao', function () {
    return view('posgraduacao');
});

Route::get('ouvidoria', function () {
    return view('ouvidoria');
});

Route::get('graduacao', function () {
    return view('graduacao');
});

Route::get('unidade', function () {
    return view('unidade');
});

Route::get('intro', function () {
    return view('intro');
});


Route::get('egresso', function () {
    return view('egresso');
});



//EMITIR PDF INSCRITOS
Route::any('relatorioinscritos', 'RelatorioController@gerarelatorio')->name('relatorio.relatoriopdfinscrito');
Route::any('relatorio','RelatorioController@relatorioinscrito')->name('relatorio.relatorioinscrito');

//EMITIR PDF LEADS
Route::any('relatorioleadspdf', 'RelatorioLeadsController@gerarelatorioleads')->name('relatorio.relatoriopdfleads');
Route::any('relatorioleads','RelatorioLeadsController@relatorioleads')->name('relatorio.relatoriolead');


Route::post('enviaremail1','MailController@store')->name('mail.store1');
Route::post('enviaremail2','MailController@store2')->name('mail.store2');
Route::post('enviaremail3','MailController@store3')->name('mail.store3');

Auth::routes();
Route::get('/home', 'HomeController@index');
Route::resource('noticias', 'noticiaController');

Route::get('users/{id}/delete', 'UsersController@forceDelete');


//ROTAS BANCO/VIEW HOME
Route::get('principal','IndexController@noticia');

Route::get('upload', function () {
    return view('upload');
});


Route::get('noticia','noticiaController@noticia1');

//ROTAS BANCO/VIEW NOTICIA NOTICIA
Route::get('noticia1','noticiaController@noticia1');
Route::get('noticia2','noticiaController@noticia2');
Route::get('noticia3','noticiaController@noticia3');
Route::get('noticia4','noticiaController@noticia4');
Route::get('noticia5','noticiaController@noticia5');
Route::get('noticia6','noticiaController@noticia6');

//----------UNIDADES---------------//

//unidade2
Route::get('Alagoinhas','UnidadeAlagoinhasController@unidade');

//unidade3
Route::get('Aracaju','UnidadeAracajuController@unidade');

//unidade4
Route::get('FBTAracaju','UnidadeFBTAracajuController@unidade');

//unidade5
Route::get('Arapiraca','UnidadeArapiracaController@unidade');

//unidade6
Route::get('Barreiras','UnidadeBarreirasController@unidade');

//unidade7
Route::get('Camacari','UnidadeCamacariController@unidade');

//unidade8
Route::get('Fortaleza','UnidadeFortalezaController@unidade');

//unidade9
Route::get('Feiradesantana','UnidadeFeiradesantanaController@unidade');

//unidade10
Route::get('FBTFeiradesantana','UnidadeFBTFeiradesantanaController@unidade');

//unidade11
Route::get('Juazeiro','UnidadeJuazeiroController@unidade');

//unidade12
Route::get('Maceio','UnidadeMaceioController@unidade');

//unidade13
Route::get('Mossoro','UnidadeMossoroController@unidade');

//unidade14
Route::get('Natal','UnidadeNatalController@unidade');

//unidade15
Route::get('Parnaiba','UnidadeParnaibaController@unidade');

//unidade16
Route::get('Salvador','UnidadeSalvadorController@unidade');

//unidade17
Route::get('Teresina','UnidadeTeresinaController@unidade');





//UPLOAD DE ARQUIVOS PARA O LOCAL STORAGE
Route::post('upload','UploadController@upload');


//ROTA ENVIO FORM INSCRIÇÃO EVENTO
Route::post('inscricao', 'InscricaoController@save')->name('inscricao.save');

//INSTITUCIONAL

Route::any('institucional','InstitucionalController@unidadeCurso')->name('unicurso.show');



Auth::routes();

Route::resource('users', 'usersController');

Route::resource('unidades', 'unidadeController');

Route::resource('unidades', 'unidadeController');

Route::resource('unidades', 'unidadeController');

Route::resource('unidades', 'unidadesController');

Route::resource('noticias', 'noticiasController');

Route::resource('cursos', 'cursoController');

Route::resource('tipoCursos', 'tipo_cursoController');

Route::resource('eventos', 'eventosController');

Route::resource('tipoEventos', 'tipo_eventoController');

Route::resource('banners', 'bannersController');

Route::resource('galerias', 'galeriaController');

Route::resource('leads', 'leadsController');

Route::resource('tipoCursos', 'tipo_cursosController');

Route::resource('tipoEventos', 'tipo_eventosController');

Route::resource('inscritos', 'inscritosController');

Route::resource('campanhas', 'campanhaController');