<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $fillable = [
        'titulo', 'local', 'descricao','data_evento','horario','limite_participantes','data_inicio','data_fim'
    ];
}
