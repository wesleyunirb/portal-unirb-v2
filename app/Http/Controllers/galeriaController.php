<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreategaleriaRequest;
use App\Http\Requests\UpdategaleriaRequest;
use App\Repositories\galeriaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Unidade; 


class galeriaController extends AppBaseController
{
    /** @var  galeriaRepository */
    private $galeriaRepository;

    public function __construct(galeriaRepository $galeriaRepo)
    {
        $this->galeriaRepository = $galeriaRepo;
    }

    /**
     * Display a listing of the galeria.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $galerias = $this->galeriaRepository->all();

        return view('galerias.index')
            ->with('galerias', $galerias);
    }

    /**
     * Show the form for creating a new galeria.
     *
     * @return Response
     */
    public function create()
    {
        $unidades = Unidade::all();
        

        if(!empty($unidades)){

            return view('galerias.create', compact('unidades'))->with('unidades', $unidades);

        }else{

            return view('galerias.create', compact('unidades'))->with('unidades', $unidades);

        } 
    }

    /**
     * Store a newly created galeria in storage.
     *
     * @param CreategaleriaRequest $request
     *
     * @return Response
     */
    public function store(CreategaleriaRequest $request)
    {
        $input = $request->all();

        $galeria = $this->galeriaRepository->create($input);

        Flash::success('Imagem salva com sucesso .');

        return redirect(route('galerias.index'));
    }

    /**
     * Display the specified galeria.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $galeria = $this->galeriaRepository->find($id);

        if (empty($galeria)) {
            Flash::error('Imagem não encontrada');

            return redirect(route('galerias.index'));
        }

        return view('galerias.show')->with('galeria', $galeria);
    }

    /**
     * Show the form for editing the specified galeria.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $galeria = $this->galeriaRepository->find($id);
        $unidades = Unidade::all();


        if (empty($galeria)) {
            Flash::error('Imagem não encontrada');

            return redirect(route('galerias.index'));
        }

        return view('galerias.edit', compact('unidades','galeria'))->with('unidades', $unidades)->with('galeria', $galeria);
    }

    /**
     * Update the specified galeria in storage.
     *
     * @param int $id
     * @param UpdategaleriaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdategaleriaRequest $request)
    {
        $galeria = $this->galeriaRepository->find($id);

        if (empty($galeria)) {
            Flash::error('Imagem não encontrada');

            return redirect(route('galerias.index'));
        }

        $galeria = $this->galeriaRepository->update($request->all(), $id);

        Flash::success('Galeria atualizada com sucesso.');

        return redirect(route('galerias.index'));
    }

    /**
     * Remove the specified galeria from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $galeria = $this->galeriaRepository->find($id);

        if (empty($galeria)) {
            Flash::error('Imagem não encontrada');

            return redirect(route('galerias.index'));
        }

        $this->galeriaRepository->delete($id);

        Flash::success('Imagem excluida com sucesso.');

        return redirect(route('galerias.index'));
    }
}
