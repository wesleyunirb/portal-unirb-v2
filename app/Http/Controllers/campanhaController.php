<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecampanhaRequest;
use App\Http\Requests\UpdatecampanhaRequest;
use App\Repositories\campanhaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class campanhaController extends AppBaseController
{
    /** @var  campanhaRepository */
    private $campanhaRepository;

    public function __construct(campanhaRepository $campanhaRepo)
    {
        $this->campanhaRepository = $campanhaRepo;
    }

    /**
     * Display a listing of the campanha.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $campanhas = $this->campanhaRepository->all();

        return view('campanhas.index')
            ->with('campanhas', $campanhas);
    }

    /**
     * Show the form for creating a new campanha.
     *
     * @return Response
     */
    public function create()
    {
        return view('campanhas.create');
    }

    /**
     * Store a newly created campanha in storage.
     *
     * @param CreatecampanhaRequest $request
     *
     * @return Response
     */
    public function store(CreatecampanhaRequest $request)
    {
        $input = $request->all();

        $campanha = $this->campanhaRepository->create($input);

        Flash::success('Campanha saved successfully.');

        return redirect(route('campanhas.index'));
    }

    /**
     * Display the specified campanha.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $campanha = $this->campanhaRepository->find($id);

        if (empty($campanha)) {
            Flash::error('Campanha not found');

            return redirect(route('campanhas.index'));
        }

        return view('campanhas.show')->with('campanha', $campanha);
    }

    /**
     * Show the form for editing the specified campanha.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $campanha = $this->campanhaRepository->find($id);

        if (empty($campanha)) {
            Flash::error('Campanha not found');

            return redirect(route('campanhas.index'));
        }

        return view('campanhas.edit')->with('campanha', $campanha);
    }

    /**
     * Update the specified campanha in storage.
     *
     * @param int $id
     * @param UpdatecampanhaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecampanhaRequest $request)
    {
        $campanha = $this->campanhaRepository->find($id);

        if (empty($campanha)) {
            Flash::error('Campanha not found');

            return redirect(route('campanhas.index'));
        }

        $campanha = $this->campanhaRepository->update($request->all(), $id);

        Flash::success('Campanha updated successfully.');

        return redirect(route('campanhas.index'));
    }

    /**
     * Remove the specified campanha from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $campanha = $this->campanhaRepository->find($id);

        if (empty($campanha)) {
            Flash::error('Campanha not found');

            return redirect(route('campanhas.index'));
        }

        $this->campanhaRepository->delete($id);

        Flash::success('Campanha deleted successfully.');

        return redirect(route('campanhas.index'));
    }
}
