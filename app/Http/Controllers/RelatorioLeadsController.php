<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; 


class RelatorioLeadsController extends Controller
{
    public function relatorioleads(Request $request){

        $idcampanha = $request->input('campanha');

        $campanha = DB::table('campanha')
        ->select('campanha.*')->get();

        $leads = DB::table('leads')
         ->join('campanha', 'campanha.id', '=', 'leads.campanha_id')
         ->select('leads.*')
         ->where('campanha.id', $idcampanha)->get();

        return view('relatorioleads')
        ->with(compact('leads','campanha'));

         
              
    }

    public function gerarelatorioleads(Request $request)
    {
        $idcampopdfcampanha = $request->input('campanhaid');
        
        $leads = DB::table('leads')
        ->join('campanha', 'campanha.id', '=', 'leads.campanha_id')
        ->select('leads.*')
        ->where('campanha.id',$idcampopdfcampanha)->get();

        $campanha = DB::table('campanha')
        ->join('leads', 'campanha.id', '=', 'leads.campanha_id')
        ->select('campanha.*')
        ->where('campanha.id',$idcampopdfcampanha)->distinct()->get();
    
    
        $qtdlead=count($leads);  
     
        return \PDF::loadView('pdfleads', compact('leads','qtdlead','campanha'))
                    // Se quiser que fique no formato a4 retrato: ->setPaper('a4', 'landscape')
                    ->download('relatorio.pdf');
    
                    return redirect('relatorioleads');
    }
}
