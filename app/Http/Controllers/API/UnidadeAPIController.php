<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUnidadeAPIRequest;
use App\Http\Requests\API\UpdateUnidadeAPIRequest;
use App\Models\Unidade;
use App\Repositories\UnidadeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class UnidadeController
 * @package App\Http\Controllers\API
 */

class UnidadeAPIController extends AppBaseController
{
    /** @var  UnidadeRepository */
    private $unidadeRepository;

    public function __construct(UnidadeRepository $unidadeRepo)
    {
        $this->unidadeRepository = $unidadeRepo;
    }

    /**
     * Display a listing of the Unidade.
     * GET|HEAD /unidades
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $unidades = $this->unidadeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($unidades->toArray(), 'Unidades retrieved successfully');
    }

    /**
     * Store a newly created Unidade in storage.
     * POST /unidades
     *
     * @param CreateUnidadeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUnidadeAPIRequest $request)
    {
        $input = $request->all();

        $unidade = $this->unidadeRepository->create($input);

        return $this->sendResponse($unidade->toArray(), 'Unidade saved successfully');
    }

    /**
     * Display the specified Unidade.
     * GET|HEAD /unidades/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Unidade $unidade */
        $unidade = $this->unidadeRepository->find($id);

        if (empty($unidade)) {
            return $this->sendError('Unidade not found');
        }

        return $this->sendResponse($unidade->toArray(), 'Unidade retrieved successfully');
    }

    /**
     * Update the specified Unidade in storage.
     * PUT/PATCH /unidades/{id}
     *
     * @param int $id
     * @param UpdateUnidadeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUnidadeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Unidade $unidade */
        $unidade = $this->unidadeRepository->find($id);

        if (empty($unidade)) {
            return $this->sendError('Unidade not found');
        }

        $unidade = $this->unidadeRepository->update($input, $id);

        return $this->sendResponse($unidade->toArray(), 'Unidade updated successfully');
    }

    /**
     * Remove the specified Unidade from storage.
     * DELETE /unidades/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Unidade $unidade */
        $unidade = $this->unidadeRepository->find($id);

        if (empty($unidade)) {
            return $this->sendError('Unidade not found');
        }

        $unidade->delete();

        return $this->sendSuccess('Unidade deleted successfully');
    }
}
