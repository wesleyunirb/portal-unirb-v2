<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatenoticiaAPIRequest;
use App\Http\Requests\API\UpdatenoticiaAPIRequest;
use App\Models\noticia;
use App\Repositories\noticiaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class noticiaController
 * @package App\Http\Controllers\API
 */

class noticiaAPIController extends AppBaseController
{
    /** @var  noticiaRepository */
    private $noticiaRepository;

    public function __construct(noticiaRepository $noticiaRepo)
    {
        $this->noticiaRepository = $noticiaRepo;
    }

    /**
     * Display a listing of the noticia.
     * GET|HEAD /noticias
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $noticias = $this->noticiaRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($noticias->toArray(), 'Noticias retrieved successfully');
    }

    /**
     * Store a newly created noticia in storage.
     * POST /noticias
     *
     * @param CreatenoticiaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatenoticiaAPIRequest $request)
    {
        $input = $request->all();

        $noticia = $this->noticiaRepository->create($input);

        return $this->sendResponse($noticia->toArray(), 'Noticia saved successfully');
    }

    /**
     * Display the specified noticia.
     * GET|HEAD /noticias/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var noticia $noticia */
        $noticia = $this->noticiaRepository->find($id);

        if (empty($noticia)) {
            return $this->sendError('Noticia not found');
        }

        return $this->sendResponse($noticia->toArray(), 'Noticia retrieved successfully');
    }

    /**
     * Update the specified noticia in storage.
     * PUT/PATCH /noticias/{id}
     *
     * @param int $id
     * @param UpdatenoticiaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatenoticiaAPIRequest $request)
    {
        $input = $request->all();

        /** @var noticia $noticia */
        $noticia = $this->noticiaRepository->find($id);

        if (empty($noticia)) {
            return $this->sendError('Noticia not found');
        }

        $noticia = $this->noticiaRepository->update($input, $id);

        return $this->sendResponse($noticia->toArray(), 'noticia updated successfully');
    }

    /**
     * Remove the specified noticia from storage.
     * DELETE /noticias/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var noticia $noticia */
        $noticia = $this->noticiaRepository->find($id);

        if (empty($noticia)) {
            return $this->sendError('Noticia not found');
        }

        $noticia->delete();

        return $this->sendResponse($id, 'Noticia deleted successfully');
    }
}
