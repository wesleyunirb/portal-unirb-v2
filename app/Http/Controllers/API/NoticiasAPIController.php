<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNoticiasAPIRequest;
use App\Http\Requests\API\UpdateNoticiasAPIRequest;
use App\Models\Noticias;
use App\Repositories\NoticiasRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class NoticiasController
 * @package App\Http\Controllers\API
 */

class NoticiasAPIController extends AppBaseController
{
    /** @var  NoticiasRepository */
    private $noticiasRepository;

    public function __construct(NoticiasRepository $noticiasRepo)
    {
        $this->noticiasRepository = $noticiasRepo;
    }

    /**
     * Display a listing of the Noticias.
     * GET|HEAD /noticias
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $noticias = $this->noticiasRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($noticias->toArray(), 'Noticias retrieved successfully');
    }

    /**
     * Store a newly created Noticias in storage.
     * POST /noticias
     *
     * @param CreateNoticiasAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateNoticiasAPIRequest $request)
    {
        $input = $request->all();

        $noticias = $this->noticiasRepository->create($input);

        return $this->sendResponse($noticias->toArray(), 'Noticias saved successfully');
    }

    /**
     * Display the specified Noticias.
     * GET|HEAD /noticias/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Noticias $noticias */
        $noticias = $this->noticiasRepository->find($id);

        if (empty($noticias)) {
            return $this->sendError('Noticias not found');
        }

        return $this->sendResponse($noticias->toArray(), 'Noticias retrieved successfully');
    }

    /**
     * Update the specified Noticias in storage.
     * PUT/PATCH /noticias/{id}
     *
     * @param int $id
     * @param UpdateNoticiasAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNoticiasAPIRequest $request)
    {
        $input = $request->all();

        /** @var Noticias $noticias */
        $noticias = $this->noticiasRepository->find($id);

        if (empty($noticias)) {
            return $this->sendError('Noticias not found');
        }

        $noticias = $this->noticiasRepository->update($input, $id);

        return $this->sendResponse($noticias->toArray(), 'Noticias updated successfully');
    }

    /**
     * Remove the specified Noticias from storage.
     * DELETE /noticias/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Noticias $noticias */
        $noticias = $this->noticiasRepository->find($id);

        if (empty($noticias)) {
            return $this->sendError('Noticias not found');
        }

        $noticias->delete();

        return $this->sendSuccess('Noticias deleted successfully');
    }
}
