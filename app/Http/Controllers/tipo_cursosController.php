<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createtipo_cursosRequest;
use App\Http\Requests\Updatetipo_cursosRequest;
use App\Repositories\tipo_cursosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class tipo_cursosController extends AppBaseController
{
    /** @var  tipo_cursosRepository */
    private $tipoCursosRepository;

    public function __construct(tipo_cursosRepository $tipoCursosRepo)
    {
        $this->tipoCursosRepository = $tipoCursosRepo;
    }

    /**
     * Display a listing of the tipo_cursos.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tipoCursos = $this->tipoCursosRepository->all();

        return view('tipo_cursos.index')
            ->with('tipoCursos', $tipoCursos);
    }

    /**
     * Show the form for creating a new tipo_cursos.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_cursos.create');
    }

    /**
     * Store a newly created tipo_cursos in storage.
     *
     * @param Createtipo_cursosRequest $request
     *
     * @return Response
     */
    public function store(Createtipo_cursosRequest $request)
    {
        $input = $request->all();

        $tipoCursos = $this->tipoCursosRepository->create($input);

        Flash::success('Tipo Cursos saved successfully.');

        return redirect(route('tipoCursos.index'));
    }

    /**
     * Display the specified tipo_cursos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoCursos = $this->tipoCursosRepository->find($id);

        if (empty($tipoCursos)) {
            Flash::error('Tipo Cursos not found');

            return redirect(route('tipoCursos.index'));
        }

        return view('tipo_cursos.show')->with('tipoCursos', $tipoCursos);
    }

    /**
     * Show the form for editing the specified tipo_cursos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoCursos = $this->tipoCursosRepository->find($id);

        if (empty($tipoCursos)) {
            Flash::error('Tipo Cursos not found');

            return redirect(route('tipoCursos.index'));
        }

        return view('tipo_cursos.edit')->with('tipoCursos', $tipoCursos);
    }

    /**
     * Update the specified tipo_cursos in storage.
     *
     * @param int $id
     * @param Updatetipo_cursosRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetipo_cursosRequest $request)
    {
        $tipoCursos = $this->tipoCursosRepository->find($id);

        if (empty($tipoCursos)) {
            Flash::error('Tipo Cursos not found');

            return redirect(route('tipoCursos.index'));
        }

        $tipoCursos = $this->tipoCursosRepository->update($request->all(), $id);

        Flash::success('Tipo Cursos updated successfully.');

        return redirect(route('tipoCursos.index'));
    }

    /**
     * Remove the specified tipo_cursos from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoCursos = $this->tipoCursosRepository->find($id);

        if (empty($tipoCursos)) {
            Flash::error('Tipo Cursos not found');

            return redirect(route('tipoCursos.index'));
        }

        $this->tipoCursosRepository->delete($id);

        Flash::success('Tipo Cursos deleted successfully.');

        return redirect(route('tipoCursos.index'));
    }
}
