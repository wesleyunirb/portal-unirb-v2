<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createtipo_cursoRequest;
use App\Http\Requests\Updatetipo_cursoRequest;
use App\Repositories\tipo_cursoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class tipo_cursoController extends AppBaseController
{
    /** @var  tipo_cursoRepository */
    private $tipoCursoRepository;

    public function __construct(tipo_cursoRepository $tipoCursoRepo)
    {
        $this->tipoCursoRepository = $tipoCursoRepo;
    }

    /**
     * Display a listing of the tipo_curso.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tipoCursos = $this->tipoCursoRepository->all();

        return view('tipo_cursos.index')
            ->with('tipoCursos', $tipoCursos);
    }

    /**
     * Show the form for creating a new tipo_curso.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_cursos.create');
    }

    /**
     * Store a newly created tipo_curso in storage.
     *
     * @param Createtipo_cursoRequest $request
     *
     * @return Response
     */
    public function store(Createtipo_cursoRequest $request)
    {
        $input = $request->all();

        $tipoCurso = $this->tipoCursoRepository->create($input);

        Flash::success('Tipo Curso criado com sucesso.');

        return redirect(route('tipoCursos.index'));
    }

    /**
     * Display the specified tipo_curso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoCurso = $this->tipoCursoRepository->find($id);

        if (empty($tipoCurso)) {
            Flash::error('Tipo Curso não encontrado');

            return redirect(route('tipoCursos.index'));
        }

        return view('tipo_cursos.show')->with('tipoCurso', $tipoCurso);
    }

    /**
     * Show the form for editing the specified tipo_curso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoCurso = $this->tipoCursoRepository->find($id);

        if (empty($tipoCurso)) {
            Flash::error('Tipo Curso não encontrado');

            return redirect(route('tipoCursos.index'));
        }

        return view('tipo_cursos.edit')->with('tipoCurso', $tipoCurso);
    }

    /**
     * Update the specified tipo_curso in storage.
     *
     * @param int $id
     * @param Updatetipo_cursoRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetipo_cursoRequest $request)
    {
        $tipoCurso = $this->tipoCursoRepository->find($id);

        if (empty($tipoCurso)) {
            Flash::error('Tipo Curso não encontrado');

            return redirect(route('tipoCursos.index'));
        }

        $tipoCurso = $this->tipoCursoRepository->update($request->all(), $id);

        Flash::success('Tipo Curso atualizado com sucesso.');

        return redirect(route('tipoCursos.index'));
    }

    /**
     * Remove the specified tipo_curso from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoCurso = $this->tipoCursoRepository->find($id);

        if (empty($tipoCurso)) {
            Flash::error('Tipo Curso não encontrado');

            return redirect(route('tipoCursos.index'));
        }

        $this->tipoCursoRepository->delete($id);

        Flash::success('Tipo Curso excluido com sucesso.');

        return redirect(route('tipoCursos.index'));
    }
}
