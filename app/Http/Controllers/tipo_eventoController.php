<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createtipo_eventoRequest;
use App\Http\Requests\Updatetipo_eventoRequest;
use App\Repositories\tipo_eventoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class tipo_eventoController extends AppBaseController
{
    /** @var  tipo_eventoRepository */
    private $tipoEventoRepository;

    public function __construct(tipo_eventoRepository $tipoEventoRepo)
    {
        $this->tipoEventoRepository = $tipoEventoRepo;
    }

    /**
     * Display a listing of the tipo_evento.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tipoEventos = $this->tipoEventoRepository->all();

        return view('tipo_eventos.index')
            ->with('tipoEventos', $tipoEventos);
    }

    /**
     * Show the form for creating a new tipo_evento.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_eventos.create');
    }

    /**
     * Store a newly created tipo_evento in storage.
     *
     * @param Createtipo_eventoRequest $request
     *
     * @return Response
     */
    public function store(Createtipo_eventoRequest $request)
    {
        $input = $request->all();

        $tipoEvento = $this->tipoEventoRepository->create($input);

        Flash::success('Tipo Evento salvo com sucesso.');

        return redirect(route('tipoEventos.index'));
    }

    /**
     * Display the specified tipo_evento.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoEvento = $this->tipoEventoRepository->find($id);

        if (empty($tipoEvento)) {
            Flash::error('Tipo Evento não encontrado');

            return redirect(route('tipoEventos.index'));
        }

        return view('tipo_eventos.show')->with('tipoEvento', $tipoEvento);
    }

    /**
     * Show the form for editing the specified tipo_evento.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoEvento = $this->tipoEventoRepository->find($id);

        if (empty($tipoEvento)) {
            Flash::error('Tipo Evento não encontrado');

            return redirect(route('tipoEventos.index'));
        }

        return view('tipo_eventos.edit')->with('tipoEvento', $tipoEvento);
    }

    /**
     * Update the specified tipo_evento in storage.
     *
     * @param int $id
     * @param Updatetipo_eventoRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetipo_eventoRequest $request)
    {
        $tipoEvento = $this->tipoEventoRepository->find($id);

        if (empty($tipoEvento)) {
            Flash::error('Tipo Evento não encontrado');

            return redirect(route('tipoEventos.index'));
        }

        $tipoEvento = $this->tipoEventoRepository->update($request->all(), $id);

        Flash::success('Tipo Evento atualizado com sucesso.');

        return redirect(route('tipoEventos.index'));
    }

    /**
     * Remove the specified tipo_evento from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoEvento = $this->tipoEventoRepository->find($id);

        if (empty($tipoEvento)) {
            Flash::error('Tipo Evento não encontrado');

            return redirect(route('tipoEventos.index'));
        }

        $this->tipoEventoRepository->delete($id);

        Flash::success('Tipo Evento excluido com sucesso.');

        return redirect(route('tipoEventos.index'));
    }
}
