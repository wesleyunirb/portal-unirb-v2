<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateunidadesRequest;
use App\Http\Requests\UpdateunidadesRequest;
use App\Repositories\unidadesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;


class unidadesController extends AppBaseController
{
    /** @var  unidadesRepository */
    private $unidadesRepository;

    public function __construct(unidadesRepository $unidadesRepo)
    {
        $this->unidadesRepository = $unidadesRepo;
    }

    /**
     * Display a listing of the unidades.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $unidades = $this->unidadesRepository->all();

        return view('unidades.index')
            ->with('unidades', $unidades);
    }

    /**
     * Show the form for creating a new unidades.
     *
     * @return Response
     */
    public function create()
    {
        return view('unidades.create');
    }

    /**
     * Store a newly created unidades in storage.
     *
     * @param CreateunidadesRequest $request
     *
     * @return Response
     */
    public function store(CreateunidadesRequest $request)
    {
        $input = $request->all();

        $unidades = $this->unidadesRepository->create($input);

        Flash::success('Unidade criada com sucesso.');

        return redirect(route('unidades.index'));
    }

    /**
     * Display the specified unidades.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $unidades = $this->unidadesRepository->find($id);

        if (empty($unidades)) {
            Flash::error('Unidade não encontrada');

            return redirect(route('unidades.index'));
        }

        return view('unidades.show')->with('unidades', $unidades);
    }

    /**
     * Show the form for editing the specified unidades.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $unidades = $this->unidadesRepository->find($id);

        if (empty($unidades)) {
            Flash::error('Unidade não encontrada');

            return redirect(route('unidades.index'));
        }

        return view('unidades.edit')->with('unidades', $unidades);
    }

    /**
     * Update the specified unidades in storage.
     *
     * @param int $id
     * @param UpdateunidadesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateunidadesRequest $request)
    {
        $unidades = $this->unidadesRepository->find($id);

        if (empty($unidades)) {
            Flash::error('Unidade não encontrada');

            return redirect(route('unidades.index'));
        }

        $unidades = $this->unidadesRepository->update($request->all(), $id);

        Flash::success('Unidade atualizada com sucesso.');

        return redirect(route('unidades.index'));
    }

    /**
     * Remove the specified unidades from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $unidades = $this->unidadesRepository->find($id);

        if (empty($unidades)) {
            Flash::error('Unidade não encontrada');

            return redirect(route('unidades.index'));
        }

        $this->unidadesRepository->delete($id);

        Flash::success('Unidade excluida com sucesso.');

        return redirect(route('unidades.index'));
    }

 

    
    

    
}
