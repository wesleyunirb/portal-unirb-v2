<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unidade;
use Redirect;
use DB; 
use App\Quotation; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //unidades
         $unidades = DB::table('unidades')
         ->select('unidades.*')->get();
         
         //POSCOUNT
         $unidadescount=count($unidades);  

         //Cursos
         $cursos = DB::table('curso')
         ->select('curso.*')->get();
         
         //cursosCOUNT
         $cursoscount=count($cursos);  

           //Noticias
           $noticias = DB::table('noticias')
           ->select('noticias.*')->get();
           
           //noticiasCOUNT
           $noticiascount=count($noticias);  


            //Eventos
            $eventos = DB::table('eventos')
            ->select('eventos.*')->get();
            
            //eventosCOUNT
            $eventoscount=count($eventos);  


         return view('home')
         ->with(compact('unidades','unidadescount','cursos','cursoscount','noticias','noticiascount','eventoscount'));                      }


public function top()
    {
         //unidades
         $unidades = DB::table('unidades')
         ->select('unidades.*')->get();
         
         //POSCOUNT
         $unidadescount=count($unidades);  

         //Cursos
         $cursos = DB::table('curso')
         ->select('curso.*')->get();
         
         //cursosCOUNT
         $cursoscount=count($cursos);  

           //Noticias
           $noticias = DB::table('noticias')
           ->select('noticias.*')->get();
           
           //noticiasCOUNT
           $noticiascount=count($noticias);  


            //Eventos
            $eventos = DB::table('eventos')
            ->select('eventos.*')->get();
            
            //eventosCOUNT
            $eventoscount=count($eventos);  


         return view('home')
         ->with(compact('unidades','unidadescount','cursos','cursoscount','noticias','noticiascount','eventoscount'));                      }
}

