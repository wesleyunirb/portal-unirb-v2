<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use DB; 
use App\Lead; 

class RelatorioController extends Controller
{
    public function relatorioinscrito(Request $request){

        $idevento = $request->input('evento');


         $evento= DB::table('eventos')
         ->select('eventos.*')->get(); 

         $inscritos = DB::table('inscritos')
          ->join('eventos', 'eventos.id', '=', 'inscritos.evento_id')
          ->select('inscritos.*')
          ->where('eventos.id',$idevento)->get();     

         return view('relatorioinscritos')
         ->with(compact('evento','inscritos')); 

         
              
    }

    public function gerarelatorio(Request $request)
{
    $idcampopdfevento = $request->input('eventoid');
    
    $inscritos = DB::table('inscritos')
    ->join('eventos', 'eventos.id', '=', 'inscritos.evento_id')
    ->select('inscritos.*')
    ->where('eventos.id',$idcampopdfevento)->get();

    $evento= DB::table('eventos')
    ->select('eventos.titulo')
    ->where('eventos.id',$idcampopdfevento)->get();

    $qtdlead=count($inscritos);  
 
    return \PDF::loadView('pdfinscritos', compact('inscritos','qtdlead','evento'))
                // Se quiser que fique no formato a4 retrato: ->setPaper('a4', 'landscape')
                ->download('relatorio.pdf');

                return redirect('relatorioinscritos');
}
}
