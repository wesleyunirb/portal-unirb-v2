<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatebannersRequest;
use App\Http\Requests\UpdatebannersRequest;
use App\Repositories\bannersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Unidade; 


class bannersController extends AppBaseController
{
    /** @var  bannersRepository */
    private $bannersRepository;

    public function __construct(bannersRepository $bannersRepo)
    {
        $this->bannersRepository = $bannersRepo;
    }

    /**
     * Display a listing of the banners.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $banners = $this->bannersRepository->all();

        return view('banners.index')
            ->with('banners', $banners);
    }

    /**
     * Show the form for creating a new banners.
     *
     * @return Response
     */
    public function create()
    {
        $unidades = Unidade::all();
        

        if(!empty($unidades)){

            return view('banners.create', compact('unidades'))->with('unidades', $unidades);

        }else{

            return view('banners.create', compact('unidades'))->with('unidades', $unidades);

        }   
     }

    /**
     * Store a newly created banners in storage.
     *
     * @param CreatebannersRequest $request
     *
     * @return Response
     */
    public function store(CreatebannersRequest $request)
    {
        $input = $request->all();

        $banners = $this->bannersRepository->create($input);

        Flash::success('Banner Criado com sucesso.');

        return redirect(route('banners.index'));
    }

    /**
     * Display the specified banners.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $banners = $this->bannersRepository->find($id);

        if (empty($banners)) {
            Flash::error('Banner não encontrado');

            return redirect(route('banners.index'));
        }

        return view('banners.show')->with('banners', $banners);
    }

    /**
     * Show the form for editing the specified banners.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $banners = $this->bannersRepository->find($id);
        $unidades = Unidade::all();


        if (empty($banners)) {
            Flash::error('Banner não encontrado');

            return redirect(route('banners.index'));
        }

        return view('banners.edit', compact('unidades','banners'))->with('unidades', $unidades)->with('banners', $banners);
    }

    /**
     * Update the specified banners in storage.
     *
     * @param int $id
     * @param UpdatebannersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatebannersRequest $request)
    {
        $banners = $this->bannersRepository->find($id);

        if (empty($banners)) {
            Flash::error('Banner não encontrado');

            return redirect(route('banners.index'));
        }

        $banners = $this->bannersRepository->update($request->all(), $id);

        Flash::success('Banner Atualizado com sucesso.');

        return redirect(route('banners.index'));
    }

    /**
     * Remove the specified banners from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $banners = $this->bannersRepository->find($id);

        if (empty($banners)) {
            Flash::error('Banner não encontrado');

            return redirect(route('banners.index'));
        }

        $this->bannersRepository->delete($id);

        Flash::success('Banner Excluido com sucesso.');

        return redirect(route('banners.index'));
    }
}
