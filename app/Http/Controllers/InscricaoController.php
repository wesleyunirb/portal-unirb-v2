<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inscrito;

use DB; 


class InscricaoController extends Controller
{
    public function save(Request $req){
        $inscrito = new Inscrito;
        $inscrito->nome = $req->nome;
        $inscrito->cpf = $req->cpf;
        $inscrito->email = $req->email;
        $inscrito->telefone = $req->telefone;
        $inscrito->evento_id = $req->eventoid;
        $inscrito->unidade_id = $req->unidadeid;
      

        
        $inscrito->save();
        return redirect('principal');

    }
}
