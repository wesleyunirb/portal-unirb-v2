<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateusersRequest;
use App\Http\Requests\UpdateusersRequest;
use App\Repositories\usersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Level;
use Validator;
use App\User;


class usersController extends AppBaseController
{
    /** @var  usersRepository */
    private $usersRepository;

    public function __construct(usersRepository $usersRepo)
    {
        $this->usersRepository = $usersRepo;
    }

    /**
     * Display a listing of the users.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->usersRepository->all();

        return view('users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new users.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created users in storage.
     *
     * @param CreateusersRequest $request
     *
     * @return Response
     */
    public function store(CreateusersRequest $request)
    {
        $data = $request->all();

        //Criando regras de validação de dados
        $validatedData = $request->validate([
            'name'             => 'required|max:55',
            'email'            => 'email|max:55',
            'password'         => 'required',
            'ti'         => 'required',
            'mkt'         => 'required',
        ]);

        if($data){
            //Criando um usuário
            $user = new User;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->ti = $data['ti'];
            $user->mkt = $data['mkt'];
            $user->password = bcrypt($data['password']);
            //Salvando usuário no banco de dados
            $user->save();

            Flash::success('Usuário Cadastrado com Sucesso.');
            return redirect(route('users.index'));

        }else{
            
            Flash::error('Falha ao Cadastrar Usuário');
            return redirect(route('users.index'));
        }
    }


    /**
     * Display the specified users.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $users = $this->usersRepository->find($id);

        if (empty($users)) {
            Flash::error('Usuário não encontrado');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('users', $users);
    }

    /**
     * Show the form for editing the specified users.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $users = $this->usersRepository->find($id);
        

        if (empty($users)) {
            Flash::error('Usuário não encontrado ');

            return redirect(route('users.index'));
        }

        return view('users.edit')->with('users', $users);
    }

    /**
     * Update the specified users in storage.
     *
     * @param int $id
     * @param UpdateusersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateusersRequest $request)
    {
        $data = $request->all();

        //Através do método "find", passamos o id de um usuário, recuperamos e salvamos na variável.
        $user = User::find($id);

        //Criando regras de validação de dados
        $validatedData = $request->validate([
            'name'             => 'required|max:55',
            'email'            => 'required|string|email|max:55',
            'password'         => 'required|max:55',
        ]);

        //Se o email passado na requisição for diferente do cadastro atual do médico esse bloco será executado
        if($user->email != $data['email'])
        {
            //Verificando se o e-mail digitado já existe no banco de dados
            if(User::where('email', $data['email'])->count()){
                //Caso o email já esteja em uso será exibida uma mensagem informando o mesmo
                return redirect()->back()->with('error', 'O email informado já está em uso');
            }
        }

        //Somente os campos determinados abaixo poderão ser preenchidos
        $data = $request->only(['name', 'email', 'password','ti','mkt','default']);

        if($data)
        {
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->ti = $data['ti'];
            $user->mkt = $data['mkt'];
            $user->default = $data['default'];
            $user->password = bcrypt($data['password']);
            //Atualizando dados
            $user->update();

            Flash::success('Usuário atualizado com sucesso.');
            return redirect(route('users.index'));

        }else{
            return redirect()->back()->with('error', 'Erro ao atualizar os dados');
        }
    }




    /**
     * Remove the specified users from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $users = $this->usersRepository->find($id);

        if (empty($users)) {
            Flash::error('Usuário não encontrado');

            return redirect(route('users.index'));
        }

        $this->usersRepository->delete($id);

        Flash::success('Usuário excluido com sucesso.');

        return redirect(route('users.index'));
    }
}
