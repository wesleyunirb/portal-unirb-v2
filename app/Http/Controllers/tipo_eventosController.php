<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createtipo_eventosRequest;
use App\Http\Requests\Updatetipo_eventosRequest;
use App\Repositories\tipo_eventosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class tipo_eventosController extends AppBaseController
{
    /** @var  tipo_eventosRepository */
    private $tipoEventosRepository;

    public function __construct(tipo_eventosRepository $tipoEventosRepo)
    {
        $this->tipoEventosRepository = $tipoEventosRepo;
    }

    /**
     * Display a listing of the tipo_eventos.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tipoEventos = $this->tipoEventosRepository->all();

        return view('tipo_eventos.index')
            ->with('tipoEventos', $tipoEventos);
    }

    /**
     * Show the form for creating a new tipo_eventos.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_eventos.create');
    }

    /**
     * Store a newly created tipo_eventos in storage.
     *
     * @param Createtipo_eventosRequest $request
     *
     * @return Response
     */
    public function store(Createtipo_eventosRequest $request)
    {
        $input = $request->all();

        $tipoEventos = $this->tipoEventosRepository->create($input);

        Flash::success('Tipo Eventos saved successfully.');

        return redirect(route('tipoEventos.index'));
    }

    /**
     * Display the specified tipo_eventos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoEventos = $this->tipoEventosRepository->find($id);

        if (empty($tipoEventos)) {
            Flash::error('Tipo Eventos not found');

            return redirect(route('tipoEventos.index'));
        }

        return view('tipo_eventos.show')->with('tipoEventos', $tipoEventos);
    }

    /**
     * Show the form for editing the specified tipo_eventos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoEventos = $this->tipoEventosRepository->find($id);

        if (empty($tipoEventos)) {
            Flash::error('Tipo Eventos not found');

            return redirect(route('tipoEventos.index'));
        }

        return view('tipo_eventos.edit')->with('tipoEventos', $tipoEventos);
    }

    /**
     * Update the specified tipo_eventos in storage.
     *
     * @param int $id
     * @param Updatetipo_eventosRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetipo_eventosRequest $request)
    {
        $tipoEventos = $this->tipoEventosRepository->find($id);

        if (empty($tipoEventos)) {
            Flash::error('Tipo Eventos not found');

            return redirect(route('tipoEventos.index'));
        }

        $tipoEventos = $this->tipoEventosRepository->update($request->all(), $id);

        Flash::success('Tipo Eventos updated successfully.');

        return redirect(route('tipoEventos.index'));
    }

    /**
     * Remove the specified tipo_eventos from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoEventos = $this->tipoEventosRepository->find($id);

        if (empty($tipoEventos)) {
            Flash::error('Tipo Eventos not found');

            return redirect(route('tipoEventos.index'));
        }

        $this->tipoEventosRepository->delete($id);

        Flash::success('Tipo Eventos deleted successfully.');

        return redirect(route('tipoEventos.index'));
    }
}
