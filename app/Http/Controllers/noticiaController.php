<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatenoticiasRequest;
use App\Http\Requests\UpdatenoticiasRequest;
use App\Repositories\noticiasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\noticia;
use Redirect;
use DB; 
use App\Quotation;
use App\Unidade; 

class noticiaController extends AppBaseController
{
    /** @var  noticiasRepository */
    private $noticiasRepository;

    public function __construct(noticiasRepository $noticiasRepo)
    {
        $this->noticiasRepository = $noticiasRepo;
    }

    /**
     * Display a listing of the noticias.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $noticias = $this->noticiasRepository->all();

        return view('noticias.index')
            ->with('noticias', $noticias);
    }

    /**
     * Show the form for creating a new noticias.
     *
     * @return Response
     */
    public function create()
    {
        $unidades = Unidade::all();
        

        if(!empty($unidades)){

            return view('noticias.create', compact('unidades'))->with('unidades', $unidades);

        }else{

            return view('noticias.create', compact('unidades'))->with('unidades', $venda);

        }

        
    }

    /**
     * Store a newly created noticias in storage.
     *
     * @param CreatenoticiasRequest $request
     *
     * @return Response
     */
    public function store(CreatenoticiasRequest $request)
    {
        $input = $request->all();


        $noticias = $this->noticiasRepository->create($input);

        Flash::success('Noticias saved successfully.');

        return redirect(route('noticias.index'));
    }

    /**
     * Display the specified noticias.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $noticias = $this->noticiasRepository->find($id);

        if (empty($noticias)) {
            Flash::error('Noticias not found');

            return redirect(route('noticias.index'));
        }
        

        return view('noticias.show')->with('noticias', $noticias);
    }

    /**
     * Show the form for editing the specified noticias.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $noticias = $this->noticiasRepository->find($id);

        if (empty($noticias)) {
            Flash::error('Noticias not found');

            return redirect(route('noticias.index'));
        }

        return view('noticias.edit')->with('noticias', $noticias);
    }

    /**
     * Update the specified noticias in storage.
     *
     * @param int $id
     * @param UpdatenoticiasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatenoticiasRequest $request)
    {
        $noticias = $this->noticiasRepository->find($id);

        if (empty($noticias)) {
            Flash::error('Noticia salva com sucesso');

            return redirect(route('noticias.index'));
        }

        $noticias = $this->noticiasRepository->update($request->all(), $id);

        Flash::success('Noticia atualizada com sucesso.');

        return redirect(route('noticias.index'));
    }

    /**
     * Remove the specified noticias from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $noticias = $this->noticiasRepository->find($id);

        if (empty($noticias)) {
            Flash::error('Noticia não encontrada');

            return redirect(route('noticias.index'));
        }

        $this->noticiasRepository->delete($id);

        Flash::success('Noticia excluida com sucesso.');

        return redirect(route('noticias.index'));
    }

    public function noticia1()
    {
        //noticia1

        $noticia = DB::table('noticias')
        ->join('unidades', 'unidades.id', '=', 'noticias.unidade_id')
        ->select('noticias.*')
        ->where('unidades.id', 1)->where('noticias.id',1)
        ->get();
     
         return view('noticia')
         ->with(compact('noticia'));                  
    }

    public function noticia2()
    {
        //noticia2
          $noticia = DB::table('noticias')
        ->join('unidades', 'unidades.id', '=', 'noticias.unidade_id')
        ->select('noticias.*')
        ->where('unidades.id', 1)->where('noticias.id',2)
        ->get();
     
         return view('noticia')
         ->with(compact('noticia'));                  
    }

    public function noticia3()
    {
        //noticia2
          $noticia = DB::table('noticias')
        ->join('unidades', 'unidades.id', '=', 'noticias.unidade_id')
        ->select('noticias.*')
        ->where('unidades.id', 1)->where('noticias.id',3)
        ->get();
     
         return view('noticia')
         ->with(compact('noticia'));                  
    }

    public function noticia4()
    {
        //noticia2
          $noticia = DB::table('noticias')
        ->join('unidades', 'unidades.id', '=', 'noticias.unidade_id')
        ->select('noticias.*')
        ->where('unidades.id', 1)->where('noticias.id',4)
        ->get();
     
         return view('noticia')
         ->with(compact('noticia'));                  
    }

    public function noticia5()
    {
        //noticia2
          $noticia = DB::table('noticias')
        ->join('unidades', 'unidades.id', '=', 'noticias.unidade_id')
        ->select('noticias.*')
        ->where('unidades.id', 1)->where('noticias.id',5)
        ->get();
     
         return view('noticia')
         ->with(compact('noticia'));                  
    }

    public function noticia6()
    {
        //noticia2
          $noticia = DB::table('noticias')
        ->join('unidades', 'unidades.id', '=', 'noticias.unidade_id')
        ->select('noticias.*')
        ->where('unidades.id', 1)->where('noticias.id',6)
        ->get();
     
         return view('noticia')
         ->with(compact('noticia'));                  
    }

    

   
}

   
 



