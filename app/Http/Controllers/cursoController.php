<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecursoRequest;
use App\Http\Requests\UpdatecursoRequest;
use App\Repositories\cursoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Unidade; 
use App\Tipo_curso; 


class cursoController extends AppBaseController
{
    /** @var  cursoRepository */
    private $cursoRepository;

    public function __construct(cursoRepository $cursoRepo)
    {
        $this->cursoRepository = $cursoRepo;
    }

    /**
     * Display a listing of the curso.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $cursos = $this->cursoRepository->all();

        return view('cursos.index')
            ->with('cursos', $cursos);
    }

    /**
     * Show the form for creating a new curso.
     *
     * @return Response
     */
    public function create()
    {

        $unidades = Unidade::all();
        $tipocursos = Tipo_curso::all();


        return view('cursos.create', compact('unidades','tipocursos'))->with('unidades', $unidades)->with('tipocursos', $tipocursos);
    }

    /**
     * Store a newly created curso in storage.
     *
     * @param CreatecursoRequest $request
     *
     * @return Response
     */
    public function store(CreatecursoRequest $request)
    {
        $input = $request->all();

        $curso = $this->cursoRepository->create($input);

        Flash::success('Curso saved successfully.');

        return redirect(route('cursos.index'));
    }

    /**
     * Display the specified curso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $curso = $this->cursoRepository->find($id);

        if (empty($curso)) {
            Flash::error('Curso not found');

            return redirect(route('cursos.index'));
        }

        return view('cursos.show')->with('curso', $curso);
    }

    /**
     * Show the form for editing the specified curso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $curso = $this->cursoRepository->find($id);
        $unidades = Unidade::all();
        $tipocursos = Tipo_curso::all();

        if (empty($curso)) {
            Flash::error('Curso not found');

            return redirect(route('cursos.index'));
        }

        return view('cursos.edit', compact('unidades','curso','tipocursos'))->with('unidades', $unidades)->with('curso', $curso)->with('tipocursos', $tipocursos);
    }

    /**
     * Update the specified curso in storage.
     *
     * @param int $id
     * @param UpdatecursoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecursoRequest $request)
    {
        $curso = $this->cursoRepository->find($id);

        if (empty($curso)) {
            Flash::error('Curso not found');

            return redirect(route('cursos.index'));
        }

        $curso = $this->cursoRepository->update($request->all(), $id);

        Flash::success('Curso updated successfully.');

        return redirect(route('cursos.index'));
    }

    /**
     * Remove the specified curso from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $curso = $this->cursoRepository->find($id);

        if (empty($curso)) {
            Flash::error('Curso not found');

            return redirect(route('cursos.index'));
        }

        $this->cursoRepository->delete($id);

        Flash::success('Curso deleted successfully.');

        return redirect(route('cursos.index'));
    }
}
