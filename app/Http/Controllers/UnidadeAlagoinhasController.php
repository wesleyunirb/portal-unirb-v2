<?php

namespace App\Http\Controllers;

use Redirect;
use DB; 
use App\Quotation; 
use Request; 
use Illuminate\Support\Facades\Input as input; 

class UnidadeAlagoinhasController extends Controller
{
    public function unidade()
    {
        //NOME UNIDADE
        $nome = DB::table('unidades') ->select('nome') ->where('unidades.id', 2)->get();

        //NOME Logomarca
        $logomarca = DB::table('unidades') ->select('logomarca') ->where('unidades.id', 2)->get();

    
        //--------BANNERS-----//
        //banner1

      $banner1 = DB::table('banners')
      ->join('unidades', 'unidades.id', '=', 'banners.unidade_id')
      ->select('banners.img')
      ->where('unidades.id', 2)->where('banners.id',4)
      ->get();

       //banner2

       $banner2 = DB::table('banners')
       ->join('unidades', 'unidades.id', '=', 'banners.unidade_id')
       ->select('banners.img')
       ->where('unidades.id', 2)->where('banners.id',5)
       ->get();

        //banner3

        $banner3 = DB::table('banners')
        ->join('unidades', 'unidades.id', '=', 'banners.unidade_id')
        ->select('banners.img')
        ->where('unidades.id', 2)->where('banners.id',6)
        ->get();


        
      
         //--------NOTICIAS-----//

          //noticia1
          $noticia1 = DB::table('noticias')
          ->join('unidades', 'unidades.id', '=', 'noticias.unidade_id')
          ->select('noticias.*')
          ->where('unidades.id',2)->where('noticias.id',6)->where('noticias.status',1)->get();             
  
         //noticia2
         $noticia2 = DB::table('noticias')
         ->join('unidades', 'unidades.id', '=', 'noticias.unidade_id')
         ->select('noticias.*')
         ->where('unidades.id',2)->where('noticias.id',7)->where('noticias.status',1)->get();  
        

         //noticia3
         $noticia3 = DB::table('noticias')
         ->join('unidades', 'unidades.id', '=', 'noticias.unidade_id')
         ->select('noticias.*')
         ->where('unidades.id',2)->where('noticias.id',8)->where('noticias.status',1)->get();  

          
        //noticia4
        $noticia4 = DB::table('noticias')
        ->join('unidades', 'unidades.id', '=', 'noticias.unidade_id')
        ->select('noticias.*')
        ->where('unidades.id',2)->where('noticias.id',9)->where('noticias.status',1)->get();  

        

          //--------CURSOS-----//


          //BACHARELADO
          $bacharelado = DB::table('curso')
          ->join('tipo_cursos', 'curso.tipo_cursos_id', '=', 'tipo_cursos.id')
           ->join('unidades', 'curso.unidade_id', '=', 'unidades.id')
          ->select('curso.nome')
          ->where('tipo_cursos.id',1)->where('unidades.id',2)->where('curso.status',1)->orderBy('nome' ,'ASC')->get();  
         
          //BACHARELADO COUNT
            $bachareladocount=count($bacharelado);

        
          //Tecnólogo
          $tecnologo = DB::table('curso')
          ->join('tipo_cursos', 'curso.tipo_cursos_id', '=', 'tipo_cursos.id')
           ->join('unidades', 'curso.unidade_id', '=', 'unidades.id')
          ->select('curso.nome')
          ->where('tipo_cursos.id',2)->where('unidades.id',2)->where('curso.status',1)->orderBy('nome' ,'ASC')->get();  
         
          //Tecnólogo COUNT
          $tecnologocount=count($tecnologo);   

          //licenciatura
          $licenciatura = DB::table('curso')
          ->join('tipo_cursos', 'curso.tipo_cursos_id', '=', 'tipo_cursos.id')
           ->join('unidades', 'curso.unidade_id', '=', 'unidades.id')
          ->select('curso.nome')
          ->where('tipo_cursos.id',3)->where('unidades.id',2)->where('curso.status',1)->orderBy('nome' ,'ASC')->get(); 

           //LicenciaturaCOUNT
           $licenciaturacount=count($licenciatura);   
    

           //EAD
           $ead = DB::table('curso')
           ->join('tipo_cursos', 'curso.tipo_cursos_id', '=', 'tipo_cursos.id')
            ->join('unidades', 'curso.unidade_id', '=', 'unidades.id')
           ->select('curso.nome')
           ->where('tipo_cursos.id',4)->where('unidades.id',2)->where('curso.status',1)->orderBy('nome' ,'ASC')->get();

            //EADCOUNT
            $eadcount=count($ead);   

           
            //Pós-Graduação
            $pos = DB::table('curso')
            ->join('tipo_cursos', 'curso.tipo_cursos_id', '=', 'tipo_cursos.id')
             ->join('unidades', 'curso.unidade_id', '=', 'unidades.id')
            ->select('curso.nome')
            ->where('tipo_cursos.id',5)->where('unidades.id',2)->where('curso.status',1)->orderBy('nome' ,'ASC')->get();

            //POSCOUNT
            $poscount=count($pos);  

              //Sequencial
              $sequencial = DB::table('curso')
              ->join('tipo_cursos', 'curso.tipo_cursos_id', '=', 'tipo_cursos.id')
               ->join('unidades', 'curso.unidade_id', '=', 'unidades.id')
              ->select('curso.nome')
              ->where('tipo_cursos.id',6)->where('unidades.id',2)->where('curso.status',1)->orderBy('nome' ,'ASC')->get();
  
              //POSCOUNT
              $seqcount=count($sequencial);  


              //LOCALIZAÇÃO 
              $latitude = DB::table('unidades')->select('unidades.latitude')->where('unidades.id',2)->get();
              $longitude = DB::table('unidades')->select('unidades.longitude')->where('unidades.id',2)->get();
            
             
              //MODAL CURSOS BACHARELADO
              $curso1bacharelado = DB::table('curso')
              ->join('tipo_cursos', 'curso.tipo_cursos_id', '=', 'tipo_cursos.id')
               ->join('unidades', 'curso.unidade_id', '=', 'unidades.id')
              ->select('curso.*')
              ->where('tipo_cursos.id',1)->where('unidades.id',2)->where('curso.id',1)->get();

             
              // ---- EVENTOS ----

               //EVENTO1
               $evento1 = DB::table('eventos')
               ->join('unidades', 'eventos.unidade_id', '=', 'unidades.id')
               ->select('eventos.*')->where('unidades.id',2)->where('eventos.id',1)->where('eventos.status',1)->get();

               //EVENTO2
               $evento2 = DB::table('eventos')
               ->join('unidades', 'eventos.unidade_id', '=', 'unidades.id')
               ->select('eventos.*')->where('unidades.id',2)->where('eventos.id',2)->where('eventos.status',1)->get();

                //EVENTO3
                $evento3 = DB::table('eventos')
                ->join('unidades', 'eventos.unidade_id', '=', 'unidades.id')
                ->select('eventos.*')->where('unidades.id',2)->where('eventos.id',3)->where('eventos.status',1)->get();

                 //EVENTO4
               $evento4 = DB::table('eventos')
               ->join('unidades', 'eventos.unidade_id', '=', 'unidades.id')
               ->select('eventos.*')->where('unidades.id',2)->where('eventos.id',4)->where('eventos.status',1)->get();

                //EVENTO5
                $evento5 = DB::table('eventos')
                ->join('unidades', 'eventos.unidade_id', '=', 'unidades.id')
                ->select('eventos.*')->where('unidades.id',2)->where('eventos.id',5)->where('eventos.status',1)->get();

                 //EVENTO6
               $evento6 = DB::table('eventos')
               ->join('unidades', 'eventos.unidade_id', '=', 'unidades.id')
               ->select('eventos.*')->where('unidades.id',2)->where('eventos.id',6)->where('eventos.status',1)->get();

                //EVENTO7
                $evento7 = DB::table('eventos')
                ->join('unidades', 'eventos.unidade_id', '=', 'unidades.id')
                ->select('eventos.*')->where('unidades.id',2)->where('eventos.id',7)->where('eventos.status',1)->get();

                  //EVENTO8
                  $evento8 = DB::table('eventos')
                  ->join('unidades', 'eventos.unidade_id', '=', 'unidades.id')
                  ->select('eventos.*')->where('unidades.id',2)->where('eventos.id',8 )->where('eventos.status',1)->get();


                    
              // ---- GALERIA ----
                 
                  //IMG GALERIA 1
                 $galeria1 = DB::table('galeria')
                 ->join('unidades', 'galeria.unidade_id', '=', 'unidades.id')
                  ->select('galeria.*')->where('unidades.id',2)->where('galeria.id',1)->get();

                   //IMG GALERIA 2
                 $galeria2 = DB::table('galeria')
                 ->join('unidades', 'galeria.unidade_id', '=', 'unidades.id')
                  ->select('galeria.*')->where('unidades.id',2)->where('galeria.id',2)->get();

                   //IMG GALERIA 3
                 $galeria3= DB::table('galeria')
                 ->join('unidades', 'galeria.unidade_id', '=', 'unidades.id')
                  ->select('galeria.*')->where('unidades.id',2)->where('galeria.id',3)->get();

                   //IMG GALERIA 4
                 $galeria4 = DB::table('galeria')
                 ->join('unidades', 'galeria.unidade_id', '=', 'unidades.id')
                  ->select('galeria.*')->where('unidades.id',2)->where('galeria.id',4)->get();

                   //IMG GALERIA 5
                 $galeria5 = DB::table('galeria')
                 ->join('unidades', 'galeria.unidade_id', '=', 'unidades.id')
                  ->select('galeria.*')->where('unidades.id',2)->where('galeria.id',5)->get();

                   //IMG GALERIA 6
                   $galeria6 = DB::table('galeria')
                 ->join('unidades', 'galeria.unidade_id', '=', 'unidades.id')
                  ->select('galeria.*')->where('unidades.id',2)->where('galeria.id',6)->get();


                  //id Unidade FORMULARIO DE INSCRIÇÃO
                 $formunidade1 = DB::table('unidades')
                  ->select('unidades.*')->where('unidades.id',2)->get();
                  $formunidade2 = DB::table('unidades')
                  ->select('unidades.*')->where('unidades.id',2)->get();
                  $formunidade3 = DB::table('unidades')
                  ->select('unidades.*')->where('unidades.id',2)->get();
                  $formunidade4 = DB::table('unidades')
                  ->select('unidades.*')->where('unidades.id',2)->get();
                  $formunidade5 = DB::table('unidades')
                  ->select('unidades.*')->where('unidades.id',2)->get();
                  $formunidade6 = DB::table('unidades')
                  ->select('unidades.*')->where('unidades.id',2)->get();
                  $formunidade7 = DB::table('unidades')
                  ->select('unidades.*')->where('unidades.id',2)->get();
                  $formunidade8 = DB::table('unidades')
                  ->select('unidades.*')->where('unidades.id',2)->get();

                 
                  //unidades select HOME
        $unidade2 = DB::table('unidades') ->select('nome')->where('unidades.id',2)->get();
        $unidade3 = DB::table('unidades') ->select('nome')->where('unidades.id',3)->get();
        $unidade4 = DB::table('unidades') ->select('nome')->where('unidades.id',4)->get();
        $unidade5 = DB::table('unidades') ->select('nome')->where('unidades.id',5)->get();
        $unidade6 = DB::table('unidades') ->select('nome')->where('unidades.id',6)->get();
        $unidade7 = DB::table('unidades') ->select('nome')->where('unidades.id',7)->get();
        $unidade8 = DB::table('unidades') ->select('nome')->where('unidades.id',8)->get();
        $unidade9 = DB::table('unidades') ->select('nome')->where('unidades.id',9)->get();
        $unidade10 = DB::table('unidades') ->select('nome')->where('unidades.id',10)->get();
        $unidade11 = DB::table('unidades') ->select('nome')->where('unidades.id',11)->get();
        $unidade12 = DB::table('unidades') ->select('nome')->where('unidades.id',12)->get();
        $unidade13 = DB::table('unidades') ->select('nome')->where('unidades.id',13)->get();
        $unidade14 = DB::table('unidades') ->select('nome')->where('unidades.id',14)->get();
        $unidade15 = DB::table('unidades') ->select('nome')->where('unidades.id',15)->get();
        $unidade16 = DB::table('unidades') ->select('nome')->where('unidades.id',16)->get();
        $unidade17 = DB::table('unidades') ->select('nome')->where('unidades.id',17)->get();

                  
          
        
          
                  return view('unidade')
         ->with(compact('nome','noticia1','noticia2','noticia3','noticia4','bacharelado','tecnologo','licenciatura','ead','pos','sequencial','bachareladocount','tecnologocount','licenciaturacount','eadcount','poscount','seqcount','latitude','longitude','curso1bacharelado','evento1','evento2',
         'evento3','evento4','evento5','evento6','evento7','evento8','banner1','banner2','banner3','galeria1','galeria2','galeria3','galeria4','galeria5','galeria6','logomarca','unidade2','unidade3','unidade4','unidade5','unidade6','unidade7','unidade8','unidade9','unidade10','unidade11','unidade12','unidade13','unidade14','unidade15','unidade16','unidade17','formunidade1','formunidade2','formunidade3','formunidade4','formunidade5','formunidade6','formunidade7','formunidade8'));                  
    
   
        }
                  
    
   
        

       
        
}

