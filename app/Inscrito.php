<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscrito extends Model
{
    protected $fillable = [
        'nome', 'cpf', 'email','telefone'
    ];
}
