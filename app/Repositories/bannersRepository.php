<?php

namespace App\Repositories;

use App\Models\banners;
use App\Repositories\BaseRepository;

/**
 * Class bannersRepository
 * @package App\Repositories
 * @version November 26, 2019, 4:17 pm UTC
*/

class bannersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'img',
        'unidade_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return banners::class;
    }
}
