<?php

namespace App\Repositories;

use App\Models\unidades;
use App\Repositories\BaseRepository;

/**
 * Class unidadesRepository
 * @package App\Repositories
 * @version November 12, 2019, 2:49 pm UTC
*/

class unidadesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome',
        'calendario',
        'portal_cademico',
        'latitude',
        'longitude',
        'logradouro',
        'estado',
        'cidade',
        'bairro',
        'cep',
        'logomarca'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return unidades::class;
    }
}
