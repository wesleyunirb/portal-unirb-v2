<?php

namespace App\Repositories;

use App\Models\campanhas;
use App\Repositories\BaseRepository;

/**
 * Class campanhasRepository
 * @package App\Repositories
 * @version February 10, 2020, 4:11 pm UTC
*/

class campanhasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return campanhas::class;
    }
}
