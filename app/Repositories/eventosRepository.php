<?php

namespace App\Repositories;

use App\Models\eventos;
use App\Repositories\BaseRepository;

/**
 * Class eventosRepository
 * @package App\Repositories
 * @version January 14, 2020, 7:23 pm UTC
*/

class eventosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo_eventos_id',
        'unidade_id',
        'titulo',
        'local',
        'descricao',
        'data_evento',
        'horario',
        'limite_participantes',
        'data_inicio',
        'data_fim',
        'logomarca',
        'programacao',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return eventos::class;
    }
}
