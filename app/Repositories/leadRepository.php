<?php

namespace App\Repositories;

use App\Models\lead;
use App\Repositories\BaseRepository;

/**
 * Class leadRepository
 * @package App\Repositories
 * @version December 4, 2019, 5:54 pm UTC
*/

class leadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome',
        'cpf',
        'telefone',
        'email',
        'evento_id',
        'evento_tipo_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return lead::class;
    }
}
