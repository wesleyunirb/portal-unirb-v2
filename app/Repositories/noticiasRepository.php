<?php

namespace App\Repositories;

use App\Models\noticias;
use App\Repositories\BaseRepository;

/**
 * Class noticiasRepository
 * @package App\Repositories
 * @version November 26, 2019, 1:53 pm UTC
*/

class noticiasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'unidade_id',
        'titulo',
        'subtitulo',
        'data',
        'descricao',
        'image',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return noticias::class;
    }
}
