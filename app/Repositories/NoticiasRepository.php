<?php

namespace App\Repositories;

use App\Models\Noticias;
use App\Repositories\BaseRepository;

/**
 * Class NoticiasRepository
 * @package App\Repositories
 * @version November 20, 2019, 1:31 pm UTC
*/

class NoticiasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Noticias::class;
    }
}
