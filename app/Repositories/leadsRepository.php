<?php

namespace App\Repositories;

use App\Models\leads;
use App\Repositories\BaseRepository;

/**
 * Class leadsRepository
 * @package App\Repositories
 * @version February 10, 2020, 4:50 pm UTC
*/

class leadsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cpf',
        'nome',
        'telefone',
        'email',
        'campanha_id',
        'unidade_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return leads::class;
    }
}
