<?php

namespace App\Repositories;

use App\Models\curso;
use App\Repositories\BaseRepository;

/**
 * Class cursoRepository
 * @package App\Repositories
 * @version January 17, 2020, 12:58 pm UTC
*/

class cursoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'unidade_id',
        'tipo_cursos_id',
        'nome',
        'descricao',
        'portaria',
        'duracao',
        'turno',
        'mensalidade',
        'matriz_curricular',
        'qtd_vagas',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return curso::class;
    }
}
