<?php

namespace App\Repositories;

use App\Models\tipo_eventos;
use App\Repositories\BaseRepository;

/**
 * Class tipo_eventosRepository
 * @package App\Repositories
 * @version January 14, 2020, 7:15 pm UTC
*/

class tipo_eventosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tipo_eventos::class;
    }
}
