<?php

namespace App\Repositories;

use App\Models\tipo_evento;
use App\Repositories\BaseRepository;

/**
 * Class tipo_eventoRepository
 * @package App\Repositories
 * @version November 19, 2019, 12:47 pm UTC
*/

class tipo_eventoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tipo_evento::class;
    }
}
