<?php

namespace App\Repositories;

use App\Models\tipo_curso;
use App\Repositories\BaseRepository;

/**
 * Class tipo_cursoRepository
 * @package App\Repositories
 * @version November 13, 2019, 5:44 pm UTC
*/

class tipo_cursoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tipo_curso::class;
    }
}
