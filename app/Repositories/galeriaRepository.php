<?php

namespace App\Repositories;

use App\Models\galeria;
use App\Repositories\BaseRepository;

/**
 * Class galeriaRepository
 * @package App\Repositories
 * @version November 27, 2019, 11:30 am UTC
*/

class galeriaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'img',
        'titulo',
        'unidade_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return galeria::class;
    }
}
