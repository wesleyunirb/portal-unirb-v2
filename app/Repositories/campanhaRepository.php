<?php

namespace App\Repositories;

use App\Models\campanha;
use App\Repositories\BaseRepository;

/**
 * Class campanhaRepository
 * @package App\Repositories
 * @version February 10, 2020, 4:14 pm UTC
*/

class campanhaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return campanha::class;
    }
}
