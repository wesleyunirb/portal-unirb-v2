<?php

namespace App\Repositories;

use App\Models\unidade;
use App\Repositories\BaseRepository;

/**
 * Class unidadeRepository
 * @package App\Repositories
 * @version November 11, 2019, 6:17 pm UTC
*/

class unidadeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return unidade::class;
    }
}
