<?php

namespace App\Repositories;

use App\Models\Unidade;
use App\Repositories\BaseRepository;

/**
 * Class UnidadeRepository
 * @package App\Repositories
 * @version January 29, 2020, 7:43 pm UTC
*/

class UnidadeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Unidade::class;
    }
}
