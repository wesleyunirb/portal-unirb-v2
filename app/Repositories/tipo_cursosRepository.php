<?php

namespace App\Repositories;

use App\Models\tipo_cursos;
use App\Repositories\BaseRepository;

/**
 * Class tipo_cursosRepository
 * @package App\Repositories
 * @version January 14, 2020, 6:29 pm UTC
*/

class tipo_cursosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tipo_cursos::class;
    }
}
