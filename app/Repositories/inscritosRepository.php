<?php

namespace App\Repositories;

use App\Models\inscritos;
use App\Repositories\BaseRepository;

/**
 * Class inscritosRepository
 * @package App\Repositories
 * @version February 7, 2020, 4:04 pm UTC
*/

class inscritosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome',
        'cpf',
        'telefone',
        'email',
        'evento_id',
        'unidade_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return inscritos::class;
    }
}
