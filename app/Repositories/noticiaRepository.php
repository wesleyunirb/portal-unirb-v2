<?php

namespace App\Repositories;

use App\Models\noticia;
use App\Repositories\BaseRepository;

/**
 * Class noticiaRepository
 * @package App\Repositories
 * @version November 8, 2019, 1:17 pm UTC
*/

class noticiaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return noticia::class;
    }
}
