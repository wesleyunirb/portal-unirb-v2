<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class curso
 * @package App\Models
 * @version January 17, 2020, 12:58 pm UTC
 *
 * @property \App\Models\TipoCurso tipoCursos
 * @property \App\Models\Unidade unidade
 * @property integer unidade_id
 * @property integer tipo_cursos_id
 * @property string nome
 * @property string descricao
 * @property string portaria
 * @property string duracao
 * @property string turno
 * @property string mensalidade
 * @property string matriz_curricular
 * @property string qtd_vagas
 * @property boolean status
 */
class curso extends Model
{
    use SoftDeletes;

    public $table = 'curso';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'unidade_id',
        'tipo_cursos_id',
        'nome',
        'descricao',
        'portaria',
        'duracao',
        'turno',
        'mensalidade',
        'matriz_curricular',
        'qtd_vagas',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'unidade_id' => 'integer',
        'tipo_cursos_id' => 'integer',
        'nome' => 'string',
        'descricao' => 'string',
        'portaria' => 'string',
        'duracao' => 'string',
        'turno' => 'string',
        'mensalidade' => 'string',
        'matriz_curricular' => 'string',
        'qtd_vagas' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'unidade_id' => 'required',
        'tipo_cursos_id' => 'required',
        'nome' => 'required',
        'descricao' => 'required',
        'portaria' => 'required',
        'duracao' => 'required',
        'turno' => 'required',
        'mensalidade' => 'required',
        'matriz_curricular' => 'required',
        'qtd_vagas' => 'required',
        'status' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tipoCursos()
    {
        return $this->belongsTo(\App\Models\TipoCurso::class, 'tipo_cursos_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function unidade()
    {
        return $this->belongsTo(\App\Models\Unidade::class, 'unidade_id');
    }
}
