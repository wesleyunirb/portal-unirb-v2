<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class lead
 * @package App\Models
 * @version December 4, 2019, 5:54 pm UTC
 *
 * @property \App\Models\Evento evento
 * @property \App\Models\TipoEvento eventoTipo
 * @property string nome
 * @property string cpf
 * @property string telefone
 * @property string email
 * @property integer evento_id
 * @property integer evento_tipo_id
 */
class lead extends Model
{
    use SoftDeletes;

    public $table = 'lead';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nome',
        'cpf',
        'telefone',
        'email',
        'evento_id',
        'evento_tipo_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'cpf' => 'string',
        'telefone' => 'string',
        'email' => 'string',
        'evento_id' => 'integer',
        'evento_tipo_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nome' => 'required',
        'cpf' => 'required',
        'telefone' => 'required',
        'email' => 'required',
        'evento_id' => 'required',
        'evento_tipo_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function evento()
    {
        return $this->belongsTo(\App\Models\Evento::class, 'evento_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function eventoTipo()
    {
        return $this->belongsTo(\App\Models\TipoEvento::class, 'evento_tipo_id');
    }
}
