<?php 

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class noticia
 * @package App\Models
 * @version November 8, 2019, 1:17 pm UTC
 *
 */
class noticia extends Model
{
    use SoftDeletes;

    public $table = 'noticias';
    

    protected $dates = ['deleted_at'];

    protected $guarded = ['id', 'created_at', 'update_at','deleted_at'];
    public $fillable = ['nome','titulo','subtitulo','ano','descricao'];
        
  

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
