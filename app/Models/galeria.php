<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class galeria
 * @package App\Models
 * @version November 27, 2019, 11:30 am UTC
 *
 * @property \App\Models\Unidade unidade
 * @property string img
 * @property string titulo
 * @property integer unidade_id
 */
class galeria extends Model
{
    use SoftDeletes;

    public $table = 'galeria';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'img',
        'titulo',
        'unidade_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'img' => 'string',
        'titulo' => 'string',
        'unidade_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'img' => 'required',
        'titulo' => 'required',
        'unidade_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function unidade()
    {
        return $this->belongsTo(\App\Models\Unidade::class, 'unidade_id');
    }
}
