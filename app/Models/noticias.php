<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class noticias
 * @package App\Models
 * @version November 26, 2019, 1:53 pm UTC
 *
 * @property \App\Models\Unidade unidade
 * @property integer unidade_id
 * @property string titulo
 * @property string subtitulo
 * @property string data
 * @property string descricao
 * @property string image
 * @property boolean status
 */
class noticias extends Model
{
    use SoftDeletes;

    public $table = 'noticias';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'unidade_id',
        'titulo',
        'subtitulo',
        'data',
        'descricao',
        'image',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'unidade_id' => 'integer',
        'titulo' => 'string',
        'subtitulo' => 'string',
        'data' => 'string',
        'descricao' => 'string',
        'image' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'unidade_id' => 'required',
        'titulo' => 'required',
        'subtitulo' => 'required',
        'data' => 'required',
        'descricao' => 'required',
        'status' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function unidade()
    {
        return $this->belongsTo(\App\Models\Unidade::class, 'unidade_id');
    }
}
