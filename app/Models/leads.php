<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class leads
 * @package App\Models
 * @version February 10, 2020, 4:50 pm UTC
 *
 * @property string cpf
 * @property string nome
 * @property string telefone
 * @property string email
 * @property integer campanha_id
 * @property integer unidade_id
 */
class leads extends Model
{
    use SoftDeletes;

    public $table = 'leads';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'cpf',
        'nome',
        'telefone',
        'email',
        'campanha_id',
        'unidade_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cpf' => 'string',
        'nome' => 'string',
        'telefone' => 'string',
        'email' => 'string',
        'campanha_id' => 'integer',
        'unidade_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cpf' => 'required',
        'nome' => 'required',
        'telefone' => 'required',
        'email' => 'required'
    ];

    
}
