<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class inscritos
 * @package App\Models
 * @version February 7, 2020, 4:04 pm UTC
 *
 * @property \App\Models\Evento evento
 * @property \App\Models\Unidade unidade
 * @property string nome
 * @property string cpf
 * @property string telefone
 * @property string email
 * @property integer evento_id
 * @property integer unidade_id
 */
class inscritos extends Model
{
    use SoftDeletes;

    public $table = 'inscritos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nome',
        'cpf',
        'telefone',
        'email',
        'evento_id',
        'unidade_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'cpf' => 'string',
        'telefone' => 'string',
        'email' => 'string',
        'evento_id' => 'integer',
        'unidade_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nome' => 'required',
        'cpf' => 'required',
        'telefone' => 'required',
        'email' => 'required',
        'evento_id' => 'required',
        'unidade_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function evento()
    {
        return $this->belongsTo(\App\Models\Evento::class, 'evento_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function unidade()
    {
        return $this->belongsTo(\App\Models\Unidade::class, 'unidade_id');
    }
}
