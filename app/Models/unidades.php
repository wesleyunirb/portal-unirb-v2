<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class unidades
 * @package App\Models
 * @version November 12, 2019, 2:49 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection noticias
 * @property string nome
 * @property string calendario
 * @property string portal_cademico
 * @property string latitude
 * @property string longitude
 * @property string logradouro
 * @property string estado
 * @property string cidade
 * @property string bairro
 * @property string cep
 * @property string logomarca
 */
class unidades extends Model
{
    use SoftDeletes;

    public $table = 'unidades';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nome',
        'calendario',
        'portal_cademico',
        'latitude',
        'longitude',
        'logradouro',
        'estado',
        'cidade',
        'bairro',
        'cep',
        'logomarca'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'calendario' => 'string',
        'portal_cademico' => 'string',
        'latitude' => 'string',
        'longitude' => 'string',
        'logradouro' => 'string',
        'estado' => 'string',
        'cidade' => 'string',
        'bairro' => 'string',
        'cep' => 'string',
        'logomarca' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nome' => 'required',
        'calendario' => 'required',
        'portal_cademico' => 'required',
        'latitude' => 'required',
        'longitude' => 'required',
        'logradouro' => 'required',
        'estado' => 'required',
        'cidade' => 'required',
        'bairro' => 'required',
        'cep' => 'required',
        'logomarca' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function noticias()
    {
        return $this->hasMany(\App\Models\Noticia::class, 'unidade_id');
    }
}
