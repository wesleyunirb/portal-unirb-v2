<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class eventos
 * @package App\Models
 * @version January 14, 2020, 7:23 pm UTC
 *
 * @property \App\Models\TipoEvento tipoEventos
 * @property \App\Models\Unidade unidade
 * @property \Illuminate\Database\Eloquent\Collection leads
 * @property integer tipo_eventos_id
 * @property integer unidade_id
 * @property string titulo
 * @property string local
 * @property string descricao
 * @property string data_evento
 * @property string horario
 * @property string limite_participantes
 * @property string data_inicio
 * @property string data_fim
 * @property string logomarca
 * @property string programacao
 * @property boolean status
 */
class eventos extends Model
{
    use SoftDeletes;

    public $table = 'eventos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'tipo_eventos_id',
        'unidade_id',
        'titulo',
        'local',
        'descricao',
        'data_evento',
        'horario',
        'limite_participantes',
        'data_inicio',
        'data_fim',
        'logomarca',
        'programacao',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tipo_eventos_id' => 'integer',
        'unidade_id' => 'integer',
        'titulo' => 'string',
        'local' => 'string',
        'descricao' => 'string',
        'data_evento' => 'string',
        'horario' => 'string',
        'limite_participantes' => 'string',
        'data_inicio' => 'date',
        'data_fim' => 'date',
        'logomarca' => 'string',
        'programacao' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tipo_eventos_id' => 'required',
        'unidade_id' => 'required',
        'titulo' => 'required',
        'local' => 'required',
        'descricao' => 'required',
        'data_evento' => 'required',
        'horario' => 'required',
        'limite_participantes' => 'required',
        'data_inicio' => 'required',
        'data_fim' => 'required',
        'logomarca' => 'required',
        'programacao' => 'required',
        'status' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tipoEventos()
    {
        return $this->belongsTo(\App\Models\TipoEvento::class, 'tipo_eventos_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function unidade()
    {
        return $this->belongsTo(\App\Models\Unidade::class, 'unidade_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function leads()
    {
        return $this->hasMany(\App\Models\Lead::class, 'evento_id');
    }
}
