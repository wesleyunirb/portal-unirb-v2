﻿<!DOCTYPE html>
<html lang="pt">
<head>
<title>Portal UNIRB </title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Portal universitário UNIRB">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"><link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="css/main_styles.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link rel="stylesheet" type="text/css" href="css/blog_responsive.css">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">



<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!-- MASK-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>






<script>
		function envio()
		{
			
		var nome = document.getElementById("nome").value;
		var telefone = document.getElementById("telefone").value;
        var email = document.getElementById("email").value;
        var cidade = document.getElementById("cidade").value;
        var curso = document.getElementById("curso").value;
	
	
	
			if(nome,telefone,email,cidade,curso == ""){
		  swal("Erro ao Enviar!", "Por Favor preencha os campos corretamente", "error");
	
	
		}else{
		swal("Enviado com sucesso !", "Obrigado! Entraremos em contato", "success");
		 // Salva a lista alterada
		}
			
		}

		function enviar()
		{
			
		var nome = document.getElementById("nome2").value;
        var email = document.getElementById("email2").value;
        var assunto = document.getElementById("assunto1").value;
	
	
	
			if(nome,email,assunto == ""){
		  swal("Erro ao Enviar!", "Por Favor preencha os campos corretamente", "error");
	
	
		}else{
		swal("Enviado com sucesso !", "Obrigado! Entraremos em contato", "success");
		 // Salva a lista alterada
		}
			
		}
		</script>

<?php
			$situacao_usuario = "pendente";
			if($situacao_usuario == "pendente"){ ?>
				<script>
					$(document).ready(function(){
						$('#myModal').modal('show');
					});
				</script>
			<?php } ?>

			
			
			<script>

			function Mudarestado() {
        var display = document.getElementById(label1);
        if(display == "none")
            document.getElementById(el).style.display = 'block';
        else
            document.getElementById(el).style.display = 'none';
    }
	</script>


	

			

</head>




<body>



<div class="super_container">

<!-- Modal REVISTA ELETRONICA -->
<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="font-size:20px">Revistas Eletrônicas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<ul>
<li>
<a href="images/revistas/Revista Cientifica - Vol 1 N. 2 - 2008-2009.pdf" target="_blank"><button type="button"  class="btn btn-primary btn-lg btn-block" ><span style="font-size:18px">Revista Unirb Volume I Número 2 Ano: 2008-2009</span></button></a><br>
<a href="images/revistas/Revista Jurídica UNIRB - Ano I - Julho 2011.pdf" target="_blank"><button type="button"  class="btn btn-primary btn-lg btn-block"><span style="font-size:18px">Revista Jurídica UNIRB - Ano I - Julho 2011.pdf</span></button></a><br>
<a href="images/revistas/Revista Científica da UNIRB - Ano III - Junho 2011.pdf" target="_blank"><button type="button"  class="btn btn-primary btn-lg btn-block"><span style="font-size:18px">Revista Científica da UNIRB - Ano III - Junho 2011.pdf</span></button></a><br>
<a href="images/revistas/revistaano42012.pdf" target="_blank"><button type="button"  class="btn btn-primary btn-lg btn-block"><span style="font-size:18px">Tema: Multidisciplinar  Ano 2012</span></button></a><br>


</li>
</ul>     
 </div>
     
    </div>
  </div>
</div>

<!-- Modal BIBLIOTECA DIGITAL -->
<div class="modal fade" id="modalExemplobiblioteca" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="font-size:20px">Biblioteca Digital</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<ul>
<li>
<a href="images/revistas/revistaano42012.pdf" target="_blank"><button type="button"  class="btn btn-secondary btn-lg btn-block">Tema: Multidisciplinar  Ano 2012</button></a><br>
<a href="images/revistas/revistaano42012.pdf" target="_blank"><button type="button"  class="btn btn-secondary btn-lg btn-block">Tema: Multidisciplinar  Ano 2013</button></a>
</li>
</ul>     
 </div>
     
    </div>
  </div>
</div>



	<!-- Header -->

	<header class="header">
			
		<!-- Top Bar -->
		<div class="top_bar" style="background-color:black">
			<div class="top_bar_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
								<ul class="top_bar_contact_list">

								<li>
	<i class="fa fa-map-marker" aria-hidden="true"></i>
	<div>
	<span class="font-txt-header ouvidoria"><a href="" data-toggle="modal" data-target="#myModal"><b style="font-size:14px"> Escolha a sua Unidade</b></a><span></div>
     </li>
			
	<li>
	<i class="fa fa-phone" aria-hidden="true"></i>
	<div><span class="font-txt-header ouvidoria"><a href="ouvidoria"><b>OUVIDORIA</b></a><span></div>
     </li>
      &nbsp;&nbsp;
	 <!-- <li>
		<div><span class="font-txt-header"><a href=""><b>RESULTADO DO VESTIBULAR</b></a></span></div>
		</li>-->
	<!--<li>
	<div><span class="font-txt-header"><a href="ouvidoria"><b style="font-size:13px">EDITAIS</b></a></div>

       </li>-->
	</ul>
	<div class="top_bar_login ml-auto">
									<div class="login_button"><a href="https://unirb1.portaldominus.com.br/login.php"  target="_blank" ><i class="fa fa-graduation-cap" aria-hidden="true"></i> PORTAL ACADÊMICO </a></div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>

		<!-- Header Content -->
		<div class="header_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo_container">
								<a href="/">
									<div class="logo_text logo"><img src="images/home/redeunirb.png" ></div>
								</a>
							</div>
							<nav class="main_nav_contaner ml-auto">
								<ul class="main_nav">
								<li>
                                 <div class="navbar-nav">
                                 <div class="dropdown">
                                 <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">A UNIRB</a> 
                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                 <a class="dropdown-item" href="institucional" style="color:black">Institucional</a>
                                <a class="dropdown-item" href="https://docs.google.com/forms/d/1nQpnH2eQ7SN92IiuC_Kn_IygQhG28MU5b2gQzh5Nt-g/viewform?edit_requested=true#responses"  target="_blank" style="color:black;font-size:15px">Egressos</a>
                                <a class="dropdown-item" href="http://portal.unirb.edu.br/cpa_novo/cpa_administrativo_ssa.php"  target="_blank" style="color:black;font-size:15px">CPA</a>
								<a class="dropdown-item" href="/linhadotempo" style="color:black;font-size:15px">Linha do Tempo</a>
								<a class="dropdown-item" href="/laboratorios" style="color:black;font-size:15px">Laboratórios</a>
								<a class="dropdown-item" href="/biblioteca" style="color:black;font-size:15px">Biblioteca</a>
								<a class="dropdown-item" href="/egresso" style="color:black;font-size:15px">Programa de apoio ao Egresso</a>


                               </div>
								  </div>
								</li>

								<li>
                                 <div class="navbar-nav">
                                 <div class="dropdown">
                                 <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" >Cursos</a> 
                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								 <a class="dropdown-item" href="/graduacao" style="color:black;font-size:15px">Graduação</a>
								 <a class="dropdown-item" href="/posgraduacao" style="color:black;font-size:15px">Pós-Graduação</a>
								 <a class="dropdown-item" href="http://betaead.unirb.edu.br/" style="color:black;font-size:15px">EAD</a>

                               </div>
								  </div>
								</li>

									<li>
                                 <div class="navbar-nav">
                                 <div class="dropdown">
                                 <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" >Estude na UNIRB</a> 
                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="/creditoestudantil" style="color:black;font-size:15px">Crédito Estudantil</a>
								<a class="dropdown-item" href="/convenios" style="color:black;font-size:15px">Convênios</a>
								<a class="dropdown-item" href="/estagio" style="color:black;font-size:15px">Programas de Estágio</a>
								<a class="dropdown-item" href="/intercambio" style="color:black;font-size:15px">Intercâmbio</a>

                               </div>
								  </div>
								</li>

								<li>
                                 <div class="navbar-nav">
								 <div class=""><button class="btn btn-success"><b><a href="vestibular" style="color:white;font-size:13px" >INSCREVA-SE</a></b></button></div>

                                 
								  </div>
								</li>

									
								</ul>



									<!-- Hamburger -->

									<div class="hamburger menu_mm">
									<i class="fa fa-bars menu_mm" aria-hidden="true" style="color:black"></i>
								</div>	
							</nav>
								
							</nav>

						</div>
					</div>
				</div>
			</div>
		</div>

			
	</header>

	  <!-- Modal pop up -->
	  <div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:3%">
		<div class="modal-dialog " role="document" style="background-color:#1f466f">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:#1e426e">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:red"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
					<div class="container-fluid">
    <div class="row">
        <div class="col-12 mt-3">
            <div class="">
                <div class="card-horizontal" >
                    <div class="img-square-wrapper">
                        <img class="img-popup" src="images/home/redeunirb.png" alt="Card image cap">
                    </div>
                    <div class="card-body pose-popup" >
                        <h4 class="card-title" style="font-size:25px">Seja Bem Vindo !</h4>
						<div class="form-group">
						<div class="dropdown">                               
	
    <div class="btn-group">
       <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		<i class="fa fa-map-marker" style="color:black"></i>&nbsp;
		 <b style="color:black">Escolha a sua unidade</b> <span class="caret"></span></button>
         <ul class="dropdown-menu scrollable-menu" role="menu">
		 
		  <!--Unidade Alagoinhas-->
		  @foreach ($unidade2 as $uni) 
         <li> <a class="dropdown-item" href="Alagoinhas">{{$uni->nome}}</a></li>
	     @endforeach

		 <!--Unidade Aracaju-->
		 @foreach ($unidade3 as $uni) 
         <li> <a class="dropdown-item" href="Aracaju">{{$uni->nome}}</a></li>
	     @endforeach

		 <!--Unidade FBT- Aracaju-->
		 @foreach ($unidade4 as $uni) 
         <li> <a class="dropdown-item" href="FBTAracaju">{{$uni->nome}}</a></li>
	     @endforeach

		 <!--Unidade Arapiraca-->
		 @foreach ($unidade5 as $uni) 
         <li> <a class="dropdown-item" href="Arapiraca">{{$uni->nome}}</a></li>
	     @endforeach

		 <!--Unidade Barreiras-->
		 @foreach ($unidade6 as $uni) 
         <li> <a class="dropdown-item" href="Barreiras">{{$uni->nome}}</a></li>
	     @endforeach


		 <!--Unidade Camaçari-->
		 @foreach ($unidade7 as $uni) 
         <li> <a class="dropdown-item" href="Camacari">{{$uni->nome}}</a></li>
	     @endforeach

		  <!--Unidade Fortaleza-->
		  @foreach ($unidade8 as $uni) 
         <li> <a class="dropdown-item" href="Fortaleza">{{$uni->nome}}</a></li>
	     @endforeach

		  <!--Unidade Feira de Santana-->
		  @foreach ($unidade9 as $uni) 
         <li> <a class="dropdown-item" href="Feiradesantana">{{$uni->nome}}</a></li>
	     @endforeach

		  <!--Unidade FBT- Feira de Santana-->
		  @foreach ($unidade10 as $uni) 
         <li> <a class="dropdown-item" href="FBTFeiradesantana">{{$uni->nome}}</a></li>
	     @endforeach

		  <!--Unidade Juazeiro-->
		  @foreach ($unidade11 as $uni) 
         <li> <a class="dropdown-item" href="Juazeiro">{{$uni->nome}}</a></li>
	     @endforeach

		   <!--Unidade Maceió-->
		   @foreach ($unidade12 as $uni) 
         <li> <a class="dropdown-item" href="Maceio">{{$uni->nome}}</a></li>
	     @endforeach

		   <!--Unidade Mossoró-->
		   @foreach ($unidade13 as $uni) 
         <li> <a class="dropdown-item" href="Mossoro">{{$uni->nome}}</a></li>
	     @endforeach

		  <!--Unidade Natal-->
		  @foreach ($unidade14 as $uni) 
         <li> <a class="dropdown-item" href="Natal">{{$uni->nome}}</a></li>
	     @endforeach

		 <!--Unidade Parnaíba-->
		 @foreach ($unidade15 as $uni) 
         <li> <a class="dropdown-item" href="Parnaiba">{{$uni->nome}}</a></li>
	     @endforeach

		 <!--Unidade Salvador-->
		 @foreach ($unidade16 as $uni) 
         <li> <a class="dropdown-item" href="Salvador">{{$uni->nome}}</a></li>
	     @endforeach

         <!--Unidade Teresina-->
         @foreach ($unidade17 as $uni) 
         <li> <a class="dropdown-item" href="Teresina">{{$uni->nome}}</a></li>
	     @endforeach
		
	    
                </ul>
            </div>

    </div>
  </div>
  </div>

                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>           </div>
                    
                </div>
            </div>
      

	<!-- Menu -->

	<div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<nav class="menu_nav">
			<ul class="menu_mm">
			<div class="logo_container">
			<a href="/">
		    <div class="logo_text logo"><img src="images/home/logo.png" style="width:130%" ></div>
			</a>
		    </div>
				
			</ul>

			<ul class="menu_mm">
			<div class="dropdown">
            <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="font-size:18px">A UNIRB</a> 
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" href="institucional" style="color:black">Institucional</a>
                                <a class="dropdown-item" href="https://docs.google.com/forms/d/1nQpnH2eQ7SN92IiuC_Kn_IygQhG28MU5b2gQzh5Nt-g/viewform?edit_requested=true#responses"  target="_blank" style="color:black;font-size:15px">Egressos</a>
                                <a class="dropdown-item" href="http://portal.unirb.edu.br/cpa_novo/cpa_administrativo_ssa.php"  target="_blank" style="color:black;font-size:15px">CPA</a>
								<a class="dropdown-item" href="/linhadotempo" style="color:black;font-size:15px">Linha do Tempo</a>
								<a class="dropdown-item" href="/laboratorios" style="color:black;font-size:15px">Laboratórios</a>
								<a class="dropdown-item" href="/biblioteca" style="color:black;font-size:15px">Biblioteca</a>
								<a class="dropdown-item" href="/biblioteca" style="color:black;font-size:15px">Egressos e Estudantes</a>


         </div>
				
			</ul>

			<ul class="menu_mm">
			<div class="dropdown">
            <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="font-size:18px">Cursos</a> 
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" href="/graduacao" style="color:black;font-size:15px">Graduação</a>
								 <a class="dropdown-item" href="/posgraduacao" style="color:black;font-size:15px">Pós-Graduação</a>
								 <a class="dropdown-item" href="http://betaead.unirb.edu.br/" style="color:black;font-size:15px">EAD</a>

         </div>
				
			</ul>

			<ul class="menu_mm">
			<div class="dropdown">
            <a class="nav-item nav-link active  dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="font-size:18px">Estude na UNIRB</a> 
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="/creditoestudantil" style="color:black;font-size:15px">Crédito Estudantil</a>
								<a class="dropdown-item" href="/convenios" style="color:black;font-size:15px">Convênios</a>
								<a class="dropdown-item" href="/estagio" style="color:black;font-size:15px">Programas de Estágio</a>
								<a class="dropdown-item" href="/intercambio" style="color:black;font-size:15px">Intercâmbio</a>

		 </div>
		 <br>
				
			</ul>

			<ul class="menu_mm">
			<div class=""><a href="https://unirb1.portaldominus.com.br/login.php" style="color:black;font-size:18px" target="_blank"><i class="fa fa-graduation-cap" aria-hidden="true"></i> PORTAL DO ALUNO</a></div>

				
			</ul>

				</ul><br>

			<ul class="menu_mm">
			<div class="dropdown">
			<div class=""><button class="btn btn-success"><b><a href="vestibular" style="color:white">INSCREVA-SE</a></b></button></div>
           

				
			</ul>
		</nav>
	</div>
	

	
	<!-- Slider --><br>
	

	<div id="carouselExampleControls" class="carousel slide pose-slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
	@foreach ($banner1 as $banner1) 
	<div class="blog_post_image"><img src="uploads/{{$banner1->img}}" alt="" style="width:100%"class=" img-responsive"></div>
	@endforeach   
	 </div>
    <div class="carousel-item">
	@foreach ($banner2 as $banner2) 
	<div class="blog_post_image"><img src="uploads/{{$banner2->img}}" alt="" style="width:100%"class=" img-responsive"></div>
	@endforeach  
    </div>
   <div class="carousel-item">
   @foreach ($banner3 as $banner3) 
	<div class="blog_post_image"><img src="uploads/{{$banner3->img}}" alt="" style="width:100%"class=" img-responsive"></div>
	@endforeach     </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


	<!--home -->
	<div class="card-deck pose-card-deck">
  <div class="card" style="background-color:#1e426e">
  <div class="card-body">
	<h5 class="card-title txt25" style="color:white"><b>Graduação</b></h5>
      <p class="card-text txt15" style="color:white">Confira nossas opções de cursos em diversas áreas de conhecimento da UNIRB! <a href="graduacao"><i class="fa fa-arrow-circle-right fa-2x " aria-hidden="true" style="margin-left:18%"></i></a></p>
    </div>
  </div>
  <div class="card" style="background-color:#9a999e">
    <div class="card-body">
	<h5 class="card-title txt25" style="color:white">Pós-Graduação</h5>
	<p class="card-text txt15" style="color:white">Garanta a sua pós e estude com os melhores professores. <a href="posgraduacao"><i class="fa fa-arrow-circle-right fa-2x " aria-hidden="true" style="margin-left:60%"></i></a></p>
    </div>
  </div>
  <div class="card" style="background-color:#9f1322">
    <div class="card-body">
	<h5 class="card-title txt25" style="color:white">EAD</h5>
	<p class="card-text txt15" style="color:white">Conheça os cursos online e adquira o mesmo diploma da graduação presencial. <a href="http://betaead.unirb.edu.br/"><i class="fa fa-arrow-circle-right fa-2x " aria-hidden="true" style="margin-left:15%"></i></a></p>
    </div>
  </div>
  <div class="card" style="background-color:#25a0bb">
    <div class="card-body">
	<h5 class="card-title txt25" style="color:white">Portal Acadêmico</h5>
      <p class="card-text txt15" style="color:white">Acesse a sua rede personalizada e acompanhe as informações do seu curso.<a href="https://unirb1.portaldominus.com.br/login.php"><i class="fa fa-arrow-circle-right fa-2x " aria-hidden="true" style="margin-left:20%"></i></a></p>
    </div>
  </div>
</div>




	<!-- Features -->

	<center>
		<div class=" pose-motivos">
						<h2 class="color-blue txt50" >Por que estudar na UNIRB?</h2>
						<p class="txt15" style="color:black">A universidade UNIRB está a mais de 18 anos entre as melhores instituições de ensino superior do país.</p>
					</div>
				</center>

			<center>
				<div class="row features_row col-md-10"  style="margin-top:2%">
				
				<!-- Features Item -->
				<div class="col-lg-2 feature_col" >
					<div class="feature text-center trans_400">
						<div class="feature_icon "><img src="images/home/google.png" alt=""></div>
						<h3 class="feature_title" style="margin-top:70%"><b>Google Driver Ilimitado</b></h3>
					</div>
				</div>

				<!-- Features Item -->
				<div class="col-lg-2 feature_col">
				<div class="feature text-center trans_400">
						<div class="feature_icon"><img src="images/home/discinterat.png" alt=""></div><br>
						<h3 class="feature_title" style="margin-top:55%"><b>Disciplinas Interativas</b></h3>
					</div>
				</div>

				<!-- Features Item -->
				<div class="col-lg-2 feature_col">
					<div class="feature text-center trans_400">
						<div class="feature_icon"><img src="images/home/biblioteca.png" alt=""></div><br>
						<h3 class="feature_title" style="margin-top:55%"><b>Biblioteca Virtual</b></h3>
					</div>
				</div>

				<!-- Features Item -->
				<div class="col-lg-2 feature_col">
				<div class="feature text-center trans_400">
						<div class="feature_icon"><img src="images/home/laboratorios.png" alt=""></div><br>
						<h3 class="feature_title" style="margin-top:55%"><b>Laboratórios Completos</b></h3>
					</div>
				</div>

				<!-- Features Item -->
				<div class="col-lg-2 feature_col">
					<div class="feature text-center trans_400">
						<div class="feature_icon"><img src="images/home/aulaspraticas.png" alt=""></div><br>
						<h3 class="feature_title" style="margin-top:55%"><b>Aulas Práticas Externas</b></h3>
					</div>
				</div>

				<!-- Features Item -->
				<div class="col-lg-2 feature_col">
				<div class="feature text-center trans_400">
						<div class="feature_icon "><img src="images/home/googlefor.png" alt=""></div>
						<h3 class="feature_title" style="margin-top:70%"><b>Google For Education</b></h3>
					</div>
				</div>

			</div>
		</div>
	</div>
</center>
	


	<!-- Blog -->
<div class="container col-md-12 " style="background-color:#1e426e;box-shadow: 10px 5px 5px #363636;">
	<div class="row pose-motivos">
	<div class="col">
	<br><br>
	<div class="section_title_container text-center">
	<h2 class="section_title txt50 "  style="color:white" >Acontece na UNIRB
	</h2>
					</div>
				</div>
			</div>


	<div class="blog  " style="background-color:#1e426e;">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="blog_post_container pose-blog ">
					
						<!-- Blog Post -->
						
						<div class="blog_post trans_200  " >
						<center>	<div class="blog_post_image  "><img src="images/home/redeunirb.png" alt="" style=" width:50%"></div><center>
							<div class="blog_post_body">
								<div class="blog_post_title txt25" style="color:black"><ul>
										<li style="color:black">	
										
                                         <p class="" style="color:black;font-size:18px"><b>Revista Cientifica</b></p>
							 	         </li>
									</ul>
								</div>
								
							
								<div class="blog_post_text">
									<p class="txt15" style="color:black">
									
                                         <p class="txt18" style="color:black">Unirb lança revista com as novidades</p>
							 	         </li></p>
								</div>
								
								<button class="btn btn-primary" data-toggle="modal" data-target="#modalExemplo"> ver revistas</button>
								
								
							</div>
						</div>
					

						<!-- Blog Post -->
						<div class="blog_post trans_200 ">
						@foreach ($noticia1 as $not1) 
							<div class="blog_post_image"><img src="uploads/{{$not1->image}}" alt=""></div>
							
							<div class="blog_post_body">
							
								<div class="blog_post_title txt25" style="color:black"><ul>
										<li style="color:black">	
										
                                         <p class="" style="color:black;font-size:18px"><b>{{$not1->titulo}}</b></p>
							 	        </li>
									</ul>
								</div>
								
								<div class="blog_post_meta">
									<ul>
										<li style="color:black">	
										
                                         <p class="txt15" style="color:black">{{$not1->data}}</p>
							 	         </li>
									</ul>
								</div>
								<div class="blog_post_text">
									<p class="txt15" style="color:black">
									 
                                         <p class="txt18" style="color:black">{{$not1->subtitulo}}</p>
							 	         </li></p>
								</div>

								<div class="blog_post_text">
									<p class="txt15" style="color:black">
									
                                         <input type="hidden" class="text" style="color:black" id="inputnot2" value="{{$not1->status}}"></input>
							 	         </li></p>
								</div>
								@endforeach
								<a href="noticia1"><button class="btn btn-primary"> Saiba Mais</button></a>
								</div>
							
						</div>

						<!-- Blog Post -->
						<div class="blog_post trans_200 ">
						@foreach ($noticia2 as $not2) 
							<div class="blog_post_image"><img src="uploads/{{$not2->image}}" alt=""></div>
												
							<div class="blog_post_body">
							<div class="blog_post_title txt25" style="color:black"><ul>
										<li style="color:black">	
                                         <p class="" style="color:black;font-size:18px"><b>{{$not2->titulo}}</b></p>
							 	         </li>
									</ul>
								</div>
								
								<div class="blog_post_meta">
									<ul>
										<li style="color:black">	
                                         <p class="txt15" style="color:black">{{$not2->data}}</p>
							 	         </li>
									</ul>
								</div>
								<div class="blog_post_text">
								
									<p class="txt15" style="color:black">
                                         <p class="txt18" style="color:black">{{$not2->subtitulo}}</p>
							 	         </li></p>
								</div>
								<div class="blog_post_text">
								
									<p class="txt15" style="color:black">
                                         <input type="hidden" style="color:black" id="inputnot3" value="{{$not2->status}}"></input>
							 	         </li></p>
								</div>

								@endforeach
								<a href="noticia2"><button class="btn btn-primary"> Saiba Mais</button></a>
							</div>
						</div>

							<!-- Blog Post -->
							<div class="blog_post trans_200 ">
							@foreach ($noticia3 as $not3) 
							<div class="blog_post_image"><img src="uploads/{{$not3->image}}" alt=""></div>
								<div class="blog_post_body">
							<div class="blog_post_title txt25" style="color:black"><ul>
										<li style="color:black">	
                                         <p class="" style="color:black;font-size:18px"><b>{{$not3->titulo}}</b></p>
							 	         </li>
									</ul>
								</div>
								
								<div class="blog_post_meta">
									<ul>
										<li style="color:black">	
                                         <p class="txt15" style="color:black">{{$not3->data}}</p>
							 	         </li>
									</ul>
								</div>
								<div class="blog_post_text">
									<p class="txt15" style="color:black">
									 
                                         <p class="txt18" style="color:black">{{$not3->subtitulo}}</p>
							 	         </li></p>
								</div>

								<div class="blog_post_text">
								
									<p class="txt15" style="color:black">
                                         <input type="hidden" style="color:black" id="inputnot4" value="{{$not3->status}}"></input>
							 	         </li></p>
								</div>
								@endforeach

								<a href="noticia3"><button class="btn btn-primary"> Saiba Mais</button></a>

							</div>
						</div>

							<!-- Blog Post -->
							<div class="blog_post trans_200 ">
						@foreach ($noticia4 as $not4) 
							<div class="blog_post_image"><img src="uploads/{{$not4->image}}" alt=""></div>
							
							<div class="blog_post_body">
							<div class="blog_post_title txt25" style="color:black"><ul>
										<li style="color:black">	
                                         <p class="" style="color:black;font-size:18px"><b>{{$not4->titulo}}</b></p>
							 	        </li>
									</ul>
								</div>
								
								<div class="blog_post_meta">
									<ul>
										<li style="color:black">	
                                         <p class="txt15" style="color:black">{{$not4->data}}</p>
							 	         </li>
									</ul>
								</div>
								<div class="blog_post_text">
									<p class="txt15" style="color:black">
                                         <p class="txt18" style="color:black">{{$not4->subtitulo}}</p>
							 	         </li></p>
								</div>

								<div class="blog_post_text">
								
									<p class="txt15" style="color:black">
                                         <input type="hidden" style="color:black" id="inputnot5" value="{{$not4->status}}"></input>
							 	         </li></p>
								</div>
								@endforeach
								<a href="noticia4"><button class="btn btn-primary"> Saiba Mais</button></a>

							</div>
						</div>


						<!-- Blog Post -->
						<div class="blog_post trans_200">
						@foreach ($noticia5 as $not5) 
							<div class="blog_post_image"><img src="uploads/{{$not5->image}}" alt=""></div>
							<div class="blog_post_body">
							<div class="blog_post_title txt25" style="color:black"><ul>
										<li style="color:black">	
                                         <p class="" style="color:black;font-size:18px"><b>{{$not5->titulo}}</b></p>
							 	         </li>
									</ul>
								</div>
								
								<div class="blog_post_meta">
									<ul>
										<li style="color:black">	
                                         <p class="txt15" style="color:black">{{$not5->data}}</p>
							 	         </li>
									</ul>
								</div>
								<div class="blog_post_text">
									<p class="txt15" style="color:black">
                                         <p class="txt18" style="color:black">{{$not5->subtitulo}}</p>
							 	         </li></p>
								</div>
								@endforeach
								<a href="noticia5"><button class="btn btn-primary"> Saiba Mais</button></a>

							</div>
						</div>

					</div>
				</div>
			</div>
			
		</div>
	</div>
	</div>

	
	<!--INFRA-->
	<div class="pose-infra">
				
					<div class="section_title_container text-center col-md-12">
						<h2 class="section_title color-blue txt50"><b>Infraestrutura</b></h2>
						
				</div>
			<br>
			<center><div class="col-md-8">
			<p  class="text-justify txt15" style="color:black">O Centro Universitário UNIRB, dispõe de uma completa infraestrutura em laboratórios nas área de saúde, comunicação, engenharia, informática e biblioteca atendendo às necessidades dos alunos durante todo o período acadêmico.</p>
			</div></center>
			<br>

	<!-- Partners -->
	

	<div class="partners " >
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="partners_slider_container">
						<div class="owl-carousel owl-theme partners_slider">

							<!-- Partner Item -->
							<div class="owl-item partner_item"><img src="images/laboratorio/anatomia.jpg"></div>

							<!-- Partner Item -->
							<div class="owl-item partner_item"><img src="images/laboratorio/comunicacao.jpg" alt=""></div>

							<!-- Partner Item -->
							<div class="owl-item partner_item"><img src="images/laboratorio/saude.jpg" alt=""></div>

							<!-- Partner Item -->
							<div class="owl-item partner_item"><img src="images/laboratorio/engenharia.jpg" alt=""></div>

							<!-- Partner Item -->
							<div class="owl-item partner_item"><img src="images/laboratorio/educacaofisica.jpg" alt=""></div>

							<!-- Partner Item -->
							<div class="owl-item partner_item"><img src="images/laboratorio/quimica.jpg" alt=""></div>

							<!-- Partner Item -->
							<div class="owl-item partner_item"><img src="images/laboratorio/pedagogia.jpg" alt=""></div>

							
						

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Newsletter -->

	<div class="newsletter pose-preencha" >
		<div class="newsletter_background parallax-window" data-parallax="scroll"  data-speed="0.8" style="background-color:#363636" ></div>
		<div class="container">
		<center><div><p class="txt15" style="color:white">Preencha os dados abaixo e receba mais informações sobre o seu curso</p> </div></center>
			<div class="row pose-curso">
				<div class="col">
					<div class="newsletter_container d-flex flex-lg-row flex-column align-items-center justify-content-start">

	<!-- Newsletter Content -->
	<div class="newsletter_content col-md-12 ">
	    <div class="newsletter_subtitle" style="font-size:22px">Quer estudar na  </div>
	    <div class="newsletter_title" style="font-size:30px" >UNIRB ?</div>
	</div>

	<!-- Newsletter Form -->
	<div class="newsletter_form_container  col-md-12">
	{!! Form:: open(['route'=>'mail.store1', 'method' => 'POST']) !!}       
     <div class="form-row">
       <div class="col-md-2 ">
         <label  style="color:white"><b>Nome</b></label>
     <input type="text" class="form-control" id="nome" name="nome"  required>
     
	</div>
	
    <div class="col-md-2">
	<label  style="color:white"><b>Email</b></label>
      <input type="email" class="form-control" id="email" name="email"   required>
	</div>

	<div class="col-md-2">
	<label  style="color:white"><b>Telefone</b></label>
      <input type="text" class="form-control" id="telefone" name="telefone" required>
	</div>

	<div class="col-md-2">
	<label  style="color:white"><b>Cidade</b></label>
      <input type="text" class="form-control" id="cidade" name="cidade"  required>
	</div>

	<div class="col-md-2">
	<label  style="color:white"><b>Curso</b></label>
      <input type="text" class="form-control" id="curso" name="curso" required>
	</div>
	
	<div class="col-md-2" style="margin-top:28px;">
       <button class="btn btn-warning" type="submit" onclick="envio()"><b style="color:white">Enviar</b></button>     
	</div>

  </div>


  {!!Form:: close()!!}                  
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	@include('partials._footer1')
</div>

<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>


<script>

$(document).ready(function(){

  $('#telefone').mask('(99) 9 9999-9999');
  
});
</script>





</body>
</html>