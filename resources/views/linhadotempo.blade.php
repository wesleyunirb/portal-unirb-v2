﻿<title>Portal UNIRB | Linha do Tempo</title>

@include('partials._head')
<body>
@include('partials._header')

<div class="container pose-linha">
            <h4 class="txt50" style="color:#1f466f">Linha do Tempo - A UNIRB</h4>
            <div class="row pose-ano">
                <div class="col-md-12">
                    <div class="main-timeline4">
                        <div class="timeline">
                            <a class="timeline-content">
                                <span class="year">2002</span>
                                <div class="inner-content text-justify">
                                    <h3 class="title"></h3>
                                    <p class="description text-justify" style="color:black">
                                    <b>Credenciamento da Faculdade Regional da Bahia FARB/UNIRB e autorização dos cursos de Administração Pública e Administração com habilitação em Comércio Exterior, com sede no Colégio Nossa Senhora das Mercês, no Bairro Campo Grande. </b>
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="timeline">
                            <a  class="timeline-content">
                                <span class="year">2006</span>
                                <div class="inner-content text-justify">
                                    <h3 class="title"></h3>
                                    <p class="description text-justify" style="color:black">
                                   <b> Aquisição da sede própria, localizada na Avenida Tamburugy, 474 – Patamares, onde funciona atualmente. Credenciamento da segunda unidade, a Faculdade Regional de Alagoinhas (FARAL).</b>
                                    </p>
                                </div>
                            </a>
                        </div>

                        <div class="timeline">
                            <a  class="timeline-content">
                                <span class="year">2013</span>
                                <div class="inner-content text-justify">
                                    <h3 class="title"></h3>
                                    <p class="description" style="color:black">
                                    <b> Início da expansão com a aquisição da unidade de Feira de Santana–BA.</b>
                                    </p>
                                </div>
                            </a>
                        </div>
                        
                        <div class="timeline float-left">
                            <a  class="timeline-content">
                                <span class="year">2017</span>
                                <div class="inner-content text-justify">
                                    <h3 class="title"></h3>
                                    <p class="description" style="color:black"> 
                                    <b>Transformação da Faculdade Regional da Bahia em Centro Universitário UNIRB.</b>
                                    </p>
                                </div> 
                                <br> <br> <br>
                            </a>
                        </div>
                        
                        <div class="timeline mt-1">   
                            <a  class="timeline-content ">   <br> 
                                <span class="year mt-5">2019</span>  
                                <div class="inner-content text-justify">
                                    <h3 class="title"></h3>
                                    <p class="description text-justify" style="color:black">
                                    <b>Credenciamento do Centro Universitário UNIRB – Salvador para a oferta de cursos superiores na modalidade à distância – EAD;<br>
                                  Transformação da Faculdade Regional de Alagoinhas (FARAL) em Centro Universitário UNIRB- Alagoinhas;<br>
                                    Aquisição da 23ª Unidade da Rede UNIRB na Cidade de Fortaleza- CE.</b>
                                    </p>
                                </div>
                            </a>
                        </div>
                       
                        
                    </div>
                </div>
            </div>
        </div>


@include('partials._footer')

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>







<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 




</body>
</html>