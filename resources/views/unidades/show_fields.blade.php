<!-- Nome Field -->
<div class="form-group col-md-2">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{!! $unidades->nome !!}</p>
</div>

<!-- Calendario Field -->
<div class="form-group col-md-2">
    {!! Form::label('calendario', 'Calendario:') !!}
    <p>{!! $unidades->calendario !!}</p>
</div>

<!-- Portal Cademico Field -->
<div class="form-group col-md-2">
    {!! Form::label('portal_cademico', 'Portal Cademico:') !!}
    <p>{!! $unidades->portal_cademico !!}</p>
</div>

<!-- Latitude Field -->
<div class="form-group col-md-2">
    {!! Form::label('latitude', 'Latitude:') !!}
    <p>{!! $unidades->latitude !!}</p>
</div>

<!-- Longitude Field -->
<div class="form-group col-md-2">
    {!! Form::label('longitude', 'Longitude:') !!}
    <p>{!! $unidades->longitude !!}</p>
</div>

<!-- Logradouro Field -->
<div class="form-group col-md-2">
    {!! Form::label('logradouro', 'Logradouro:') !!}
    <p>{!! $unidades->logradouro !!}</p>
</div>

<!-- Estado Field -->
<div class="form-group col-md-2">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{!! $unidades->estado !!}</p>
</div>

<!-- Cidade Field -->
<div class="form-group col-md-2">
    {!! Form::label('cidade', 'Cidade:') !!}
    <p>{!! $unidades->cidade !!}</p>
</div>

<!-- Bairro Field -->
<div class="form-group col-md-2">
    {!! Form::label('bairro', 'Bairro:') !!}
    <p>{!! $unidades->bairro !!}</p>
</div>

<!-- Cep Field -->
<div class="form-group col-md-2">
    {!! Form::label('cep', 'Cep:') !!}
    <p>{!! $unidades->cep !!}</p>
</div>

<!-- Logomarca Field -->
<div class="form-group col-md-2">
    {!! Form::label('logomarca', 'Logomarca:') !!}
    <p>{!! $unidades->logomarca !!}</p>
</div>
