﻿<title>Portal UNIRB | Laboratórios</title>


@include('partials._head')
<body>

@include('partials._header')


<!-- LAB SAUDE -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#1e426e">
        <h5 class="modal-title txt18" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white"> Laboratório de Saúde</b> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="card mb-3" style="max-width: 100%;">
  <div class="row no-gutters">
    <div class="col-md-4">
      <img src="images/laboratorio/saude.jpg" class="card-img" alt="...">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title"> Laboratório Anatomia</h5>
        <h5 class="card-title"> Laboratório Bioquímica</h5>
        <h5 class="card-title"> Laboratório Estética</h5>
        <h5 class="card-title"> Laboratório Brinquedoteca</h5>
        <h5 class="card-title"> Laboratório Física</h5>
        <h5 class="card-title"> Laboratório Enf.Habilidades</h5>
        <h5 class="card-title"> Laboratório Microbiologia</h5>
        <h5 class="card-title"> Laboratório Microscopia</h5>
        <h5 class="card-title"> Laboratório Pŕe-Clínica</h5>
        <h5 class="card-title"> Laboratório Radiologia</h5>
        <h5 class="card-title"> Laboratório Segurança do Trabalho Patrimonial</h5>
        <h5 class="card-title"> Laboratório Enf. Semiologia / Semiotécnica</h5>
        <h5 class="card-title"> Laboratório Veterinária</h5>
        <h5 class="card-title"> Laboratório Gastronomia - Cozinha Fria e Quente</h5>
        <h5 class="card-title"> Laboratório de Técnicas Dietéticas</h5>

       

      </div>
    </div>
  </div>
</div>
   
      </div>
      
    </div>
  </div>
</div>

<!-- LAB INFORMATICA-->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#1e426e">
        <h5 class="modal-title txt18" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white"> Laboratório de Informática</b> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="card mb-3" style="max-width: 100%;">
  <div class="row no-gutters">
    <div class="col-md-4">
    <img src="images/laboratorio/tecnologia.jpg" class="card-img" alt="...">    </div>
    <div class="col-md-8">
    <div class="card-body">
        <h5 class="card-title">Laboratório ACESSO</h5>
        <h5 class="card-title">Laboratório PAMUD</h5>
        <h5 class="card-title">Laboratório NTED1</h5>
        <h5 class="card-title">Laboratório NTED2</h5>
        <h5 class="card-title">Laboratório REDES</h5>
        <h5 class="card-title">Laboratório PILOTAGEM</h5>
        <h5 class="card-title">Laboratório EAD</h5>

      
      </div>
    </div>
  </div>
</div>
   
      </div>
      
    </div>
  </div>
</div>


<!-- LAB ENGENHARIA -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#1e426e">
        <h5 class="modal-title txt18" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white"> Laboratório de Engenharia</b> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="card mb-3" style="max-width: 100%;">
  <div class="row no-gutters">
    <div class="col-md-4">
    <img src="images/laboratorio/engenharia.jpg" class="card-img" alt="...">    </div>
    <div class="col-md-8">
    <div class="card-body">
    <h5 class="card-title">Laboratório de Desenho</h5>
    <h5 class="card-title">Laboratório de Engenharia</h5>

      
      </div>
    </div>
  </div>
</div>
   
      </div>
      
    </div>
  </div>
</div>


<!-- LAB COMUNICAÇÃO -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#1e426e">
        <h5 class="modal-title txt25"  id="exampleModalLabel"><div class="logo_text logo "><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white"> Laboratório de Comunicação</b> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="card mb-3" style="max-width: 100%;">
  <div class="row no-gutters">
    <div class="col-md-4">
    <img src="images/laboratorio/engenharia.jpg" class="card-img" alt="...">    </div>
    <div class="col-md-8">
    <div class="card-body">
    <h5 class="card-title">Estúdio de TV</h5>
    <h5 class="card-title">Laboratório Movimento I</h5>
    <h5 class="card-title">Laboratório Movimento II</h5>
    <h5 class="card-title">Laboratório Movimento III</h5>      
  
      </div>
    </div>
  </div>
</div>
   
      </div>
      
    </div>
  </div>
</div>





@include('partials._header')



	<!-- banner -->

	<div id="carouselExampleControls" class="carousel slide pose-slide1" data-ride="carousel" >
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="images/laboratorio/banner.png" style="width:100%" class="tam-img1 img-responsive" />
    </div> 
  </div>
    
     <!-- indice -->
    

	<div class="home1" >
		<div class="breadcrumbs_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="breadcrumbs">
							<ul>
								<li><a href="/">Home</a></li>
								<li>laboratórios</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>			
    </div>

   

    <!-- Team -->
    <div class="card mb-3" style="max-width: 100%;background-color:#1e426e;margin-top:1%">
  <div class="row no-gutters">
    <div class="col-md-4">
    <img src="images/laboratorio/computer.png" class="card-img" style="width:30%">    </div>
    <div class="col-md-8">
      <div class="card-body">
      <h5 class="card-title  txt25" style="color:white; padding:25px; font-weight:5px;" ><b>Laboratórios preparados para garantir a melhor experiência prática<br> em todos os cursos.</b></h5>
      </div>
    </div>
  </div>
</div>
   
<br>

<div class="card-deck">
  <div class="card" style="background-color:#ccd8e1">
  <div class="card-header" style="background-color:#1e426e">
    <span style="color:white" class="txt15"><b> Laboratório de Saúde</b></span>
  </div>
  <br>
  <center> <a  href="" data-toggle="modal" data-target="#exampleModal"><img src="images/laboratorio/labsaude.png" class="card-img-top" style="width:45%;"></a></center>   <br>
    
  </div>
  <div class="card" style="background-color:#ccd8e1">
  <div class="card-header" style="background-color:#1e426e">
    <span style="color:white" class="txt15"><b>Laboratório de Informática</b></span>
  </div>
  <center> <a href=""  data-toggle="modal" data-target="#exampleModal1"><img src="images/laboratorio/labtecnologia.png" class="card-img-top" style="width:45%;"></a></center>   <br>
   
  </div>

  <div class="card" style="background-color:#ccd8e1">
  <div class="card-header" style="background-color:#1e426e">
    <span style="color:white" class="txt15"><b>Laboratório de Engenharia</b></span>
  </div>
  <center> <a href=""  data-toggle="modal" data-target="#exampleModal2"><img src="images/laboratorio/labengenharia.png" class="card-img-top" style="width:45%;"></a></center>   <br>
    
  </div>

  <div class="card" style="background-color:#ccd8e1">
  <div class="card-header" style="background-color:#1e426e">
    <span style="color:white " class="txt15"><b>Laboratório de Comunicação</b></span>
  </div>
  <center> <a href=""  data-toggle="modal" data-target="#exampleModal3"><img src="images/laboratorio/labcomunicacao.png" class="card-img-top" style="width:45%;"></a></center>   <br>
    
  
  </div>
</div>

<br>
<div class="">
  
  <div class="card-body">
  <div>
    <h5 class="card-title txt25">Laboratórios de Saúde e Engenharia</h5>
    <p class="card-text text-justify txt3" style="color:black">Na UNIRB os laboratórios de saúde possuem normas e procedimentos para o aluno ter as instruções como: Uso obrigatório de jaleco, sapato fechado, calça cumprida e dentre outros.</p>
 
 </div> 
 <br>
 <div>
    <h5 class="card-title txt25">Laboratórios de Informática</h5>
    <p class="card-text text-justify txt3" style="color:black">Os laboratórios de Informática são equipados com computadores adquados para a elaboração das atividades acadêmicas exigidas pelos docentes como requisitos para a conclusão dos cursos.</p>
 
 </div> 
 <br>

 <div>
    <h5 class="card-title txt25">Laboratórios de Comunicação</h5>
    <p class="card-text text-justify txt3" style="color:black">Os laboratórios de comunicação são equipados com computadores adquados para a elaboração das atividades acadêmicas exigidas pelos docentes como requisitos para a conclusão dos cursos.</p>
 
 </div> 
 
 </div>
</div>



	
    
	



	
@include('partials._footer')

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>




</body>
</html>