﻿
<style>
.txthover:hover{
	color:blue;
}
</style>


<div class="super_container">



	<!-- Header -->

	<header class="header">
			
		<!-- Top Bar -->
		<div class="top_bar" style="background-color:black">
			<div class="top_bar_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
								<ul class="top_bar_contact_list">
									<li>
									<div class="container">
  <!--<div class="dropdown">
    <p  class=" dropdown-toggle " data-toggle="dropdown" style="color:white">
	<i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;
 <b>Selecione seu local</b>
    </p>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="#">Link 1</a>
      <a class="dropdown-item" href="#">Link 2</a>
      <a class="dropdown-item" href="#">Link 3</a>
    </div>
  </div>
</div>-->
									
	</li>
	<li>
	<i class="fa fa-phone" aria-hidden="true"></i>
	<div><span class="font-txt-header ouvidoria"><a href="ouvidoria"><b >OUVIDORIA</b></a><span></div>
     </li>
      &nbsp;&nbsp;<li>
		<div><span class="font-txt-header"><a href="ouvidoria"><b>RESULTADO DO VESTIBULAR</b></a></span></div>
		</li>
	<li>
	<div><span class="font-txt-header"><a href="ouvidoria"><b>EDITAIS</b></a></div>

       </li>
	</ul>
	<div class="top_bar_login ml-auto">
									<div class="login_button"><a href="https://unirb1.portaldominus.com.br/login.php"  target="_blank" ><i class="fa fa-graduation-cap" aria-hidden="true"></i> PORTAL ACADÊMICO </a></div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>

		<!-- Header Content -->
		<div class="header_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo_container">
								<a href="principal">
									<div class="logo_text logo"><img src="images/home/redeunirb.png" ></div>
								</a>
							</div>
							<nav class="main_nav_contaner ml-auto">
								<ul class="main_nav">
								<li>
                                 <div class="navbar-nav">
                                 <div class="dropdown">
                                 <a class="nav-item  dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" >A UNIRB</a> 
                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                 <a class="dropdown-item" href="institucional" style="color:black">Institucional</a>
                                <a class="dropdown-item" href="https://docs.google.com/forms/d/1nQpnH2eQ7SN92IiuC_Kn_IygQhG28MU5b2gQzh5Nt-g/viewform?edit_requested=true#responses"  target="_blank" style="color:black;font-size:15px">Egressos</a>
                                <a class="dropdown-item" href="http://portal.unirb.edu.br/cpa_novo/cpa_administrativo_ssa.php"  target="_blank" style="color:black;font-size:15px">CPA</a>
								<a class="dropdown-item" href="/linhadotempo" style="color:black;font-size:15px">Linha do Tempo</a>
								<a class="dropdown-item" href="/laboratorios" style="color:black;font-size:15px">Laboratórios</a>
								<a class="dropdown-item" href="/biblioteca" style="color:black;font-size:15px">Biblioteca</a>
								<a class="dropdown-item" href="/egresso" style="color:black;font-size:15px">Programa de apoio ao Egresso</a>

                               </div>
								  </div>
								</li>

								<li>
                                 <div class="navbar-nav">
                                 <div class="dropdown">
                                 <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" >Cursos</a> 
                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								 <a class="dropdown-item" href="/graduacao" style="color:black;font-size:15px">Graduação</a>
								 <a class="dropdown-item" href="/posgraduacao" style="color:black;font-size:15px">Pós-Graduação</a>
								 <a class="dropdown-item" href="http://betaead.unirb.edu.br/" style="color:black;font-size:15px">EAD</a>

                               </div>
								  </div>
								</li>

									<li>
                                 <div class="navbar-nav">
                                 <div class="dropdown">
                                 <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" >Estude na UNIRB</a> 
                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="/creditoestudantil" style="color:black;font-size:15px">Crédito Estudantil</a>
								<a class="dropdown-item" href="/convenios" style="color:black;font-size:15px">Convênios</a>
								<a class="dropdown-item" href="/estagio" style="color:black;font-size:15px">Programas de Estágio</a>
								<a class="dropdown-item" href="/intercambio" style="color:black;font-size:15px">Intercâmbio</a>

                               </div>
								  </div>
								</li>

								<li>
                                 <div class="navbar-nav">
								 <div class=""><button class="btn btn-success"><b><a href="vestibular" style="color:white;font-size:13px" >INSCREVA-SE</a></b></button></div>

                                 
								  </div>
								</li>

									
								</ul>



									<!-- Hamburger -->

									<div class="hamburger menu_mm">
									<i class="fa fa-bars menu_mm" aria-hidden="true" style="color:black"></i>
								</div>	
							</nav>
								
							</nav>

						</div>
					</div>
				</div>
			</div>
		</div>

			
	</header>

	<!-- Menu -->

	<div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<nav class="menu_nav">
			<ul class="menu_mm">
			<div class="logo_container">
			<a href="principal">
		    <div class="logo_text logo"><img src="images/home/redeunirb.png" style="width:130%" ></div>
			</a>
		    </div>
				
			</ul>

			<ul class="menu_mm">
			<div class="dropdown">
            <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="color:black;font-size:18px">A UNIRB</a> 
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" href="institucional" style="color:black">Institucional</a>
                                <a class="dropdown-item" href="https://docs.google.com/forms/d/1nQpnH2eQ7SN92IiuC_Kn_IygQhG28MU5b2gQzh5Nt-g/viewform?edit_requested=true#responses"  target="_blank" style="color:black;font-size:15px">Egressos</a>
                                <a class="dropdown-item" href="http://portal.unirb.edu.br/cpa_novo/cpa_administrativo_ssa.php"  target="_blank" style="color:black;font-size:15px">CPA</a>
								<a class="dropdown-item" href="/linhadotempo" style="color:black;font-size:15px">Linha do Tempo</a>
								<a class="dropdown-item" href="/laboratorios" style="color:black;font-size:15px">Laboratórios</a>
								<a class="dropdown-item" href="/biblioteca" style="color:black;font-size:15px">Biblioteca</a>
								<a class="dropdown-item" href="/biblioteca" style="color:black;font-size:15px">Egressos e Estudantes</a>


         </div>
				
			</ul>

			<ul class="menu_mm">
			<div class="dropdown">
            <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="color:black;font-size:18px">Cursos</a> 
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" href="/graduacao" style="color:black;font-size:15px">Graduação</a>
								 <a class="dropdown-item" href="/posgraduacao" style="color:black;font-size:15px">Pós-Graduação</a>
								 <a class="dropdown-item" href="http://betaead.unirb.edu.br/" style="color:black;font-size:15px">EAD</a>

         </div>
				
			</ul>

			<ul class="menu_mm">
			<div class="dropdown">
            <a class="nav-item nav-link active  dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="color:black;font-size:18px">Estude na UNIRB</a> 
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="/creditoestudantil" style="color:black;font-size:15px">Crédito Estudantil</a>
								<a class="dropdown-item" href="/convenios" style="color:black;font-size:15px">Convênios</a>
								<a class="dropdown-item" href="/estagio" style="color:black;font-size:15px">Programas de Estágio</a>
								<a class="dropdown-item" href="/intercambio" style="color:black;font-size:15px">Intercâmbio</a>

		 </div>
		 <br>
				
			</ul>

			<ul class="menu_mm">
			<div class=""><a href="https://unirb1.portaldominus.com.br/login.php" style="color:black;font-size:18px" target="_blank"><i class="fa fa-graduation-cap" aria-hidden="true"></i> PORTAL DO ALUNO</a></div>

				
			</ul>

				</ul><br>

			<ul class="menu_mm">
			<div class="dropdown">
			<div class=""><button class="btn btn-success"><b><a href="vestibular" style="color:white">INSCREVA-SE</a></b></button></div>
           

				
			</ul>
		</nav>
	</div>
	