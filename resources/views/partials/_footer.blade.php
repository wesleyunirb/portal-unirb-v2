﻿<!-- Footer -->

<!-- Modal BIBLIOTECA DIGITAL -->
<div class="modal fade" id="modalExemplobiblioteca" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="font-size:20px">Biblioteca Digital</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<ul>
<li>
<a href="images/revistas/revistaano42012.pdf" target="_blank"><button type="button"  class="btn btn-secondary btn-lg btn-block">Tema: Multidisciplinar  Ano 2012</button></a><br>
<a href="images/revistas/revistaano42012.pdf" target="_blank"><button type="button"  class="btn btn-secondary btn-lg btn-block">Tema: Multidisciplinar  Ano 2013</button></a>
</li>
</ul>     
 </div>
     
    </div>
  </div>
</div>




<footer class="footer " >
		<div class="footer_background" style=" background-image: url('images/home/fundo.png');"></div>
		<div class="container">
			<div class="row footer_row">
				<div class="col">
					<div class="footer_content">
						<div class="row">

							<div class="col-md-3 footer_col">
					
								<!-- Footer About -->
								<div class="footer_section footer_about ">
									<div class="footer_logo_container">
										<a href="#">
                                        <div class="logo_text logo "><img src="images/home/redeunirb.png" ></div>
										</a>
									</div><br>
									<div class="footer_about_text ">
										<p>#construindosonhos</p>
									</div>
									<div class="footer_social ">
										<ul>
											<li><a href="https://www.facebook.com/FaculdadeUnirb" target="_blank"><i class="fa fa-facebook fa-2x fa4" aria-hidden="true"  ></i></a></li>
											<li><a href="https://www.youtube.com/channel/UCfOUZ3mz1y5H8zdfUmH4yyw" target="_blank"><i class="fa fa-youtube fa-2x  fa2" aria-hidden="true"></i></a></li>
											<li><a href="#http://www.unirb.edu.br/salvador/farb/#" target="_blank"><i class="fa fa-linkedin fa-2x  fa3" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-md-3 footer_col">
					
								<!-- Footer Contact -->
								<div class="footer_section footer_contact" style="margin-top:-13%;width:110%;margin-left:-12%">
									<div class="footer_title"></div>
									<div class="footer_contact_info">
									<div class="card bg-light mb-3 pose-footer" style="max-width: 18rem;">
  <div class="card-body ">
   <h5 class="card-title color-blue txt18">Fale Conosco</h5>
	<p class="card-text color-blue">Atendimento Unirb: <br>3368-8300</p>
	<p class="card-text color-blue">Ouvidoria: <br>3368-8300</p>
	<p class="card-text color-blue">Sala de Imprensa: <br> 3368-8300 </p>

  </div>
</div>
									</div>
								</div>
								
</div>

<div class="col-md-3 footer_col">
					
<!-- Footer links -->
<div class="footer_section footer_links">
<div class="footer_title txt18">Páginas</div>
<div class="footer_links_container">
<ul>
<li><a href="institucional" style="color:white">Institucional</a></li>
<li><a href="egressos" style="color:white">Egressos</a></li>
<li><a href="http://portal.unirb.edu.br/cpa_novo/cpa_administrativo_ssa.php" target="_blank" style="color:white">CPA</a></li>
<li><a href="linhadotempo" style="color:white">Linha do Tempo</a></li>
<li><a href="laboratorios" style="color:white">Laboratórios</a></li>

											
</ul>
</div>
</div>
</div>

<div class="col-md-3 footer_col clearfix">
					
<!-- Footer links -->
<div class="footer_section footer_mobile">
<div class="footer_title txt18">Links Úteis</div>
<div class="footer_mobile_content">
<div class="footer_image"><a href="https://unirb.ensinoeps.com.br/alunos/"><p style="color:white">AVA</p></a></div>
<div class="footer_image"><p style="color:white" ><a href="https://dliportal.zbra.com.br/Login.aspx?key=UNIRB">Minha Biblioteca- Biblioteca Virtual</a></p></div>
<div class="footer_image"><a data-toggle="modal" data-target="#modalExemplobiblioteca"><p style="color:white">Biblioteca digital Acervo Eletrônico</p></a></div>
<div class="footer_image"><a href="https://unirb1.portaldominus.com.br/login.php"><p style="color:white">Portal do Aluno</p></a></div>


									</div>
								</div>
								
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="row copyright_row pose-copiright">
				<div class="col">
					<div class="copyright d-flex flex-lg-row flex-column align-items-center justify-content-start">
						
					<div class="cr_text">
							<span style="color:white">Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos os direitos reservados a UNIRB</span> 
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</footer>