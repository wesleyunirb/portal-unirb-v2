﻿
<title>Portal UNIRB| Pós Graduação</title>

@include('partials._head')
<body>
@include('partials._header')



<!-- Bacharelado -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#1e426e">
        <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/logo.png"  style="width:100%"></div><b style="color:white">&nbsp; Pós-Graduação</b> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="card mb-3" >
  <div class="row no-gutters">
    <div class="col-md-10">
      <div class="card-body">
      <ul style="color:black">
      <li><p class="txt18" style="color:#384158"> Salvador </p>
        
       
        </ul>  

       

      </div>
    </div>
  </div>
</div>
   
      </div>
      
    </div>
  </div>
</div>


	<!-- banner -->

	<div id="carouselExampleControls" class="carousel slide pose-slide1" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="images/home/banner2.jpg" style="width:100%" class="tam-img1 img-responsive" />
    </div> 
  </div>
    
     <!-- indice -->
    

	<div class="home1">
		<div class="breadcrumbs_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="breadcrumbs">
							<ul>
								<li><a href="/">Home</a></li>
								<li>Pós-Graduação</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>			
    </div>



  <center><div class="col-md-12">
  <div class=" center">
    <div class="card-body">
    <h5 class="card-title txt50" style="color:#1f466f">Pós-Graduação</h5>
      <p class=" txt18" style="color:black">Invista em você e garanta a sua vaga no melhor curso de pós graduação.
      <br> Unirb referência em ensino superior. </p>
      </p>
      
    </div>
  </div>

  </div>
  </center>

  <div class="row">

  
  <div class="col-sm-4">
  <div class="card">
      <div class="card-body">
        <h5 class="card-title txt25" style="color:#384158">Salvador</h5>
        <p>
  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample">
    Verificar
  </a>
</p>

<div class="collapse pose4" id="collapseExample2" >
  <div class="card card-body">
<ul style="color:black">
<li>Pós-Graduação Engenharia de Segurança Contra Incêndio e Pânico</li>
        <li>Pós-Graduação em Conciliação Arbitragem e Mediação</li>
        <li>Pós-Graduação em Engenharia</li>
        <li>Pós-Graduação Manejo Reprodutivo em Bovinos de Corte</li>
        <li>Pós-Graduação em Acupuntur</li>
        <li>Pós-Graduação em Audiologia do Diagnóstico à Reabilitação</li>
        <li>Pós-Graduação em Docência na Educação Infantil</li>
        <li>Pós-Graduação em Enfermagem Pediátrica e Neonatal</li>
        <li>Pós-Graduação em Oncologia Aspectos Terapêuticos e Estéticos</li>
        <li>Pós-Graduação em Psicologia Jurídica</li>
        <li>Pós-Graduação Lato Sensu em Docência no Ensino Superior para <br>Educação Presencial e EAD</li>
        <li>Pós-Graduação Lato Sensu em Enfermagem Dermatologica Feridas</li>
        <li>Engenharia Sanitária e Ambiental</li>
        <li>Pós-Graduação Lato Sensu em Periodontia</li>
        <li>Pós-Graduação Terapia Manual e Cinesioterapia Aplicada à Saúde</li>
        
</ul>  
      </div>
</div>

      </div>
    </div>
  </div>

  <div class="col-sm-4">
  <div class="card">
      <div class="card-body">
      <h5 class="card-title txt25" style="color:#384158">Alagoinhas</h5>
        <p>
  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample3" role="button" aria-expanded="false" aria-controls="collapseExample">
    Verificar
  </a>
</p>

<div class="collapse pose4" id="collapseExample3" >
  <div class="card card-body">
<ul style="color:black">
<li>Pós-Graduação em Ciências Criminais</li>
        <li>Pós-Graduação em Direito Processual Civil</li>
        <li>Pós-Graduação Direito do Trabalho e Previdenciário</li>
        <li>Engenharia de Alimentos</li>
        <li>Pós-Graduação Psicologia do Esporte com Ênfase em Nutrição,<br>Treinamento e Fisioterapia Desportiva</li>
        <li>Pós-Graduação Psicologia Do Esporte - Divulgação</li>
        <li>Pós-Graduação Saúde Coletiva</li>
 </ul>  
      </div>
</div>

      </div>
    </div>
  </div>

  <div class="col-sm-4">
  <div class="card">
      <div class="card-body">
      <h5 class="card-title txt25" style="color:#384158">Ceará</h5>
        <p>
  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
    Verificar
  </a>
</p>

<div class="collapse pose4" id="collapseExample" >
  <div class="card card-body">
<ul style="color:black">
<li>Pós-Graduação Lato Sensu em Gestão Social</li>

 </ul>  
  </div>
</div>

      </div>
    </div>
  </div>


</div>


<!--CURSOS EM DESTAQUE-->

<div class="section_title_container text-center col-md-12" style='margin-top:5%'>
						<h2 class="section_title color-blue txt50">Cursos em Destaque</h2>
						
        </div>
        <br><br>

        <div class="card-group pose-cardimg" >
  <div class=" col-md-2">
    <img src="images/posgraduacao/pos-odonto.png" class="card-img-top" >
    <div class="card-body">
      <h5 class="card-title txt18" style="color:black"> Endodontia</h5><br>
    </div>
  </div>
  
  <div class=" col-md-2">
  <img src="images/posgraduacao/personaltraining.png" class="card-img-top" >
    <div class="card-body">
      <h5 class="card-title txt18" style="color:black">Personal <br>Training</h5>

    </div>
  </div>

  <div class=" col-md-2">
  <img src="images/posgraduacao/engenhari.png" class="card-img-top" >
    <div class="card-body">
      <h5 class="card-title txt18" style="color:black">Engenharia</h5><br>

    </div>
  </div>

  <div class=" col-md-2">
  <img src="images/posgraduacao/direito.png" class="card-img-top" >
    <div class="card-body">
      <h5 class="card-title txt18" style="color:black">Direito Processual Civil</h5>

    </div>
  </div>

  <div class=" col-md-2">
  <img src="images/posgraduacao/enfermagem.png" class="card-img-top" >
    <div class="card-body">
      <h5 class="card-title txt18" style="color:black;">Enfermagem</h5><br>
    </div>
  </div>
  
</div>
<center> <div class="card col-md-4" style="margin-top:2%;border:1px solid #254170">
            <div class="card-body">
              <h5 class="card-title txt25" style="color:#254170">Inscreva-se na Pós-Graduação <b>UNIRB</b></h5>
              <a href="http://portal.unirb.edu.br/sistema_inscricao/?id=16" class="btn btn-warning" style="color:white"><b>Quero me increver</b> </a>
            </div>
          </div>
</center>


	
@include('partials._footer')

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>




</body>
</html>