<div class="table-responsive">
    <table class="table" id="campanhas-table">
        <thead>
            <tr>
                <th>Nome</th>
                <th colspan="3">Ações</th>
            </tr>
        </thead>
        <tbody>
        @foreach($campanhas as $campanha)
            <tr>
                <td>{{ $campanha->nome }}</td>
                <td>
                    {!! Form::open(['route' => ['campanhas.destroy', $campanha->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('campanhas.show', [$campanha->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('campanhas.edit', [$campanha->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
