﻿<title>Portal UNIRB| Intercâmbio</title>

@include('partials._head')
<body>

@include('partials._header')


<!-- banner -->



<div id="carouselExampleControls" class="carousel slide pose-slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="images/intercambio/banner.png" style="width:100%" class="tam-img1 img-responsive" />
    </div> 
  </div>

 
     <!-- indice -->
	<div class="home1" style="margin-top:3%">
		<div class="breadcrumbs_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="breadcrumbs">
							<ul>
								<li><a href="/">Home</a></li>
								<li>Intercâmbio</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>			
    </div>

	
	<center><div class="col-md-8" >

<div class=" mb-3" style="margin-top:3%">
<div class="row no-gutters">
<div class="col-md-4 card-horizontal">
<img src="images/intercambio/college.png" class="card-img"  style="width:100%">    </div>
<div class="col-md-8">
 <div class="card-body">
 <p class="card-text txt16 text-justify" style="color:black">A UNIRB e a VIA university Entidade de Ensino Superior da Dinamarca, promovem visitas culturais de aprendizado.
                        Conheça o programa oferecido para alunos de Graduação e aproveite a oportunidade de vivenciar um projeto fora do país.
                        Para maiores informações acesse o site e realize o seu sonho!<br>
                        Site: <a href="https://en.via.dk/"><b style="color:black">VIA university Entidade</b></a></p>  
</div>
</div>
</div>
</center>


<!-- Barra -->
<div >
  <img src="images/intercambio/inter_barra.png" alt="some text" width=100% >
</div>



	<!-- Footer -->
	@include('partials._footer')

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>




</body>
</html>