<!-- Cpf Field -->
<div class="form-group">
    {!! Form::label('cpf', 'Cpf:') !!}
    <p>{{ $leads->cpf }}</p>
</div>

<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{{ $leads->nome }}</p>
</div>

<!-- Telefone Field -->
<div class="form-group">
    {!! Form::label('telefone', 'Telefone:') !!}
    <p>{{ $leads->telefone }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $leads->email }}</p>
</div>

<!-- Campanha Id Field -->
<div class="form-group">
    {!! Form::label('campanha_id', 'Campanha Id:') !!}
    <p>{{ $leads->campanha_id }}</p>
</div>

<!-- Unidade Id Field -->
<div class="form-group">
    {!! Form::label('unidade_id', 'Unidade Id:') !!}
    <p>{{ $leads->unidade_id }}</p>
</div>

