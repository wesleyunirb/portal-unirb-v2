<div class="table-responsive">
    <table class="table" id="leads-table">
        <thead>
            <tr>
                <th>Cpf</th>
        <th>Nome</th>
        <th>Telefone</th>
        <th>Email</th>
        <th>Campanha Id</th>
        <th>Unidade Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($leads as $leads)
            <tr>
                <td>{{ $leads->cpf }}</td>
            <td>{{ $leads->nome }}</td>
            <td>{{ $leads->telefone }}</td>
            <td>{{ $leads->email }}</td>
            <td>{{ $leads->campanha_id }}</td>
            <td>{{ $leads->unidade_id }}</td>
                <td>
                    {!! Form::open(['route' => ['leads.destroy', $leads->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('leads.show', [$leads->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('leads.edit', [$leads->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
