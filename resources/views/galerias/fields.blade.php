<!-- Img Field -->
<div class="form-group col-sm-6">
    {!! Form::label('img', 'Img:') !!}
    {!! Form::text('img', null, ['class' => 'form-control']) !!}
</div>

<!-- Titulo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('titulo', 'Titulo:') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<!-- Unidade Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('unidade_id', 'Selecione a unidade:') !!}
    <select name="unidade_id" class="form-control">
    <option >Unidade</option>

    @foreach ($unidades as $uni)
    <option name="unidades" value="{{ $uni->id }}" >{{ $uni->nome }}</option>
    @endforeach
  </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('galerias.index') !!}" class="btn btn-danger">Cancelar</a>
</div>
