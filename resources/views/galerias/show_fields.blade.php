<!-- Img Field -->
<div class="form-group">
    {!! Form::label('img', 'Img:') !!}
    <p>{!! $galeria->img !!}</p>
</div>

<!-- Titulo Field -->
<div class="form-group">
    {!! Form::label('titulo', 'Titulo:') !!}
    <p>{!! $galeria->titulo !!}</p>
</div>

<!-- Unidade Id Field -->
<div class="form-group">
    {!! Form::label('unidade_id', 'Unidade Id:') !!}
    <p>{!! $galeria->unidade_id !!}</p>
</div>

