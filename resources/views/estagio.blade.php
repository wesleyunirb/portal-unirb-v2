﻿<title>Portal UNIRB | Estágio</title>

@include('partials._head')
<body>

<@include('partials._header')

    <!-- banner -->

<div id="carouselExampleControls" class="carousel slide pose-slide1" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="images/estagio/banner.png" style="width:100%" class="tam-img1 img-responsive" />
    </div> 
  </div>

    
     <!-- indice -->
	<div class="home1">
		<div class="breadcrumbs_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="breadcrumbs">
							<ul>
								<li><a href="/">Home</a></li>
								<li>Estágio</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>			
    </div>


<center><div class="col-md-8" style="margin-top:2%"><p class="text-justify" style="color:black;font-size:16px">O aluno coloca em prática todo aprendizado teórico do ensino acadêmico com objetivo de aprimorar as habilidades e ingressar no mercado de trabalho profissional.  
A equipe da UNIRB promove parcerias com empresas em diversas áreas e habilita a vaga na instituição para que o aluno candidate-se. O estágio é uma oportunidade ao aluno adquirir competências na área escolhida e a supervisão ocorre com o auxílio de um profissional capacitado na empresa acompanhado de relatórios semestrais para acompanhamento da instituição.</p></div><center>
				
<!-- Footer -->

	@include('partials._footer')

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>




</body>
</html>