﻿
<li class="header" style="color:white">MENU MKT | COMERCIAL</li>



<li class="treeview" style="bacground-color: #000">
<a href="#">
  <i class=" fa fa-circle-o "></i>
  <span>Unidades</span>
  <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
  </span>
</a>
<ul class="treeview-menu">

<li class="{{ Request::is('noticias*') ? 'active' : '' }}">
    <a href="{!! route('noticias.index') !!}"><i class="fa fa-newspaper-o" aria-hidden="true"></i><span>&nbsp; Notícias</span></a>
</li>


<li class="{{ Request::is('eventos*') ? 'active' : '' }}">
    <a href="{!! route('eventos.index') !!}"><i class="fa fa-edit"></i><span>Eventos</span></a>
</li>

<li class="{{ Request::is('leads*') ? 'active' : '' }}">
    <a href="{!! route('leads.index') !!}"><i class="fa fa-users" aria-hidden="true"></i><span>Leads</span></a>
</li>

<li class="">
    <a href="upload"><i class="fa fa-cloud-upload" aria-hidden="true"></i><span>Upload de imagem</span></a>
</li>




</ul>




