
<li class="header" style="color:white">MENU TI</li>



<li class="treeview" style="bacground-color: #000">
<a href="#">
  <i class=" fa fa-circle-o "></i>
  <span>Administração</span>
  <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
  </span>
</a>
<ul class="treeview-menu">
<li class="{{ Request::is('unidades*') ? 'active' : '' }}">
    <a href="{!! route('unidades.index') !!}"><i class="fa fa-home" aria-hidden="true"></i><span>Unidades</span></a>
</li>
<li class="{{ Request::is('noticias*') ? 'active' : '' }}">
    <a href="{!! route('noticias.index') !!}"><i class="fa fa-newspaper-o" aria-hidden="true"></i><span> Notícias</span></a>
</li>
<li class="{{ Request::is('cursos*') ? 'active' : '' }}">
    <a href="{!! route('cursos.index') !!}"><i class="fa fa-edit"></i><span>Cursos</span></a>
</li>

<li class="{{ Request::is('tipoCursos*') ? 'active' : '' }}">
    <a href="{!! route('tipoCursos.index') !!}"><i class="fa fa-edit"></i><span>Tipo dos Cursos</span></a>
</li>

<li class="{{ Request::is('eventos*') ? 'active' : '' }}">
    <a href="{!! route('eventos.index') !!}"><i class="fa fa-calendar" aria-hidden="true"></i><span>Eventos</span></a>
</li>

<li class="{{ Request::is('tipoEventos*') ? 'active' : '' }}">
    <a href="{!! route('tipoEventos.index') !!}"><i class="fa fa-calendar" aria-hidden="true"></i><span>Tipo Eventos</span></a>
</li>


<li class="{{ Request::is('banners*') ? 'active' : '' }}">
    <a href="{!! route('banners.index') !!}"><i class="fa fa-picture-o" aria-hidden="true"></i><span>Banners</span></a>
</li>
<li class="{{ Request::is('galerias*') ? 'active' : '' }}">
    <a href="{!! route('galerias.index') !!}"><i class="fa fa-picture-o" aria-hidden="true"></i><span>Galerias</span></a>
</li>

<li class="{{ Request::is('inscritos*') ? 'active' : '' }}">
    <a href="{!! route('inscritos.index') !!}"><i class="fa fa-users" aria-hidden="true"></i><span>Inscritos eventos</span></a>
</li>

<li class="{{ Request::is('leads*') ? 'active' : '' }}">
    <a href="{!! route('leads.index') !!}"><i class="fa fa-users" aria-hidden="true"></i><span>Leads</span></a>
</li>

<li class="{{ Request::is('campanhas*') ? 'active' : '' }}">
    <a href="{!! route('campanhas.index') !!}"><i class="fa fa-users" aria-hidden="true"></i><span>Campanhas</span></a>
</li>


<li class="">
    <a href="upload"><i class="fa fa-cloud-upload" aria-hidden="true"></i><span>Upload de imagem</span></a>
</li>


</ul>


<li class="treeview" style="bacground-color: #000">
<a href="#">
  <i class=" fa fa-circle-o "></i>
  <span>Configurações</span>
  <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
  </span>
</a>
<ul class="treeview-menu">
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-users" aria-hidden="true"></i><span>&nbsp;Usuários</span></a>
</li>




</ul>


