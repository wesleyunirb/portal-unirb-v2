@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            <b>Noticias</b>
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($noticias, ['route' => ['noticias.update', $noticias->id], 'method' => 'patch']) !!}

                        @include('noticias.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection