<div class="table-responsive">
    <table class="table" id="noticias-table">
        <thead>
            <tr>
                <th>Unidade</th>
        <th>Título</th>
        <th>Subtítulo</th>
        <th>Data</th>
        <th>Imagem</th>
        <th>Status</th>
                <th colspan="3">Ações</th>
            </tr>
        </thead>
        <tbody>
        @foreach($noticias as $noticias)
            <tr>

                <td>{!! $noticias->unidade_id!!}</td>

            <td>{!! $noticias->titulo !!}</td>
            <td>{!! $noticias->subtitulo !!}</td>
            <td>{!! $noticias->data !!}</td>
            <td>{!! $noticias->image !!}</td>
            <td>{!! $noticias->status !!}</td>
                <td>
                    {!! Form::open(['route' => ['noticias.destroy', $noticias->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('noticias.show', [$noticias->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                       <a href="{!! route('noticias.edit', [$noticias->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                         <!--{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}-->
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
