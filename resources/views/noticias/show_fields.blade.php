<!-- Unidade Id Field -->
<div class="form-group col-md-3">
    {!! Form::label('unidade_id', 'Unidade:') !!}
    <p>{!! $noticias->unidade_id !!}</p>
</div>

<!-- Titulo Field -->
<div class="form-group col-md-3">
    {!! Form::label('titulo', 'Titulo:') !!}
    <p>{!! $noticias->titulo !!}</p>
</div>

<!-- Subtitulo Field -->
<div class="form-group col-md-3">
    {!! Form::label('subtitulo', 'Subtítulo:') !!}
    <p>{!! $noticias->subtitulo !!}</p>
</div>

<!-- Data Field -->
<div class="form-group col-md-3">
    {!! Form::label('data', 'Data:') !!}
    <p>{!! $noticias->data !!}</p>
</div>

<!-- Descricao Field -->
<div class="form-group col-md-3">
    {!! Form::label('descricao', 'Descrição:') !!}
    <p>{!! $noticias->descricao !!}</p>
</div>

<!-- Image Field -->
<div class="form-group col-md-3">
    {!! Form::label('image', 'Imagem:') !!}
    <p>{!! $noticias->image !!}</p>
</div>

<!-- Status Field -->
<div class="form-group col-md-3">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $noticias->status !!}</p>
</div>

