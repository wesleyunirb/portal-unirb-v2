﻿@extends('layouts.app')

@section('content')


<div class="form-group ">
    <a href="{{ route('inscritos.index') }}" class="btn btn-primary">Voltar</a>
</div>

<div class="container" >

<!-- Submit Field -->

	<div class="row">
        <div class="span12">
    		<div class="" id="loginModal">
              <div class="modal-body">
                <div class="well">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#login" data-toggle="tab">Pesquisar Inscritos</a></li>
                    <li><a href="#create" data-toggle="tab">Relatório PDF</a></li>
                  </ul>

                 <!--FORM PESQUISA -->
                  <div id="myTabContent" class="tab-content">
                    <div class="tab-pane active in" id="login">
                          {!! Form:: open(['route'=>'relatorio.relatorioinscrito', 'method' => 'POST']) !!}   
                          <div class="control-group col-md-4" style="margin-top:3%">
                            <!-- Pesquisa -->
                            <label style="color:black"><b>Selecione o Evento</b></label>
                            <div class="controls">
                        <select name="evento" id="evento" class="form-control">
                           <option >Evento</option>
                           @foreach ($evento as $event)
                          <option name="evento" value="{{ $event->id }}" >{{ $event->titulo }}</option>
                          @endforeach
                        </select>                            
                    </div>
                    <div style="margin-left:100%;margin-top:-10%">
                    <button type="submit" class="btn btn-primary">Pesquisar</button>
                    </div>
                    </div>
     
                   <div class="control-group">
                   <!-- table-->
                 <table style="margin-top:12%">
                <div class="table-responsive">
                <table class="table" id="leads-table" style="background-color:white">
                <thead>
                  <tr>
                    <th>Nome</th>
                    <th>Cpf</th>
                    <th>Telefone</th>
                    <th>Email</th>
                    </tr>
                    </thead>
                   <tbody>
                @foreach($inscritos as $inscritos)
                  <tr>
                    <td>{!! $inscritos->nome !!}</td>
                    <td>{!! $inscritos->cpf !!}</td>
                    <td>{!! $inscritos->telefone !!}</td>
                    <td>{!! $inscritos->email !!}</td>    
                 </tr>
                @endforeach
        </tbody>
    </table>                        
 </div>
 {!!Form:: close()!!}
 </div>

    <!--FORM PDF -->
     <div class="tab-pane fade" id="create">
     {!! Form:: open(['route'=>'relatorio.relatoriopdfinscrito', 'method' => 'POST']) !!}   
     <p style="color:black"><b>Selecione o Evento</b></p>
    <select name="eventoid" id="eventoid" class="form-control">
    <option >Evento</option>
    @foreach ($evento as $event)
    <option name="eventoid" value="{{ $event->id }}" >{{ $event->titulo }}</option>
    @endforeach
  </select>
  <button type="submit"  class="btn btn-primary"><a  style="color:white">Gerar relatorio</a></button>
  {!!Form:: close()!!}
                    </div>
                </div>
              </div>
            </div>
        </div>
	</div>
</div>

</div>







@endsection