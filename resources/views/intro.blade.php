﻿<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Portal UNIRB</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"><link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
			<link rel="stylesheet" href="css/main.css">
    </head>
    
    <style>

      .btntam{
        background-color:#16456f;
        border-radius: 10px 20px;
        font-size:22px;
      }

        .posenav{
          margin-left:-20%;margin-top:-6%;
        }
        .tamlogo{
          width:20%;
        }

        
      
      @media(min-width:300px) and (max-width:500px){
        .btntam{
        background-color:#16456f;
        border-radius: 10px 20px;
        font-size:14px;
        }

        .posenav{
          margin-left:-1%;margin-top:10%;
        }

        .tamlogo{
          width:13%;
        }

  
        
      }


      @media(min-width:550px) and (max-width:800px){
        .btntam{
        background-color:#16456f;
        border-radius: 10px 20px;
        font-size:15px;
        }

        .posenav{
          margin-left:-5%;margin-top:5%;
        }

        .tamlogo{
          width:150%;
        }

  
        
      }
      
      
      </style>
		<body>	
		  <header id="header" id="home" style="padding-top:2%">
	  		
		    <div class="container">
		    	<div class="row align-items-center justify-content-between d-flex">
			      <div id="logo">
            <div class="logo_text logo"><img src="images/home/redeunirb.png"  class="tamlogo" ></div>
			      </div>
			      <nav id="nav-menu-container " class="posenav ">
			        <ul class="nav-menu ">
			          <li><a href="principal" class=" btn btntam">Educação Presencial</a></li>
			          <li><a href="http://betaead.unirb.edu.br/" class=" btn btntam " >Educação a distância</a></li>
			        </ul>
			      </nav><!-- #nav-menu-container -->		    		
		    	</div>
		    </div>
		  </header><!-- #header -->

			<!-- start banner Area -->
			<section class="banner-area relative" id="home" >
				<div class="overlay overlay-bg"></div>	
				<div class="container">
					<div class="row fullscreen d-flex align-items-center justify-content-between" >
						<div class="banner-content col-lg-9 col-md-12">
							<h1 class="" style="color: #1C164F;margin-top:20%;font-size:60px;">
								A PRÁTICA TE LEVA <br>À EXPERIÊNCIA</b>
										
</h1>
              
              <h1 class="text-uppercase" style="color: #9e111f;font-size:40px">
								VENHA PARA A UNIRB
										
              </h1>
              
              <h1 class="text-uppercase" style="color: #1C164F;font-size:25px">
								E DESCUBRA UM MUNDO DE OPORTUNIDADES
										
							</h1>
							
						</div>										
					</div>
				</div>					
			</section>
			<!-- End banner Area -->

    
      

			<script src="js/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
			<script src="js/main.js"></script>	
		</body>
	</html>