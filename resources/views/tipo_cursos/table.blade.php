<div class="table-responsive">
    <table class="table" id="tipoCursos-table">
        <thead>
            <tr>
                <th>Tipo</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tipoCursos as $tipoCursos)
            <tr>
                <td>{{ $tipoCursos->tipo }}</td>
                <td>
                    {!! Form::open(['route' => ['tipoCursos.destroy', $tipoCursos->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('tipoCursos.show', [$tipoCursos->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('tipoCursos.edit', [$tipoCursos->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
