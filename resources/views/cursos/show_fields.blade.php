<!-- Unidade Id Field -->
<div class="form-group">
    {!! Form::label('unidade_id', 'Unidade Id:') !!}
    <p>{{ $curso->unidade_id }}</p>
</div>

<!-- Tipo Cursos Id Field -->
<div class="form-group">
    {!! Form::label('tipo_cursos_id', 'Tipo Cursos Id:') !!}
    <p>{{ $curso->tipo_cursos_id }}</p>
</div>

<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{{ $curso->nome }}</p>
</div>

<!-- Descricao Field -->
<div class="form-group">
    {!! Form::label('descricao', 'Descricao:') !!}
    <p>{{ $curso->descricao }}</p>
</div>

<!-- Portaria Field -->
<div class="form-group">
    {!! Form::label('portaria', 'Portaria:') !!}
    <p>{{ $curso->portaria }}</p>
</div>

<!-- Duracao Field -->
<div class="form-group">
    {!! Form::label('duracao', 'Duracao:') !!}
    <p>{{ $curso->duracao }}</p>
</div>

<!-- Turno Field -->
<div class="form-group">
    {!! Form::label('turno', 'Turno:') !!}
    <p>{{ $curso->turno }}</p>
</div>

<!-- Mensalidade Field -->
<div class="form-group">
    {!! Form::label('mensalidade', 'Mensalidade:') !!}
    <p>{{ $curso->mensalidade }}</p>
</div>

<!-- Matriz Curricular Field -->
<div class="form-group">
    {!! Form::label('matriz_curricular', 'Matriz Curricular:') !!}
    <p>{{ $curso->matriz_curricular }}</p>
</div>

<!-- Qtd Vagas Field -->
<div class="form-group">
    {!! Form::label('qtd_vagas', 'Qtd Vagas:') !!}
    <p>{{ $curso->qtd_vagas }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $curso->status }}</p>
</div>

