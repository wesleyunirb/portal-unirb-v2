<div class="table-responsive">
    <table class="table" id="cursos-table">
        <thead>
            <tr>
                <th>Unidade Id</th>
        <th>Tipo Cursos Id</th>
        <th>Nome</th>
        <th>Portaria</th>
        <th>Duracao</th>
        <th>Turno</th>
        <th>Mensalidade</th>
        <th>Matriz Curricular</th>
        <th>Qtd Vagas</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($cursos as $curso)
            <tr>
                <td>{{ $curso->unidade_id }}</td>
            <td>{{ $curso->tipo_cursos_id }}</td>
            <td>{{ $curso->nome }}</td>
            <td>{{ $curso->portaria }}</td>
            <td>{{ $curso->duracao }}</td>
            <td>{{ $curso->turno }}</td>
            <td>{{ $curso->mensalidade }}</td>
            <td>{{ $curso->matriz_curricular }}</td>
            <td>{{ $curso->qtd_vagas }}</td>
            <td>{{ $curso->status }}</td>
                <td>
                    {!! Form::open(['route' => ['cursos.destroy', $curso->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('cursos.show', [$curso->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('cursos.edit', [$curso->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                       <!-- {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}-->
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
