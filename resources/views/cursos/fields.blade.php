<!-- Unidade Id Field -->
<div class="form-group col-sm-6">
{!! Form::label('unidade_id', 'Selecione a Unidade:') !!}
    <select name="unidade_id" class="form-control">
    <option name="unidades" >Unidade</option>
    @foreach ($unidades as $uni)
    <option name="unidades" value="{{ $uni->id }}">{{ $uni->nome }}</option>
    @endforeach
  </select>
</div>

<!-- Tipo Cursos Id Field -->
<div class="form-group col-sm-6">
{!! Form::label('tipo_cursos_id', 'Selecione o tipo do curso:') !!}
    <select name="tipo_cursos_id" class="form-control">
    <option name="tipocursos" value="" selected="selected"> Tipo do Curso</option>

    @foreach ($tipocursos as $tipoc)
    <option name="tipoeventos" value="{{ $tipoc->id }}">{{ $tipoc->tipo }}</option>
    @endforeach
  </select>
</div>

<!-- Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nome', 'Nome:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<!-- Descricao Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descricao', 'Descricao:') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control']) !!}
</div>

<!-- Portaria Field -->
<div class="form-group col-sm-6">
    {!! Form::label('portaria', 'Portaria:') !!}
    {!! Form::text('portaria', null, ['class' => 'form-control']) !!}
</div>

<!-- Duracao Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duracao', 'Duracao:') !!}
    {!! Form::text('duracao', null, ['class' => 'form-control']) !!}
</div>

<!-- Turno Field -->
<div class="form-group col-sm-6">
    {!! Form::label('turno', 'Turno:') !!}
    {!! Form::text('turno', null, ['class' => 'form-control']) !!}
</div>

<!-- Mensalidade Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mensalidade', 'Mensalidade:') !!}
    {!! Form::text('mensalidade', null, ['class' => 'form-control']) !!}
</div>

<!-- Matriz Curricular Field -->
<div class="form-group col-sm-6">
    {!! Form::label('matriz_curricular', 'Matriz Curricular:') !!}
    {!! Form::text('matriz_curricular', null, ['class' => 'form-control']) !!}
</div>

<!-- Qtd Vagas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('qtd_vagas', 'Qtd Vagas:') !!}
    {!! Form::text('qtd_vagas', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', 0) !!}
        {!! Form::checkbox('status', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
    <a href="{{ route('cursos.index') }}" class="btn btn-danger">Cancelar</a>
</div>
