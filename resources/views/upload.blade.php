﻿
<!DOCTYPE html>
<html lang="pt">
<head>
<title>Portal UNIRB </title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Portal universitário UNIRB">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"><link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="css/main_styles.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link rel="stylesheet" type="text/css" href="css/blog_responsive.css">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">



<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!-- MASK-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


</head>
<body>



<div class="super_container">



	<!-- Header -->

	<header class="header">
			
		<!-- Top Bar -->
		<div class="top_bar" style="background-color:black">
			<div class="top_bar_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
								
	<div class="top_bar_login ml-auto">
									
								
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>

		<!-- Header Content -->
		<div class="header_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo_container">
								<a href="home">
									<div class="logo_text logo"><img src="images/home/redeunirb.png" ></div>
								</a>
							</div>
	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

    <!-- Blog -->
<div class="container col-md-12 " style="background-color:#1e426e;box-shadow: 10px 5px 5px #363636;">
	<div class="row pose-motivos">
	<div class="col">
	<br><br>
	<div class="section_title_container text-center">
    <br><br>
	<h2 class="section_title txt50 "  style="color:white" >Upload de Imagens
	</h2>
    <br><br>

    <form action="{{URL:: to('upload')}}" method="post" enctype="multipart/form-data">
    <input type="file" name="file" id="file" required>
    <button type="submit"   name="submit" class="btn btn-warning">Salvar</button>
    <input type="hidden"  value="{{csrf_token()}}" name="_token" >
</form>
<br><br>
<div>
<p><b style="color:white">Observações:<b><br>
<span style="color:white">O tamanho minimo para o upload da imagem é <span><b style="color:red">1350 X 400 px.</b>
 </p>
</div>

<br><br>
					</div>
				</div>
			</div>

	
	



	
</div>

<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>


<script>

$(document).ready(function(){

  $('#telefone').mask('(99) 9 9999-9999');
  
});
</script>





</body>
</html>