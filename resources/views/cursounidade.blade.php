﻿
<!DOCTYPE html>
<html lang="pt">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Unicat project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> portal UNIRB | vestibular</title>


<link rel="stylesheet" type="text/css" href="css/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/main_styles.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link rel="stylesheet" type="text/css" href="css/vestibular.css">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">




  
</head>


<body>

<!-- Modal PRESENCIAL -->
<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b style="font-size:25px">Graduação Presencial</b> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <label class='form-control tam-label' type="text" ><a href="http://unirbonline.inscricao.crmeducacional.com/Login/3006" target="blank" class="txt15" style="color:black">Vestibular Tradicional</a><i class="fa fa-arrow-circle-right pose-icon" aria-hidden="true" style="color:black"></i></label>
      <label class='form-control tam-label' type="text" ><a href="http://unirbonline.inscricao.crmeducacional.com/Login/1004" target="blank" class="txt15" style="color:black">Vestibular Agendado</a><i class="fa fa-arrow-circle-right pose-icon" aria-hidden="true" style="color:black"></i></label>
      <label class='form-control tam-label' type="text" ><a href="http://unirbonline.inscricao.crmeducacional.com/Login/2005" target="blank" class="txt15" style="color:black">Ingresso via Enem</a><i class="fa fa-arrow-circle-right pose-icon" aria-hidden="true" style="color:black"></i></label>
      <label class='form-control tam-label' type="text" ><a href="http://unirbonline.inscricao.crmeducacional.com/Login/4001" target="blank" class="txt15" style="color:black">Ingresso Segunda Graduação</a><i class="fa fa-arrow-circle-right pose-icon " aria-hidden="true" style="color:black"></i></label>
      <label class='form-control tam-label' type="text" ><a href="http://unirbonline.inscricao.crmeducacional.com/Login/5001" target="blank"class="txt15" style="color:black">Ingresso Tranferência Externa</a><i class="fa fa-arrow-circle-right pose-icon " aria-hidden="true" style="color:black"></i></label>
   
      </div>
      
    </div>
  </div>
</div>

<!-- Modal EAD -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b style="font-size:25px">Graduação EAD</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="modal-body">
      <label class='form-control tam-label' type="text" ><a href="http://unirbonline.inscricao.crmeducacional.com/Login/3007" target="blank" class="txt15" style="color:black">Vestibular Tradicional</a><i class="fa fa-arrow-circle-right pose-icon" aria-hidden="true" style="color:black"></i></label>
      <label class='form-control tam-label' type="text" ><a href="http://unirbonline.inscricao.crmeducacional.com/Login/1005" target="blank" class="txt15" style="color:black">Vestibular Agendado</a><i class="fa fa-arrow-circle-right pose-icon" aria-hidden="true" style="color:black"></i></label>
      <label class='form-control tam-label' type="text" ><a href="http://unirbonline.inscricao.crmeducacional.com/Login/2004" target="blank" class="txt15" style="color:black">Ingresso via Enem</a><i class="fa fa-arrow-circle-right pose-icon" aria-hidden="true" style="color:black"></i></label>
   
      </div>
      </div>
      
    </div>
  </div>
</div>


<div class="super_container">

	<!-- Header -->

	<header class="header">
			
		<!-- Top Bar -->
		<div class="top_bar" style="background-color:black">
			<div class="top_bar_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
								<ul class="top_bar_contact_list">
									<li>
									<div class="container">
  <div class="dropdown">
    <p  class=" dropdown-toggle " data-toggle="dropdown" style="color:white">
	<i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;
 <b>Selecione seu local</b>
    </p>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="#">Link 1</a>
      <a class="dropdown-item" href="#">Link 2</a>
      <a class="dropdown-item" href="#">Link 3</a>
    </div>
  </div>
</div>
									
	</li>
	<li>
	<i class="fa fa-phone" aria-hidden="true"></i>
	<div ><span class="font-txt-header"><b>OUVIDORIA</b><span></div>
     </li>
     <!-- <li>
		<div><span class="font-txt-header"><b>RESULTADO DO VESTIBULAR</b></span></div>
		</li>-->
<!--<li>
	<div><span class="font-txt-header"><b>EDITAIS</b></div>

       </li>-->
	</ul>
	<div class="top_bar_login ml-auto">
									<div class="login_button"><a href="https://unirb1.portaldominus.com.br/login.php" style="color:white" target="_blank"><i class="fa fa-graduation-cap" aria-hidden="true"></i> PORTAL DO ALUNO</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>

		<!-- Header Content -->
		<div class="header_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo_container">
								<a href="/">
									<div class="logo_text logo"><img src="images/home/redeunirb.png" ></div>
								</a>
							</div>
							<nav class="main_nav_contaner ml-auto">
								<ul class="main_nav">
								<li>
                <div class="navbar-nav">
                <div class="dropdown">
                <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="color:black">A UNIRB</a> 
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="institucional" style="color:black">Institucional</a>
                <a class="dropdown-item" href="http://portal.unirb.edu.br/cpa_novo/cpa_administrativo_ssa.php" style="color:black;font-size:15px">CPA</a>
                <a class="dropdown-item" href="https://docs.google.com/forms/d/1nQpnH2eQ7SN92IiuC_Kn_IygQhG28MU5b2gQzh5Nt-g/viewform?edit_requested=true#responses"  target="_blank" style="color:black;font-size:15px">Egressos</a>
								<a class="dropdown-item" href="/publicacoes" style="color:black;font-size:15px">Linha do Tempo</a>
								<a class="dropdown-item" href="/laboratorios" style="color:black;font-size:15px">Laboratórios</a>
								<a class="dropdown-item" href="/biblioteca" style="color:black;font-size:15px">Biblioteca</a>
                <a class="dropdown-item" href="/biblioteca" style="color:black;font-size:15px">Egressos e Estudantes</a>


                               </div>
								  </div>
								</li>

								<li>
                <div class="navbar-nav">
                <div class="dropdown">
                <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="color:black">Cursos</a> 
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="/graduacao" style="color:black;font-size:15px">Graduação</a>
                <a class="dropdown-item" href="/posgraduacao" style="color:black;font-size:15px">Pós-Graduação</a>
                <a class="dropdown-item" href="/ouvidoria" style="color:black;font-size:15px">EAD</a>

                               </div>
								  </div>
								</li>

									<li>
                  <div class="navbar-nav">
                  <div class="dropdown">
                  <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="color:black">Estude na UNIRB</a> 
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="/creditoestudantil" style="color:black;font-size:15px">Crédito Estudantil</a>
								<a class="dropdown-item" href="/convenios" style="color:black;font-size:15px">Convênios</a>
								<a class="dropdown-item" href="/estagio" style="color:black;font-size:15px">Programas de Estágio</a>
								<a class="dropdown-item" href="/intercambio" style="color:black;font-size:15px">Intercâmbio</a>

                               </div>
								  </div>
								</li>

								<li>
                
                 <div class="navbar-nav">
                 <div class=""><a href="https://www.creduc.com.br/unirb" style="color:black" target="_blank">REDECRED</b></a></div>               
                  </div>
      </li>
<li>
                  <div class="navbar-nav">
                 <div class=""><a href="https://api.whatsapp.com/send?1=pt_BR&phone=557133688300" target="_blank"> <i class="fa fa-whatsapp fa1 fa-2x" aria-hidden="true" ></i><span style="color:black"> CONTATO</span></a></div>             
								  </div>
								</li>

									
								</ul>



									<!-- Hamburger -->

									<div class="hamburger menu_mm">
									<i class="fa fa-bars menu_mm" aria-hidden="true" style="color:black"></i>
								</div>	
							</nav>
								
							</nav>

						</div>
					</div>
				</div>
			</div>
		</div>

			
	</header>

	<!-- Menu -->

	<div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<nav class="menu_nav">
			<ul class="menu_mm">
			<div class="logo_container">
			<a href="/">
		    <div class="logo_text logo"><img src="images/home/redeunirb.png" style="width:130%" ></div>
			</a>
		    </div>
				
			</ul>

			<ul class="menu_mm">
			<div class="dropdown">
      <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="color:black;font-size:18px">A UNIRB</a> 
      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
       <a class="dropdown-item" href="institucional" style="color:black;font-size:15px">Institucional</a>
       <a class="dropdown-item" href="/ouvidoria" style="color:black;font-size:15px">CPA</a>
      <a class="dropdown-item" href="https://docs.google.com/forms/d/1nQpnH2eQ7SN92IiuC_Kn_IygQhG28MU5b2gQzh5Nt-g/viewform?edit_requested=true#responses"  target="_blank" style="color:black;font-size:15px">Egressos</a>
			<a class="dropdown-item" href="/publicacoes" style="color:black;font-size:15px">Linha do Tempo</a>
			<a class="dropdown-item" href="/publicacoes" style="color:black;font-size:15px">Laboratórios</a>
			<a class="dropdown-item" href="/publicacoes" style="color:black;font-size:15px">Contato</a>
      

         </div>
				
			</ul>

			<ul class="menu_mm">
			<div class="dropdown">
            <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="color:black;font-size:18px">Cursos</a> 
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="/institucional" style="color:black;font-size:15px">Presencial</a>
            <a class="dropdown-item" href="/ouvidoria" style="color:black;font-size:15px">EAD</a>
			<a class="dropdown-item" href="/publicacoes" style="color:black;font-size:15px">Mestrado e Doutorado</a>
			<a class="dropdown-item" href="/publicacoes" style="color:black;font-size:15px">Cursos Livres</a>

         </div>
				
			</ul>

			<ul class="menu_mm">
			<div class="dropdown">
            <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="color:black;font-size:18px">Estude na UNIRB</a> 
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="/institucional" style="color:black;font-size:15px">Tipos de Graduação</a>
            <a class="dropdown-item" href="/ouvidoria" style="color:black;font-size:15px">Formas de Ingresso</a>
			<a class="dropdown-item" href="/vantagens" style="color:black;font-size:15px">Vantagens em ser aluno</a>
			<a class="dropdown-item" href="/creditoestudantil" style="color:black;font-size:15px">Crédito Estudantil</a>
			<a class="dropdown-item" href="/convenios" style="color:black;font-size:15px">Convênios</a>
			<a class="dropdown-item" href="/estagio" style="color:black;font-size:15px">Programas de Estágio</a>
			<a class="dropdown-item" href="/intercambio" style="color:black;font-size:15px">Intercâmbio</a>

		 </div>
		 <br>
				
			</ul>

			<ul class="menu_mm">
			<div class=""><a href="https://unirb1.portaldominus.com.br/login.php" style="color:black;font-size:15px" target="_blank"><i class="fa fa-graduation-cap" aria-hidden="true"></i> PORTAL DO ALUNO</a></div>
        </ul>
        <br>

        <ul class="menu_mm">
			<div class=""><a href="https://www.creduc.com.br/unirb" style="color:black;font-size:15px" target="_blank">REDECRED</a></div>
        </ul>
        <br>

			<ul class="menu_mm">
      <div class=""><a href="https://api.whatsapp.com/send?1=pt_BR&phone=557133688300" target="_blank"> <i class="fa fa-whatsapp fa1 fa-2x" aria-hidden="true" ></i><span style="color:black;font-size:15px"> CONTATO</span></a></div>             
      </ul>
			
		</nav>
	</div>
	
	
    <!-- Home -->
    
    
    
	<div class="col-md-12 card " id="container-conteudo"> 
  <img src="images/vestibular/banner.png" class="pose-img img-responsive" />
  </div>

  <div class="card">
  <div class="card-header">
    Featured
  </div>
  <div class="card-body">
  @foreach ($infoCSalvador as $curso)
  <h5 class="modal-title" id="exampleModalLabel" style="font-size:25px"> Curso: {{ $curso->nome }}</h5>
  <p class="card-text"><b style="color:#384158;font-size:15px">Portaria do MEC:</b> <span style="color:black">{{ $curso->portaria }}</span> </p>
  <p class="card-text"><b style="color:#384158;font-size:15px">Vagas:</b> <span style="color:black">{{ $curso->qtd_vagas }}</span> </p>
  <p class="card-text"><b style="color:#384158;font-size:15px">Turno:</b> <span style="color:black">{{ $curso->turno }}</span> </p>
  <p class="card-text"><b style="color:#384158;font-size:15px">Duração:</b> <span style="color:black">{{ $curso->duracao }}</span> </p>
  <p class="card-text"><b style="color:#384158;font-size:15px">Mensalidade:</b> <span style="color:black">{{ $curso->mensalidade }}</span> </p>
  <a href="{{ $curso->matriz_curricular }}" class="btn btn-primary" target="_blank">Matriz Curricular</a><br><br>

      <p class="text-justify"><b style="color:#384158;font-size:18px">Apresentação:</b><span style="color:black">
      @endforeach
        </div>
</div>

  


	<!-- Footer -->

	@include('partials._footer')	

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>





</body>
</html>