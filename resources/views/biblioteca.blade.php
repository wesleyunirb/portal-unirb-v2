﻿<title>Portal UNIRB| Biblioteca</title>

@include('partials._head')
<body>
@include('partials._header')




	<!-- banner -->

	<div id="carouselExampleControls" class="carousel slide pose-slide1" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="images/home/banner2.jpg" style="width:100%" class="tam-img1 img-responsive" />
    </div> 
  </div>
    
     <!-- indice -->
    

	<div class="home1">
		<div class="breadcrumbs_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="breadcrumbs">
							<ul>
								<li><a href="/">Home</a></li>
								<li>Biblioteca</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>			
    </div>

	<!-- Team -->
    <div class="card mb-3" style="max-width: 100%;background-color:#1e426e;margin-top:1%">
  <div class="row no-gutters">
    <div class="col-md-4">
    <img src="images/biblioteca/biblioteca.png" class="card-img" style="width:30%" >    </div>
    <div class="col-md-8">
      <div class="card-body">
     <h5 class="card-title txt50" style="color:white"  ><b>Biblioteca Digital</b></h5>
     <a href="https://dliportal.zbra.com.br/Login.aspx?key=UNIRB" target="_blank" class="btn btn-primary" style="margin-left:18%">Acesse</a>
      </div>
    </div>
  </div>
</div>
   
<br>

<div class="row">
  <div class="col-sm-6">
    <div class="">
      <div class="card-body">
        <h5 class="card-title">SISTEMA DE BIBLIOTECAS DA UNIRB</h5>
        <p class="card-text text-justify" style="color:black">

As Bibliotecas da UNIRB são informatizadas, seus serviços são automatizados
para otimização do empréstimo do acervo de livros e faz uso de sistema RM
biblios. O sistema operacionaliza os dados de identificação do material a ser
retirado da Base de Dados da Biblioteca (Processo Técnico), identificados através
de código de barras e de classificação CDD, gerando automaticamente transações
de empréstimo, devolução, renovação, reserva, recebimento de multas, pesquisa
do acervo por autor, título e assunto.
O acervo das Bibliotecas é constituído de: livros, periódicos impressos e on-line,
jornais, revistas, obras de referência, CD, CD-Rom, DVD. Fazem parte ainda do
acervo os Trabalhos de Conclusão de Cursos – TCC.</p>
<br>

<h5 class="card-title">INFRAESTRUTURA</h5>
<p class="card-text text-justify" style="color:black">
As Bibliotecas são climatizadas, compreendem em sua infraestrutura salão de
estudos, cabines individuais e salas de estudo em grupo, acesso à internet e ao
terminal de consulta do acervo (que também pode ser realizado no Balcão de
Atendimento). O sistema informatizado possibilita ao usuário realizar sua busca
por autor, título e assunto.</p>

<br>

<h5 class="card-title">POLÍTICA DE ATUALIZAÇÃO DO ACERVO</h5>
<p class="card-text text-justify" style="color:black">


A Política de atualização do Acervo do Sistema de Bibliotecas da UNIRB tem por
finalidade a definição de critérios para a aquisição, expansão e atualização do
acervo, que buscam atender prioritariamente às bibliografias básicas e
complementares dos cursos constantes nos projetos pedagógicos, manter o
acervo atualizado bem como a aplicação dos recursos orçamentários
disponibilizados pela Instituição.</p>

<br>

<h5 class="card-title">INFORMATIZAÇÃO DO ACERVO</h5>
<p class="card-text text-justify" style="color:black">

O controle do acervo encontra-se armazenado em sistema de banco de dados,
proporcionando segurança ao patrimônio da Biblioteca. O sistema RMbiblios da

Biblioteca está integrado com os setores: financeiro e a secretaria acadêmica,
permitindo uma cobertura completa das informações.
Um dos objetivos da automação é permitir à instituição ter ferramentas para
desenvolver o melhor trabalho possível em sua biblioteca e otimizar a tomada de
decisões.</p>

<br>

<h5 class="card-title">NORMAS E SERVIÇOS</h5>
<h5 class="card-title">SERVIÇOS:</h5>
<p class="card-text text-justify" style="color:black">



Empréstimo, devolução, renovação e reserva de livros;
Consulta ao acervo acesso on-line disponível no Portal UNIRB;
Portal de Periódicos;
Acesso a espaço para utilização de internet;
Conexão WI-FI;
Elaboração de ficha catalográfica para Trabalhos de Conclusão de Cursos – TCC;
Salas para estudo em grupo e cabines individuais;
Renovação e reserva de livros pela internet no Portal da UNIRB ou no Balcão de
Atendimento.</p>

<br>

<h5 class="card-title">NORMAS DE FUNCIONAMENTO DAS BIBLIOTECAS</h5>
<h5 class="card-title">EMPRÉSTIMOS</h5>
<p class="card-text text-justify" style="color:black">
O empréstimo somente será efetuado mediante apresentação de documento de
identificação. O usuário será responsável pelo material retirado, devendo devolvê-
lo até dia determinado em perfeito estado de conservação.
Com exceção das obras de referências e de consulta, todos os livros serão
emprestados.
Com relação às obras indicadas pelos professores como livros-texto será
reservado um exemplar para consulta local na Biblioteca.
As revistas serão emprestadas somente para consulta local, reprodução nas
dependências da Instituição, mediante apresentação da carteirinha.
Limites de empréstimo permitido por usuário e prazos:
Para alunos:
Três livros pelo prazo de 7 dias, prorrogável pelo mesmo período, na ausência
de pedidos de reserva;

Para professores:
Cinco livros pelo prazo de 15 dias, prorrogável pelo mesmo período, na
ausência de pedidos de reserva;
Comunidade Externa:
Consulta in lócus</p>

<br>



      </div>
    </div>
  </div>



  <div class="col-sm-6">
    <div class="">
      <div class="card-body">
        <h5 class="card-title">DEVOLUÇÃO</h5>
        <p class="card-text text-justify" style="color:black">
        
Na devolução os livros deverão esta na mesma situação que foi recebido, caso
contrário o usuário terá obrigação em substituí-lo por outro exemplar do mesmo
título e autor e com edição igual ou posterior à da obra danificada.
O usuário em pendência não poderá fazer novos empréstimos até sua total
regularização.
Para segurança do usuário, este deve examinar o livro antes de retirá–lo por
empréstimo, para verificar se está em perfeito estado, pois daí, a obra ficará sob
sua responsabilidade até o momento da devolução.
        </p>
        <br>

        <h5 class="card-title"> RENOVAÇÃO</h5>
        <p class="card-text text-justify" style="color:black">
       
A renovação do livro poderá ser feita pela internet no Portal da UNIRB ou levar a
publicação ao Balcão de atendimento, até o dia determinado para a devolução.
Caso a obra esteja em reserva a renovação não poderá ser efetuada.
        </p>
        <br>

        <h5 class="card-title">  RESERVA DE LIVROS</h5>
        <p class="card-text text-justify" style="color:black">
       
O usuário poderá efetuar a reserva do livro pela Internet no Portal da UNIRB ou no
Balcão de Atendimento
A ordem cronológica das solicitações de reserva será rigorosamente obedecida
pela Biblioteca;
Livro reservado ficará a disposição do usuário pelo prazo de 24 horas. Findo
este prazo, a reserva perderá a validade;
Não será permitida a renovação de empréstimo de livros que estão na lista de
reserva ou em poder do usuário solicitante.
        </p>
        <br>

        <h5 class="card-title">   PENALIDADE:</h5>
        <p class="card-text text-justify" style="color:black">      

Aos usuários que não devolverem o material emprestado no prazo determinado,
não será permitida a retirada de outros materiais até que efetuem o pagamento da
multa, conforme valores a seguir discriminados:
R$ 2,00 por livro/dia de atraso;
Por ocasião da devolução, o sistema calcula automaticamente o valor da multa e
suspende o direito de empréstimo do usuário na proporção de 03 (três) dias por
dia de atraso e por livro emprestado, somente ficando liberado o usuário para o
acesso a internet e uso do livro para consulta, após recolhimento dos valores da
multa e cumprindo o prazo de suspensão.
Na ocasião da renovação da matrícula, esta somente será permitida aos alunos
que não tenham pendências para com a Biblioteca.
A perda do material emprestado implica na sua reposição. Quando o material não
estiver disponível no mercado para aquisição, será substituído por outro
equivalente, segundo indicação da Chefia da Biblioteca.
        </p>
        <br>


        <h5 class="card-title">   OBSERVAÇÃO:</h5>
        <p class="card-text text-justify" style="color:black">
       
       
O aluno é responsável por todo material retirado da Biblioteca. Deverá sempre
GUARDAR o recibo de devolução. Somente aceitamos reclamações posteriores
mediante o COMPROVANTE de devolução.
Horário de funcionamento de segunda à sexta, das 7:00 às 22:00 e aos sábados
das 7:00 às 13:00, atendendo plenamente as necessidades de seus usuários.
        </p>
        <br>



      </div>
    </div>
  </div>
</div>


	
@include('partials._footer')

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>




</body>
</html>