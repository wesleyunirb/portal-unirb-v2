﻿
<title>Portal UNIRB | Crédito Estudantil</title>

@include('partials._head')



<body>

@include('partials._header')

     <!-- banner -->

<div id="carouselExampleControls" class="carousel slide pose-slide1" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="images/home/banner2.jpg" style="width:100%" class="tam-img1 img-responsive" />
    </div> 
  </div>

    
     <!-- indice -->
	<div class="home1" >
		<div class="breadcrumbs_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="breadcrumbs">
							<ul>
								<li><a href="/">Home</a></li>
								<li>Crédito estudantil</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>			
    </div>
    <br>

    <center><div class="col-md-8" ><p class="text-justify txt18"  style="color:black"> O Centro Universitário UNIRB 
    oferece as principais linhas de créditos educativos existentes no sistema educacional, 
    dando possibilidades ao aluno financiar seus estudos e pagar de acordo com as regras exigidas 
    pelas instituições financeiras.</p></div><center>


    <center><div class="col-md-8" >
      <div class="mb-3 p-3 alert-secondary" style="margin-top:3%">
      <div class="row no-gutters ">
      <div class="col-md-4 card-horizontal ">
          <img src="images/credito/fies.png" class="card-img"  style="margin-left:2%"> </div>
      <div class="col-md-8 center">
      <div class="card-body">
        <h4 class="txt22">FIES</h4><br>
      <p style="color:black" class="text18 text-justify">O Fundo de Financiamento Estudantil - FIES é um programa para o estudante universitário de instituição de ensino privado ter a possibilidade de garantir a sua tão sonhada graduação 
      e diploma universitário. Aqui podem valer-se deste recurso os alunos matriculados em curso superior e avaliado pelo MEC.</p><br>
        <center><a href="http://fies.mec.gov.br/" target="_blank"><button class="btn btn-warning" style="color:white"><b>Saiba Mais</b></button></center></a>   
            </div>
          </div>
        </div>
      </div>
    

  <div class=" mb-3 blue ">
  <div class="row no-gutters">
    <div class="col-md-4 card-horizontal ">
    <img src="images/credito/pravaler.png" class="card-img" style="margin-left:2%;" > </div>
    <div class="col-md-8 ">
    <div class="card-body">
     <h4 style="color:#1f466f" class="txt19">PRAVALER</h4><br>
   <p style="color:black" class="text18 text-justify ">O Centro Universitário UNIRB em parceria com o PRAVALER, concede ao aluno mais uma 
   oportunidade de financiar seus estudos e alcançar o diploma universitário.</p><br>
   <center><a href="https://www.pravaler.com.br/" target="_blank"><button class="btn btn-warning" style="color:white"><b>Saiba Mais</b></button></center></a>

     </div>
    </div>
  </div>
</div>


<div class=" mb-3 p-3 alert-secondary  ">
  <div class="row no-gutters">
    <div class="col-md-4 card-horizontal">
    <img src="images/credito/enem.png" class="card-img" style="margin-left:2%" > </div>
    <div class="col-md-8">
    <div class="card-body">
     <h4 style= "txt19">ENEM</h4><br>
   <p style="color:black" class="text18 text-justify ">Através do Exame Nacional de Ensino médio - ENEM, é possivel selecionar estudantes para ingressar 
     em universidades federais e para programas do Governo Federal, como Fies, Sisu e Prouni.</p><br>
    <center><a href="https://enem.inep.gov.br/" target="_blank"><button class="btn btn-warning" style="color:white"><b>Saiba Mais</b></button></center></a>

     </div>
    </div>
  </div>
</div>

</div><center>


<!--footer-->	
@include('partials._footer')

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>




</body>
</html>