﻿<title>Portal UNIRB | Convênios</title>

@include('partials._head')

<body>

@include('partials._header')

<!-- banner -->

<div id="carouselExampleControls" class="carousel slide pose-slide1" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="images/convenio/convenio.png" style="width:100%" class="tam-img1 img-responsive" />
    </div> 
  </div>

    
     <!-- indice -->
	<div class="home1">
		<div class="breadcrumbs_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="breadcrumbs">
							<ul>
								<li><a href="/">Home</a></li>
								<li>Convênios</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>			
    </div>


<center><div class="col-md-8" style="margin-top:2%"><p class="text-justify" style="color:black;font-size:18px;">O Centro Universitário UNIRB possue convênio de Bolsa Parcial de Estudo para estudantes ingressantes através da Campanha Promocional “ESPORTE CLUBE BAHIA".</p></div><center>
<br> <br>

<div class="card mb-3">
<a href="https://www.unirb.edu.br/salvador/farb/files/pdf/regulamento_concessao_bolsas_convenio_esporte_clube_bahia.pdf"> <img src="images/convenio/baner convenio.png" class="card-img-top img-responsive mx-auto" alt="..."/> </a>
</div>

<center>
<button type="button" class="btn btn-primary"> <a href="https://www.unirb.edu.br/salvador/farb/files/pdf/regulamento_concessao_bolsas_convenio_esporte_clube_bahia.pdf" target="_blank"> Acesse o Regulamento </a></button> 
<button type="button" class="btn btn-primary"> <a href="http://www.unirb.edu.br/sistema_inscricao_bahia/" target="_blank"> Inscreva-se</a></button>
</div>
</center>


<br>
<br>

@include('partials._footer')
	
</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>




</body>
</html>