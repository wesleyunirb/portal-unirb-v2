﻿<title>Portal Unirb | Graduação </title>

@include('partials._head')
<body>
@include('partials._header')

<!-- Bacharelado -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#1e426e">
        <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/logo.png"  style="width:100%"></div><b style="color:white">&nbsp; Bacharelado</b> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="card mb-3" style="max-width: 100%;">
  <div class="row no-gutters">
    <div class="col-md-8">
      <div class="card-body">
      <ul style="color:black">
        <a class="dropdown-item">Administração</a>
        <a class="dropdown-item" >Administração Pública</a>
        <a class="dropdown-item">Arquitetura e Urbanismo</a>
        <a class="dropdown-item">Biomedicina</a>
        <a class="dropdown-item">Ciências Contábeis</a>
        <a class="dropdown-item">Comunicação Social - Jornalismo</a>
        <a class="dropdown-item">Comunicação Social - Radialismo</a>
        <a class="dropdown-item">Comunicação Social - Rádio, Tv e Internet</a>
        <a class="dropdown-item">Direito</a>
        <a class="dropdown-item">Educação Física</a>
        <a class="dropdown-item">Enfermagem</a>
        <a class="dropdown-item">Engenharia de Petróleo</a>
        <a class="dropdown-item">Engenharia Mecânica</a>
        <a class="dropdown-item">Engenharia Sanitária e Ambiental</a>
        <a class="dropdown-item">Engenharia Agronômica</a>
        <a class="dropdown-item">Engenharia Automotiva</a>
        <a class="dropdown-item">Engenharia Cartográfica e de Agrimensura</a>
        <a class="dropdown-item">Engenharia Civil</a>
        <a class="dropdown-item">Engenharia da Computação</a>
        <a class="dropdown-item">Engenharia de Alimentos</a>
        <a class="dropdown-item">Engenharia de Pesca</a>
        <a class="dropdown-item">Engenharia de Produção</a>
        <a class="dropdown-item">Engenharia de Software</a>
        <a class="dropdown-item">Engenharia Elétrica</a>
        <a class="dropdown-item">Engenharia Mecatrônica</a>
        <a class="dropdown-item">Engenharia Química</a>
        <a class="dropdown-item">Estética e Coméstica</a>
        <a class="dropdown-item">Farmácia</a>
        <a class="dropdown-item">Fisioterapia</a>
        <a class="dropdown-item">Fonoaudiologia</a>
        <a class="dropdown-item">Gerontologia</a>
        <a class="dropdown-item">Medicina Veterinária</a>
        <a class="dropdown-item">Nutrição</a>
        <a class="dropdown-item">Odontologia</a>
        <a class="dropdown-item">Psicologia</a>
        <a class="dropdown-item">Publicidade e Propaganda</a>
        <a class="dropdown-item">Quiropraxia</a>
        <a class="dropdown-item">Serviço Social</a>
        <a class="dropdown-item">Zootecnia</a>
        </ul>  

       

      </div>
    </div>
  </div>
</div>
   
      </div>
      
    </div>
  </div>
</div>

<!-- TECNOLOGO -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#1e426e">
        <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/logo.png"  style="width:100%"></div><b style="color:white"> &nbsp;Tecnólogo</b> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="card mb-3" style="max-width: 100%;">
  <div class="row no-gutters">
    <div class="col-md-8">
      <div class="card-body">
      <ul>
          <a class="dropdown-item" href="#">Acupuntura</a>
          <a class="dropdown-item" href="#">Agropecuária</a>
          <a class="dropdown-item" href="#">Alimentos</a>
          <a class="dropdown-item" href="#">Análise e Desenvolvimento de Sistemas </a>
          <a class="dropdown-item" href="#">Aquicultura </a>
          <a class="dropdown-item" href="#">Bicombustíveis </a>
          <a class="dropdown-item" href="#">Construção Naval </a>
          <a class="dropdown-item" href="#">Defesa Cibernética </a>
          <a class="dropdown-item" href="#">Design de Interiores </a>
          <a class="dropdown-item" href="#">Design de Moda </a>
          <a class="dropdown-item" href="#">Design Gráfico </a>
          <a class="dropdown-item" href="#">Fotografia </a>
          <a class="dropdown-item" href="#">Futebol </a>
          <a class="dropdown-item" href="#">Gastronomia </a>
          <a class="dropdown-item" href="#">Gerontologia </a>
          <a class="dropdown-item" href="#">Gestão Comercial </a>
          <a class="dropdown-item" href="#">Gestão de Recursos Humanos </a>
          <a class="dropdown-item" href="#">Gestão Financeira </a>
          <a class="dropdown-item" href="#">Gestão Hospitalar </a>
          <a class="dropdown-item" href="#">Jogos Digitais </a>
          <a class="dropdown-item" href="#">Logística </a>
          <a class="dropdown-item" href="#">Manutenção de Aeronaves </a>
          <a class="dropdown-item" href="#">Marketing </a>
          <a class="dropdown-item" href="#">Metalurgia </a>
          <a class="dropdown-item" href="#">Multimídia </a>
          <a class="dropdown-item" href="#">Oftálmica </a>
          <a class="dropdown-item" href="#">Paisagismo </a>
          <a class="dropdown-item" href="#">Petróleo e Gás</a>
          <a class="dropdown-item" href="#">Pilotagem de Aeronaves </a>
          <a class="dropdown-item" href="#">Podologia </a>
          <a class="dropdown-item" href="#">Produção de Cacau e Chocolates </a>
          <a class="dropdown-item" href="#">Radiologia </a>
          <a class="dropdown-item" href="#">Redes de Computadores </a>
          <a class="dropdown-item" href="#">Refrigeração e Climatização </a>
          <a class="dropdown-item" href="#">Segurança no Trabalho </a>
          <a class="dropdown-item" href="#">Segurança no Trânsito </a>
          <a class="dropdown-item" href="#">Viticultura e Enologia </a>
      </ul>  

       

      </div>
    </div>
  </div>
</div>
   
      </div>
      
    </div>
  </div>
</div>

<!-- TECNOLOGO -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#1e426e">
        <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/logo.png"  style="width:100%"></div><b style="color:white"> &nbsp;Licenciatura</b> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="card mb-3" style="max-width: 100%;">
  <div class="row no-gutters">
    <div class="col-md-8">
      <div class="card-body">
      <ul>
    <a class="dropdown-item" href="#">Educação Física</a>
    <a class="dropdown-item" href="#">Formação Pedagógica p/ Portadores de Ens. Superior</a>
    <a class="dropdown-item" href="#">Pedagogia</a>
    </ul>  
       

      </div>
    </div>
  </div>
</div>
   
      </div>
      
    </div>
  </div>
</div>

<!-- TECNOLOGO -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#1e426e">
        <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/logo.png"  style="width:100%"></div><b style="color:white"> &nbsp;Sequencial</b> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="card mb-3" style="max-width: 100%;">
  <div class="row no-gutters">
    <div class="col-md-8">
      <div class="card-body">
      <ul>
      <a class="dropdown-item" href="#">Arbitragem</a>
      <a class="dropdown-item" href="#">Instrumentação em Odontologia</a>
      <a class="dropdown-item" href="#">Saúde do Idoso</a>
    </ul>   
       

      </div>
    </div>
  </div>
</div>
   
      </div>
      
    </div>
  </div>
</div>

<!-- TECNOLOGO -->
<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#1e426e">
        <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/logo.png"  style="width:100%"></div><b style="color:white"> &nbsp;EAD</b> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="card mb-3" style="max-width: 100%;">
  <div class="row no-gutters ">
    <div class="col-md-8">
      <div class="card-body">
      <ul>
    <a class="dropdown-item" href="#">Administração</a>
    <a class="dropdown-item" href="#">Administração Pública </a>
    <a class="dropdown-item" href="#">Análise e Desenvolvimento de Sistemas </a>
    <a class="dropdown-item" href="#">Ciências Contábeis </a>
    <a class="dropdown-item" href="#">Educação Física (Licenciatura) </a>
    <a class="dropdown-item" href="#">Educação Física (Bacharelado) </a>
    <a class="dropdown-item" href="#">Engenharia Civil </a>
    <a class="dropdown-item" href="#">Engenharia da Produção </a>
    <a class="dropdown-item" href="#">Farmácia </a>
    <a class="dropdown-item" href="#">Filosofia (Licenciatura) </a>
    <a class="dropdown-item" href="#">Fisioterapia </a>
    <a class="dropdown-item" href="#">Fonoaudiologia </a>
    <a class="dropdown-item" href="#">Gestão de Recursos Humanos </a>
    <a class="dropdown-item" href="#">Gestão de Marketing </a>
    <a class="dropdown-item" href="#">Letras (Licenciatura) </a>
    <a class="dropdown-item" href="#">Logística </a>
    <a class="dropdown-item" href="#">Negócios Imobiliários (Tecnólogo)  </a>
    <a class="dropdown-item" href="#">Nutrição </a>
    <a class="dropdown-item" href="#">Pedagogia (Licenciatura)  </a>
    <a class="dropdown-item" href="#">Serviço Social </a>
    <a class="dropdown-item" href="#">Terapia Ocupacional (Bacharelado) </a>
    <a class="dropdown-item" href="#">Teologia (Bacharelado) </a>
    <a class="dropdown-item" href="#">Rádio, TV e Internet (Tecnólogo) </a>
    <a class="dropdown-item" href="#">Segurança Pública (Tecnólogo) </a>
    <a class="dropdown-item" href="#">Segurança no Trânsito (Tecnólogo) </a>
  </ul>    
       

      </div>
    </div>
  </div>
</div>
   
      </div>
      
    </div>
  </div>
</div>





	<!-- banner -->

	<div id="carouselExampleControls" class="carousel slide pose-slide2" data-ride="carousel" >
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="images/home/banner2.jpg" style="width:100%" class="tam-img1 img-responsive" />
    </div> 
  </div>
    
     <!-- indice -->
    

	<div class="home1" >
		<div class="breadcrumbs_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="breadcrumbs">
							<ul>
								<li><a href="/">Home</a></li>
								<li>Graduação</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>			
    </div>

    <div class="card-deck " style="margin-top:2%">
  <div class="card">
    <div class="card-body">
      <h5 class="card-title txt25">Bacharelado</h5>
      <p class="card-text" style="color:black">Encontre um curso de graduação superior e começe a estudar na UNIRB.</p><br>
<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">  Confira </button>  

</div>
  </div>
  <div class="card">
    <div class="card-body">
    <h5 class="card-title txt25">Tecnólogo</h5>
      <p class="card-text" style="color:black">Matricule-se na insituição de ensino com os melhores cursos de tecnologia.</p><br>
      <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1">  Confira </button>  
    </div>
  </div>

  <div class="card">
    <div class="card-body">
    <h5 class="card-title txt25">Licenciatura</h5>
      <p class="card-text" style="color:black">Excelência em cursos de licenciatura. Na UNIRB é mais que uma faculdade.</p><br>
      <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">  Confira </button>  
    </div>
  </div>

  <div class="card">
    <div class="card-body">
    <h5 class="card-title txt25">Sequencial</h5>
      <p class="card-text" style="color:black">Cursos alinhados com as tendências do mercado. Garantia de melhor desempenho.</p><br>
      <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal3">  Confira </button>  
    </div>
  </div>

  <div class="card">
    <div class="card-body">
    <h5 class="card-title txt25">EAD</h5>
      <p class="card-text" style="color:black">Graduação com flexibilidade. Melhor aproveitamento em todas as áreas.</p><br>
      <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal4">  Confira </button>  
    </div>
  </div>
</div>

 

</div>
	
<br>
<br>
<!--CURSOS EM DESTAQUE-->

<div class="section_title_container text-center col-md-12">
						<h2 class="section_title color-blue txt50">Cursos em Destaque</h2>
						
        </div>
        <br><br>

        <div class="card-group pose-cardimg" >
  <div class=" col-md-2">
    <img src="images/graduacao/odontologia.png" class="card-img-top" >
    <div class="card-body">
      <h5 class="card-title txt18" style="color:black">Odontologia</h5><br>

    </div>
  </div>
  
  <div class=" col-md-2">
  <img src="images/graduacao/EngenhariaSoft.png" class="card-img-top" >
    <div class="card-body">
      <h5 class="card-title txt18" style="color:black">Engenharia da Computação</h5>

    </div>
  </div>

  <div class=" col-md-2">
  <img src="images/graduacao/EducacaoFis.png" class="card-img-top" >
    <div class="card-body">
      <h5 class="card-title txt18" style="color:black">Educação Física</h5><br>

    </div>
  </div>

  <div class=" col-md-2">
  <img src="images/graduacao/Biomedicina.png" class="card-img-top" >
    <div class="card-body">
      <h5 class="card-title txt18" style="color:black">Biomedicina</h5><br>

    </div>
  </div>

  <div class=" col-md-2">
  <img src="images/graduacao/cosmetica.png" class="card-img-top" >
    <div class="card-body">
      <h5 class="card-title txt18" style="color:black;">Cosmética</h5><br>
    </div>
  </div>
  
</div>


<center> <div class="card col-md-4" style="margin-top:2%;border:1px solid #254170">
            <div class="card-body">
              <h5 class="card-title txt25" style="color:#254170">Processo Seletivo <b>UNIRB</b></h5>
              <a href="vestibular" class="btn btn-warning" style="color:black"><b>Quero me increver</b> </a>
            </div>
          </div>
</center>

	
@include('partials._footer')

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>




</body>
</html>