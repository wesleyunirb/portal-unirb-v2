<!-- Name Field -->
<div class="form-group col-md-3">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $users->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group col-md-3">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $users->email !!}</p>
</div>

<!-- Password Field -->
<!--<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{!! $users->password !!}</p>
</div>-->

<!-- Default Field -->
<div class="form-group col-md-3">
    {!! Form::label('default', 'Default:') !!}
    <p>{!! $users->default !!}</p>
</div>

<!-- Ti Field -->
<div class="form-group col-md-3">
    {!! Form::label('ti', 'Ti:') !!}
    <p>{!! $users->ti !!}</p>
</div>

<!-- Mkt Field -->
<div class="form-group col-md-3">
    {!! Form::label('mkt', 'Mkt:') !!}
    <p>{!! $users->mkt !!}</p>
</div>

<!-- Remember Token Field -->
<!--<div class="form-group col-md-3">
    {!! Form::label('remember_token', 'Remember Token:') !!}
    <p>{!! $users->remember_token !!}</p>
</div>-->

