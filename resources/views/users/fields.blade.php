<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Default Field -->
<div class="form-group col-sm-6">
    {!! Form::label('default', 'Default:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('default', 0) !!}
        {!! Form::checkbox('default', '1', null) !!}
    </label>
</div>


<!-- Ti Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ti', 'Ti:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('ti', 0) !!}
        {!! Form::checkbox('ti', '1', null) !!}
    </label>
</div>


<!-- Mkt Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mkt', 'Mkt:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('mkt', 0) !!}
        {!! Form::checkbox('mkt', '1', null) !!}
    </label>
</div>


<!-- Remember Token Field -->
<div class="form-group col-sm-6">
    {!! Form::label('remember_token', 'Remember Token:') !!}
    {!! Form::text('remember_token', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-danger">Cancel</a>
</div>
