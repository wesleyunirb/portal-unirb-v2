﻿<!DOCTYPE html>
<html lang="pt">
<head>
@foreach ($nome as $name) 
<title>Portal UNIRB | {{$name->nome}} </title>
@endforeach
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Portal universitário UNIRB">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"><link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="css/main_styles.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link rel="stylesheet" type="text/css" href="css/blog_responsive.css">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">




<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!-- MASK-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<script>

		function fale()
		{
			
		var nome = document.getElementById("nome3").value;
        var telefone = document.getElementById("telefone3").value;
        var mensagem = document.getElementById("mensagem3").value;
	
	
	
			if(nome,telefone,mensagem == ""){
		  swal("Erro ao Enviar!", "Por Favor preencha os campos corretamente", "error");
	
	
		}else{
		swal("Enviado com sucesso !", "Obrigado! Entraremos em contato", "success");
		 // Salva a lista alterada
		}
			
		}

    function participar()
		{
			
		var nome = document.getElementById("nome").value;
        var telefone = document.getElementById("telefone").value;
        var email = document.getElementById("email").value;
        var cpf = document.getElementById("cpf").value;
	
	
	
			if(nome,telefone,email,cpf == ""){
		  swal("Erro ao Enviar!", "Por Favor preencha os campos corretamente", "error");
	
	
		}else{
		swal("Inscrição realizada sucesso !", "", "success");
    
		}
			
		}

		function myMap() {
var mapProp= {
	
	 @foreach ($latitude as $lati)
	 @foreach ($longitude as $longe)
   center:new google.maps.LatLng({{$lati->latitude}},{{$longe->longitude}}),
  @endforeach
  @endforeach
  zoom:16,
};

var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);


}
   </script>

<script>
 $(function(){
	 //Script hide/show  BUTTON evento1
    var input = document.getElementById("inputevent1").value; 
   $('#inputevent1').on('change', function(){
      $(".divbutton1")[input == '1' ? 'show':'show']();
   }).change();

   //Script hide/show  BUTTON evento2
   var input = document.getElementById("inputevent2").value; 
   $('#inputevent2').on('change', function(){
      $(".divbutton2")[input == '1' ? 'show':'show']();
   }).change();

   //Script hide/show  BUTTON evento3
   var input = document.getElementById("inputevent3").value; 
   $('#inputevent3').on('change', function(){
      $(".divbutton3")[input == '1' ? 'show':'show']();
   }).change();

   //Script hide/show  BUTTON evento4
   var input = document.getElementById("inputevent4").value; 
   $('#inputevent4').on('change', function(){
      $(".divbutton4")[input == '1' ? 'show':'show']();
   }).change();

    //Script hide/show  BUTTON evento5
    var input = document.getElementById("inputevent5").value; 
   $('#inputevent5').on('change', function(){
      $(".divbutton5")[input == '1' ? 'show':'show']();
   }).change();

   //Script hide/show  BUTTON evento6
   var input = document.getElementById("inputevent6").value; 
   $('#inputevent6').on('change', function(){
      $(".divbutton6")[input == '1' ? 'show':'show']();
   }).change();

   //Script hide/show  BUTTON evento6
   var input = document.getElementById("inputevent7").value; 
   $('#inputevent7').on('change', function(){
      $(".divbutton7")[input == '1' ? 'show':'show']();
   }).change();

   //Script hide/show  BUTTON evento6
   var input = document.getElementById("inputevent8").value; 
   $('#inputevent8').on('change', function(){
      $(".divbutton8")[input == '1' ? 'show':'show']();
   }).change();

  
});



</script>




</head>
<body>

  <!-- INSCRIÇÃO EVENTO 1 -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:100%">
      
    <div class="modal-header" style="background-color:#1e426e">

    @foreach ($evento1 as $event1)
    <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white"> {{$event1->titulo}}</b> </h5>
    @endforeach
          
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
     
	  <div class="card mb-3" >

    <div class="col-md-12">
	<div class="carousel-inner">
    <div class="carousel-item active">
    @foreach ($evento1 as $event1)
    <img src="uploads/{{$event1->logomarca}}" style="width:100%" class="tam-img1 img-responsive"> 
   @endforeach  
      </div> 
  </div>    
</div>

    <div class="col-md-12">
      <div class="card-body" >
	  <div class=" text-center">
	  <div class="card col-md-12" >
  <div class="card-body">
   <center> <h5 class="card-title txt25">Cadastre-se</h5></center>
   {!! Form:: open(['route'=>'inscricao.save', 'method' => 'POST']) !!}
  <div class="form-group">
    <input type="text" class="form-control" id="nome"  name="nome" placeholder="Nome" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="cpf" name="cpf" placeholder="Cpf" required>
  </div>
  <div class="form-group">
    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" required>
  </div>

 

  @foreach ($evento1 as $event1)
  <div class="form-group">
    <input type="hidden" class="form-control" id="eventoid" value="{{$event1->id}}" name="eventoid" >
  </div>

 @endforeach

 @foreach ($formunidade1 as $formunidade1)
  <div class="form-group">
    <input type="hidden" class="form-control" id="unidadeid" value="{{$formunidade1->id}}" name="unidadeid" >
  </div>

 @endforeach

 
  
  <button type="submit" class="btn btn-primary"  onclick="participar()">Inscreva-se</button></a>
  {!!Form:: close()!!} 
                    
  </div>
</div>
</div>
      </div>
    </div>
  </div>
</div>
      </div>
    </div>
  </div>


  <!-- INSCRIÇÂO EVENTO 2 -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:100%">
      
    <div class="modal-header" style="background-color:#1e426e">

    @foreach ($evento2 as $event2)
    <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white"> {{$event2->titulo}}</b> </h5>
    @endforeach

    
          
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
     
	  <div class="card mb-3" >

    <div class="col-md-12">
	<div class="carousel-inner">
    <div class="carousel-item active">
    @foreach ($evento2 as $event2)
    <img src="uploads/{{$event2->logomarca}}" style="width:100%" class="tam-img1 img-responsive"> 
   @endforeach  
      </div> 
  </div>    
</div>

    <div class="col-md-12">
      <div class="card-body" >
	  <div class=" text-center">
	  <div class="card col-md-12" >
  <div class="card-body">
   <center> <h5 class="card-title txt25">Cadastre-se</h5></center>
   {!! Form:: open(['route'=>'inscricao.save', 'method' => 'POST']) !!}
  <div class="form-group">
    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="cpf" name="cpf" placeholder="Cpf" required>
  </div>
  <div class="form-group">
    <input type="email" class="form-control" id="email " name="email" placeholder="Email" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" required>
  </div>

  @foreach ($evento2 as $event2)
  <div class="form-group">
    <input type="hidden" class="form-control" id="eventoid" value="{{$event2->id}}" name="eventoid" >
  </div>
 @endforeach

 @foreach ($formunidade2 as $formunidade2)
  <div class="form-group">
    <input type="hidden" class="form-control" id="unidadeid" value="{{$formunidade2->id}}" name="unidadeid" >
  </div>

 @endforeach

  <button type="submit" class="btn btn-primary" onclick="participar()" >Inscreva-se</button></a>
  {!!Form:: close()!!} 
  </div>
</div>
</div>
      </div>
    </div>
  </div>
</div>
      </div>
    </div>
  </div>

  <!-- INSCRIÇÂO EVENTO 3 -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:100%">
      
    <div class="modal-header" style="background-color:#1e426e">

    @foreach ($evento3 as $event3)
    <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white"> {{$event3->titulo}}</b> </h5>
    @endforeach
          
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
     
	  <div class="card mb-3" >

    <div class="col-md-12">
	<div class="carousel-inner">
    <div class="carousel-item active">
    @foreach ($evento3 as $event3)
    <img src="uploads/{{$event3->logomarca}}" style="width:100%" class="tam-img1 img-responsive"> 
   @endforeach  
      </div> 
  </div>    
</div>
    <div class="col-md-12">
      <div class="card-body" >
	  <div class=" text-center">
	  <div class="card col-md-12" >
  <div class="card-body">
  <center> <h5 class="card-title txt25">Cadastre-se</h5></center>
   {!! Form:: open(['route'=>'inscricao.save', 'method' => 'POST']) !!}
  <div class="form-group">
    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="cpf" name="cpf" placeholder="Cpf" required>
  </div>
  <div class="form-group">
    <input type="email" class="form-control" id="email " name="email" placeholder="Email" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" required>
  </div>

  @foreach ($evento3 as $event3)
  <div class="form-group">
    <input type="hidden" class="form-control" id="eventoid" value="{{$event3->id}}" name="eventoid" >
  </div>
 @endforeach

 @foreach ($formunidade3 as $formunidade3)
  <div class="form-group">
    <input type="hidden" class="form-control" id="unidadeid" value="{{$formunidade3->id}}" name="unidadeid" >
  </div>

 @endforeach

  <button type="submit" class="btn btn-primary" onclick="participar()" >Inscreva-se</button></a>
  {!!Form:: close()!!} 
  </div>
</div>
</div>

      </div>
    </div>
  
  </div>
</div>

      </div>
      
    </div>
  </div>

  <!-- INSCRIÇÂO EVENTO 4 -->
<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:100%">
      
    <div class="modal-header" style="background-color:#1e426e">

    @foreach ($evento4 as $event4)
    <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white"> {{$event4->titulo}}</b> </h5>
    @endforeach
          
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
     
	  <div class="card mb-3" >

    <div class="col-md-12">
	<div class="carousel-inner">
    <div class="carousel-item active">
    @foreach ($evento4 as $event4)
    <img src="uploads/{{$event4->logomarca}}" style="width:100%" class="tam-img1 img-responsive"> 
   @endforeach  
      </div> 
  </div>    
</div>
    <div class="col-md-12">
      <div class="card-body" >
	  <div class=" text-center">
	  <div class="card col-md-12" >
  <div class="card-body">
  <center> <h5 class="card-title txt25">Cadastre-se</h5></center>
   {!! Form:: open(['route'=>'inscricao.save', 'method' => 'POST']) !!}
  <div class="form-group">
    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="cpf" name="cpf" placeholder="Cpf" required>
  </div>
  <div class="form-group">
    <input type="email" class="form-control" id="email " name="email" placeholder="Email" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" required>
  </div>

  @foreach ($evento4 as $event4)
  <div class="form-group">
    <input type="hidden" class="form-control" id="eventoid" value="{{$event4->id}}" name="eventoid" >
  </div>
 @endforeach

 @foreach ($formunidade4 as $formunidade4)
  <div class="form-group">
    <input type="hidden" class="form-control" id="unidadeid" value="{{$formunidade4->id}}" name="unidadeid" >
  </div>

 @endforeach

  <button type="submit" class="btn btn-primary" onclick="participar()" >Inscreva-se</button></a>
  {!!Form:: close()!!} 
  </div>
</div>
</div>

      </div>
    </div>
  
  </div>
</div>
      </div> 
    </div>
  </div>


  <!-- INSCRIÇÂO EVENTO 5 -->
<div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:100%">
      
    <div class="modal-header" style="background-color:#1e426e">

    @foreach ($evento5 as $event5)
    <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white"> {{$event5->titulo}}</b> </h5>
    @endforeach
          
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
     
	  <div class="card mb-3" >

    <div class="col-md-12">
	<div class="carousel-inner">
    <div class="carousel-item active">
    @foreach ($evento5 as $event5)
    <img src="uploads/{{$event5->logomarca}}" style="width:100%" class="tam-img1 img-responsive"> 
   @endforeach  
      </div> 
  </div>    
</div>
    <div class="col-md-12">
      <div class="card-body" >
	  <div class=" text-center">
	  <div class="card col-md-12" >
  <div class="card-body">
  <center> <h5 class="card-title txt25">Cadastre-se</h5></center>
   {!! Form:: open(['route'=>'inscricao.save', 'method' => 'POST']) !!}
  <div class="form-group">
    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="cpf" name="cpf" placeholder="Cpf" required>
  </div>
  <div class="form-group">
    <input type="email" class="form-control" id="email " name="email" placeholder="Email" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" required>
  </div>

  @foreach ($evento5 as $event5)
  <div class="form-group">
    <input type="hidden" class="form-control" id="eventoid" value="{{$event5->id}}" name="eventoid" >
  </div>
 @endforeach

 @foreach ($formunidade5 as $formunidade5)
  <div class="form-group">
    <input type="hidden" class="form-control" id="unidadeid" value="{{$formunidade5->id}}" name="unidadeid" >
  </div>

 @endforeach
  <button type="submit" class="btn btn-primary"  onclick="participar()">Inscreva-se</button></a>
  {!!Form:: close()!!} 
  </div>
</div>
</div>

      </div>
    </div>
  
  </div>
</div>
      </div> 
    </div>
  </div>


  <!-- INSCRIÇÂO EVENTO 6 -->
<div class="modal fade" id="exampleModal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:100%">
      
    <div class="modal-header" style="background-color:#1e426e">

    @foreach ($evento6 as $event6)
    <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white"> {{$event6->titulo}}</b> </h5>
    @endforeach
          
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
     
	  <div class="card mb-3" >

    <div class="col-md-12">
	<div class="carousel-inner">
    <div class="carousel-item active">
    @foreach ($evento6 as $event6)
    <img src="uploads/{{$event6->logomarca}}" style="width:100%" class="tam-img1 img-responsive"> 
   @endforeach  
      </div> 
  </div>    
</div>
    <div class="col-md-12">
      <div class="card-body" >
	  <div class=" text-center">
	  <div class="card col-md-12" >
  <div class="card-body">
  <center> <h5 class="card-title txt25">Cadastre-se</h5></center>
   {!! Form:: open(['route'=>'inscricao.save', 'method' => 'POST']) !!}
  <div class="form-group">
    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="cpf" name="cpf" placeholder="Cpf" required>
  </div>
  <div class="form-group">
    <input type="email" class="form-control" id="email " name="email" placeholder="Email" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" required>
  </div>

  @foreach ($evento6 as $event6)
  <div class="form-group">
    <input type="hidden" class="form-control" id="eventoid" value="{{$event6->id}}" name="eventoid" >
  </div>
 @endforeach

 @foreach ($formunidade6 as $formunidade6)
  <div class="form-group">
    <input type="hidden" class="form-control" id="unidadeid" value="{{$formunidade6->id}}" name="unidadeid" >
  </div>

 @endforeach


  <button type="submit" class="btn btn-primary" onclick="participar()">Inscreva-se</button></a>
  {!!Form:: close()!!} 
  </div>
</div>
</div>

      </div>
    </div>
  
  </div>
</div>
      </div> 
    </div>
  </div>

   <!-- INSCRIÇÂO EVENTO 7 -->
<div class="modal fade" id="exampleModal7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:100%">
      
    <div class="modal-header" style="background-color:#1e426e">

    @foreach ($evento7 as $event7)
    <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white"> {{$event7->titulo}}</b> </h5>
    @endforeach
          
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
     
	  <div class="card mb-3" >

    <div class="col-md-12">
	<div class="carousel-inner">
    <div class="carousel-item active">
    @foreach ($evento7 as $event7)
    <img src="uploads/{{$event7->logomarca}}" style="width:100%" class="tam-img1 img-responsive"> 
   @endforeach  
      </div> 
  </div>    
</div>
    <div class="col-md-12">
      <div class="card-body" >
	  <div class=" text-center">
	  <div class="card col-md-12" >
  <div class="card-body">
  <center> <h5 class="card-title txt25">Cadastre-se</h5></center>
   {!! Form:: open(['route'=>'inscricao.save', 'method' => 'POST']) !!}
  <div class="form-group">
    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="cpf" name="cpf" placeholder="Cpf" required>
  </div>
  <div class="form-group">
    <input type="email" class="form-control" id="email " name="email" placeholder="Email" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" required>
  </div>

  @foreach ($evento7 as $event7)
  <div class="form-group">
    <input type="hidden" class="form-control" id="eventoid" value="{{$event7->id}}" name="eventoid" >
  </div>
 @endforeach

 @foreach ($formunidade7 as $formunidade7)
  <div class="form-group">
    <input type="hidden" class="form-control" id="unidadeid" value="{{$formunidade7->id}}" name="unidadeid" >
  </div>

 @endforeach


  <button type="submit" class="btn btn-primary"  onclick="participar()">Inscreva-se</button></a>
  {!!Form:: close()!!} 
  </div>
</div>
</div>

      </div>
    </div>
  
  </div>
</div>
      </div> 
    </div>
  </div>

    <!-- INSCRIÇÂO EVENTO 8 -->
<div class="modal fade" id="exampleModal8" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:100%">
      
    <div class="modal-header" style="background-color:#1e426e">

    @foreach ($evento8 as $event8)
    <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white"> {{$event8->titulo}}</b> </h5>
    @endforeach
          
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
     
	  <div class="card mb-3" >

    <div class="col-md-12">
	<div class="carousel-inner">
    <div class="carousel-item active">
    @foreach ($evento8 as $event8)
    <img src="uploads/{{$event8->logomarca}}" style="width:100%" class="tam-img1 img-responsive"> 
   @endforeach  
      </div> 
  </div>    
</div>
    <div class="col-md-12">
      <div class="card-body" >
	  <div class=" text-center">
	  <div class="card col-md-12" >
  <div class="card-body">
  <center> <h5 class="card-title txt25">Cadastre-se</h5></center>
   {!! Form:: open(['route'=>'inscricao.save', 'method' => 'POST']) !!}
  <div class="form-group">
    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="cpf" name="cpf" placeholder="Cpf" required>
  </div>
  <div class="form-group">
    <input type="email" class="form-control" id="email " name="email" placeholder="Email" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" required>
  </div>

  @foreach ($evento8 as $event8)
  <div class="form-group">
    <input type="hidden" class="form-control" id="eventoid" value="{{$event8->id}}" name="eventoid" >
  </div>
 @endforeach

 @foreach ($formunidade8 as $formunidade8)
  <div class="form-group">
    <input type="hidden" class="form-control" id="unidadeid" value="{{$formunidade8->id}}" name="unidadeid" >
  </div>

 @endforeach
  <button type="submit" class="btn btn-primary"  onclick="participar()">Inscreva-se</button></a>
  {!!Form:: close()!!} 
  </div>
</div>
</div>

      </div>
    </div>
  
  </div>
</div>
      </div> 
    </div>
  </div>


 <!-- Cursos Bacharelado-->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:100%">
      
	  <div class="modal-header" style="background-color:#1e426e">
	  @foreach ($curso1bacharelado as $curso) 
	  <h5 class="modal-title txt25" id="exampleModalLabel"><div class="logo_text logo"><img src="images/home/redeunirb.png"  style="width:100%"></div><b style="color:white">  {{$curso->nome}}</b> </h5>
	
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

     <center> <div class="modal-body">
	  <div class=" col-md-12" >
   <div class="card" style="width: 100%">
  <div class="card-body ">
    <h5 class="card-title txt15">Portaria do MEC : {{$curso->portaria}} </h5>
    <h5 class="card-title txt15">Turno : {{$curso->turno}} </h5>
    <h5 class="card-title txt15">Duração : {{$curso->duracao}} </h5>
    <h5 class="card-title txt15">Mensalidade: {{$curso->mensalidade}} </h5>
    <a href="pdf/matriz/bacharelado/administracao.pdf" target="blank"><h5 class="card-title txt15">Matriz Curricular </h5></a>
	<div class="card" style="width: 100%">
  <div class="card-body">
    <h5 class="card-title txt18">Sobre o Curso</h5>
    <p class="card-text text-justify" style="color:black">{{$curso->descricao}}</p>
  </div>
</div>
  </div>
</div>
@endforeach  
  </div>
</div></center>

   
      </div>
      
    </div>
  </div>



  <style>
.txthover:hover{
	color:blue;
}
</style>


<div class="super_container">

 <!-- Modal pop up -->
 <div class="modal fade " id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:3%">
		<div class="modal-dialog " role="document" style="background-color:#1f466f">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:#1e426e">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:red"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
					<div class="container-fluid">
    <div class="row">
        <div class="col-12 mt-3">
            <div class="">
                <div class="card-horizontal" >
                    <div class="img-square-wrapper">
                        <img class="img-popup" src="images/home/redeunirb.png" alt="Card image cap">
                    </div>
                    <div class="card-body pose-popup" >
                        <h4 class="card-title" style="font-size:25px">Seja Bem Vindo !</h4>
						<div class="form-group">
						<div class="dropdown">                               
	
    <div class="btn-group">
       <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		<i class="fa fa-map-marker" style="color:black"></i>&nbsp;
		 <b style="color:black">Escolha a sua unidade</b> <span class="caret"></span></button>
         <ul class="dropdown-menu scrollable-menu" role="menu">
		 
		  <!--Unidade Alagoinhas-->
		  @foreach ($unidade2 as $uni) 
         <li> <a class="dropdown-item" href="Alagoinhas">{{$uni->nome}}</a></li>
	     @endforeach

		 <!--Unidade Aracaju-->
		 @foreach ($unidade3 as $uni) 
         <li> <a class="dropdown-item" href="Aracaju">{{$uni->nome}}</a></li>
	     @endforeach

		 <!--Unidade FBT- Aracaju-->
		 @foreach ($unidade4 as $uni) 
         <li> <a class="dropdown-item" href="FBTAracaju">{{$uni->nome}}</a></li>
	     @endforeach

		 <!--Unidade Arapiraca-->
		 @foreach ($unidade5 as $uni) 
         <li> <a class="dropdown-item" href="Arapiraca">{{$uni->nome}}</a></li>
	     @endforeach

		 <!--Unidade Barreiras-->
		 @foreach ($unidade6 as $uni) 
         <li> <a class="dropdown-item" href="Barreiras">{{$uni->nome}}</a></li>
	     @endforeach


		 <!--Unidade Camaçari-->
		 @foreach ($unidade7 as $uni) 
         <li> <a class="dropdown-item" href="Camacari">{{$uni->nome}}</a></li>
	     @endforeach

		  <!--Unidade Fortaleza-->
		  @foreach ($unidade8 as $uni) 
         <li> <a class="dropdown-item" href="Fortaleza">{{$uni->nome}}</a></li>
	     @endforeach

		  <!--Unidade Feira de Santana-->
		  @foreach ($unidade9 as $uni) 
         <li> <a class="dropdown-item" href="Feiradesantana">{{$uni->nome}}</a></li>
	     @endforeach

		  <!--Unidade FBT- Feira de Santana-->
		  @foreach ($unidade10 as $uni) 
         <li> <a class="dropdown-item" href="FBTFeiradesantana">{{$uni->nome}}</a></li>
	     @endforeach

		  <!--Unidade Juazeiro-->
		  @foreach ($unidade11 as $uni) 
         <li> <a class="dropdown-item" href="Juazeiro">{{$uni->nome}}</a></li>
	     @endforeach

		   <!--Unidade Maceió-->
		   @foreach ($unidade12 as $uni) 
         <li> <a class="dropdown-item" href="Maceio">{{$uni->nome}}</a></li>
	     @endforeach

		   <!--Unidade Mossoró-->
		   @foreach ($unidade13 as $uni) 
         <li> <a class="dropdown-item" href="Mossoro">{{$uni->nome}}</a></li>
	     @endforeach

		  <!--Unidade Natal-->
		  @foreach ($unidade14 as $uni) 
         <li> <a class="dropdown-item" href="Natal">{{$uni->nome}}</a></li>
	     @endforeach

		 <!--Unidade Parnaíba-->
		 @foreach ($unidade15 as $uni) 
         <li> <a class="dropdown-item" href="Parnaiba">{{$uni->nome}}</a></li>
	     @endforeach

		 <!--Unidade Salvador-->
		 @foreach ($unidade16 as $uni) 
         <li> <a class="dropdown-item" href="Salvador">{{$uni->nome}}</a></li>
	     @endforeach

         <!--Unidade Teresina-->
         @foreach ($unidade17 as $uni) 
         <li> <a class="dropdown-item" href="Teresina">{{$uni->nome}}</a></li>
	     @endforeach
		
	    
                </ul>
            </div>

    </div>
  </div>
  </div>

                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>           </div>
                    
                </div>
            </div>

	<!-- Header -->

	<header class="header">
			
		<!-- Top Bar -->
		<div class="top_bar" style="background-color:black">
			<div class="top_bar_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
								<ul class="top_bar_contact_list">

  <li>
	<i class="fa fa-phone" aria-hidden="true"></i>
  <div><span class="font-txt-header ouvidoria"><a href="" data-toggle="modal" data-target="#myModal1"><b >Escolha outra Unidade</b></a><span></div>
     </li>

	<li>
	<i class="fa fa-phone" aria-hidden="true"></i>
	<div><span class="font-txt-header ouvidoria"><a href="ouvidoria"><b >OUVIDORIA</b></a><span></div>
     </li>
      &nbsp;&nbsp;
     
	</ul>
	<div class="top_bar_login ml-auto">
									<div class="login_button"><a href="https://unirb1.portaldominus.com.br/login.php"  target="_blank" ><i class="fa fa-graduation-cap" aria-hidden="true"></i> PORTAL ACADÊMICO </a></div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>

		<!-- Header Content -->
		<div class="header_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo_container">
								<a href="principal">
									<div class="logo_text logo">
                  @foreach ($logomarca as $logo)
                  <img src="uploads/{{$logo->logomarca}}"> 
                   @endforeach  
                  </div>
								</a>
							</div>
							<nav class="main_nav_contaner ml-auto">
								<ul class="main_nav">
								<li>
                                 <div class="navbar-nav">
                                 <div class="dropdown">
                                 <a class="nav-item  dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">A UNIRB</a> 
                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                 <a class="dropdown-item" href="institucional" style="color:black">Institucional</a>
                                <a class="dropdown-item" href="https://docs.google.com/forms/d/1nQpnH2eQ7SN92IiuC_Kn_IygQhG28MU5b2gQzh5Nt-g/viewform?edit_requested=true#responses"  target="_blank" style="color:black;font-size:15px">Egressos</a>
                                <a class="dropdown-item" href="http://portal.unirb.edu.br/cpa_novo/cpa_administrativo_ssa.php"  target="_blank" style="color:black;font-size:15px">CPA</a>
								<a class="dropdown-item" href="/linhadotempo" style="color:black;font-size:15px">Linha do Tempo</a>
								<a class="dropdown-item" href="/laboratorios" style="color:black;font-size:15px">Laboratórios</a>
								<a class="dropdown-item" href="/biblioteca" style="color:black;font-size:15px">Biblioteca</a>

                               </div>
								  </div>
								</li>

								<li>
                                 <div class="navbar-nav">
                                 <div class="dropdown">
                                 <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" >Cursos</a> 
                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								 <a class="dropdown-item" href="/graduacao" style="color:black;font-size:15px">Graduação</a>
								 <a class="dropdown-item" href="/posgraduacao" style="color:black;font-size:15px">Pós-Graduação</a>
								 <a class="dropdown-item" href="/ouvidoria" style="color:black;font-size:15px">EAD</a>

                               </div>
								  </div>
								</li>

									<li>
                                 <div class="navbar-nav">
                                 <div class="dropdown">
                                 <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" >Estude na UNIRB</a> 
                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="/creditoestudantil" style="color:black;font-size:15px">Crédito Estudantil</a>
								<a class="dropdown-item" href="/convenios" style="color:black;font-size:15px">Convênios</a>
								<a class="dropdown-item" href="/estagio" style="color:black;font-size:15px">Programas de Estágio</a>
								<a class="dropdown-item" href="/intercambio" style="color:black;font-size:15px">Intercâmbio</a>

                               </div>
								  </div>
								</li>

								<li>
                                 <div class="navbar-nav">
								 <div class=""><button class="btn btn-success"><b><a href="vestibular" style="color:white;font-size:13px" >INSCREVA-SE</a></b></button></div>

                                 
								  </div>
								</li>

									
								</ul>



									<!-- Hamburger -->

									<div class="hamburger menu_mm">
									<i class="fa fa-bars menu_mm" aria-hidden="true" style="color:black"></i>
								</div>	
							</nav>
								
							</nav>

						</div>
					</div>
				</div>
			</div>
		</div>

			
	</header>

	<!-- Menu -->

	<div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<nav class="menu_nav">
			<ul class="menu_mm">
			<div class="logo_container">
			<a href="/">
		    <div class="logo_text logo"><img src="images/home/redeunirb.png" style="width:130%" ></div>
			</a>
		    </div>
				
			</ul>

			<ul class="menu_mm">
			<div class="dropdown">
            <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="color:black;font-size:18px">A UNIRB</a> 
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" href="institucional" style="color:black">Institucional</a>
                                <a class="dropdown-item" href="https://docs.google.com/forms/d/1nQpnH2eQ7SN92IiuC_Kn_IygQhG28MU5b2gQzh5Nt-g/viewform?edit_requested=true#responses"  target="_blank" style="color:black;font-size:15px">Egressos</a>
                                <a class="dropdown-item" href="http://portal.unirb.edu.br/cpa_novo/cpa_administrativo_ssa.php"  target="_blank" style="color:black;font-size:15px">CPA</a>
								<a class="dropdown-item" href="/linhadotempo" style="color:black;font-size:15px">Linha do Tempo</a>
								<a class="dropdown-item" href="/laboratorios" style="color:black;font-size:15px">Laboratórios</a>
								<a class="dropdown-item" href="/biblioteca" style="color:black;font-size:15px">Biblioteca</a>

         </div>
				
			</ul>

			<ul class="menu_mm">
			<div class="dropdown">
            <a class="nav-item nav-link active bold dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="color:black;font-size:18px">Cursos</a> 
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" href="/graduacao" style="color:black;font-size:15px">Graduação</a>
								 <a class="dropdown-item" href="/posgraduacao" style="color:black;font-size:15px">Pós-Graduação</a>
								 <a class="dropdown-item" href="http://betaead.unirb.edu.br/" style="color:black;font-size:15px">EAD</a>

         </div>
				
			</ul>

			<ul class="menu_mm">
			<div class="dropdown">
            <a class="nav-item nav-link active  dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="color:black;font-size:18px">Estude na UNIRB</a> 
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="/creditoestudantil" style="color:black;font-size:15px">Crédito Estudantil</a>
								<a class="dropdown-item" href="/convenios" style="color:black;font-size:15px">Convênios</a>
								<a class="dropdown-item" href="/estagio" style="color:black;font-size:15px">Programas de Estágio</a>
								<a class="dropdown-item" href="/intercambio" style="color:black;font-size:15px">Intercâmbio</a>

		 </div>
		 <br>
				
			</ul>

			<ul class="menu_mm">
			<div class=""><a href="https://unirb1.portaldominus.com.br/login.php" style="color:black;font-size:18px" target="_blank"><i class="fa fa-graduation-cap" aria-hidden="true"></i> PORTAL DO ALUNO</a></div>

				
			</ul>

				</ul><br>

			<ul class="menu_mm">
			<div class="dropdown">
			<div class=""><button class="btn btn-success"><b><a href="vestibular" style="color:white">INSCREVA-SE</a></b></button></div>
           

				
			</ul>
		</nav>
	</div>
	

	<!-- Slider --><br>
	

	<div id="carouselExampleControls" class="carousel slide pose-slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
	@foreach ($banner1 as $banner1) 
	<div class="blog_post_image"><img src="uploads/{{$banner1->img}}" alt="" style="width:100%"class=" img-responsive"></div>
	@endforeach   
	 </div>
    <div class="carousel-item">
	@foreach ($banner2 as $banner2) 
	<div class="blog_post_image"><img src="uploads/{{$banner2->img}}" alt="" style="width:100%"class=" img-responsive"></div>
	@endforeach  
    </div>
   <div class="carousel-item">
   @foreach ($banner3 as $banner3) 
	<div class="blog_post_image"><img src="uploads/{{$banner3->img}}" alt="" style="width:100%"class=" img-responsive"></div>
	@endforeach     </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>



	<!-- Features -->

	<div class="pose-title">
	@foreach ($nome as $name) 
	<center><h2 class="section_title  txt50"><b style="color:black">Seja Bem-Vindo a</b> <b style="color:#9e111f">UNIRB</b> </span> <b style="color:black">{{$name->nome}}</b></h2><center>
	@endforeach
	</div>
	<br>

			<center>
				<div class="row features_row col-md-10 "  style="margin-top:-1%" >
				
				<!-- Features Item -->
				<div class="col-lg-2 feature_col" >
					<div class="feature text-center trans_400">
						<div class="feature_icon "><img src="images/unidade/calendarioacad.png" alt="calendario"></div>
						<h3 class="feature_title" style="margin-top:70%"><b>Calendário Acadêmico</b></h3>
					</div>
				</div>

				<!-- Features Item -->
				<div class="col-lg-2 feature_col">
				<a href="https://unirb1.portaldominus.com.br/login.php" target="_blank"><div class="feature text-center trans_400">
						<div class="feature_icon"><img src="images/unidade/portaldoaluno.png" alt=""></div><br>
						<h3 class="feature_title" style="margin-top:55%"><b>Portal do<br> Aluno</b></h3>
					</div></a>
				</div>

				<!-- Features Item -->
				<div class="col-lg-2 feature_col">
				<a href="http://unirb.edu.br/cpa.html" target="_blank">
					<div class="feature text-center trans_400">
						<div class="feature_icon"><img src="images/unidade/cpa.png" alt=""></div><br>
						<h3 class="feature_title" style="margin-top:55%"><b>CPA</b></h3>
					</div></a>
				</div>

				<!-- Features Item -->
				<div class="col-lg-2 feature_col">
				<a href="laboratorios" target="_blank"><div class="feature text-center trans_400">
						<div class="feature_icon"><img src="images/unidade/sistemadensino.png" alt=""></div><br>
						<h3 class="feature_title" style="margin-top:55%"><b>Sistemas de Ensino</b></h3>
					</div></a>
				</div>

				<!-- Features Item -->
				<div class="col-lg-2 feature_col">
				<a href="biblioteca" target="_blank">	<div class="feature text-center trans_400">
						<div class="feature_icon"><img src="images/unidade/materialestudo.png" alt=""></div><br>
						<h3 class="feature_title" style="margin-top:55%"><b>Material de Estudo</b></h3>
					</div></a>
				</div>

				<!-- Features Item -->
				<div class="col-lg-2 feature_col">
				<a href="https://docs.google.com/forms/d/1nQpnH2eQ7SN92IiuC_Kn_IygQhG28MU5b2gQzh5Nt-g/viewform?edit_requested=true#responses" target="_blank"><div class="feature text-center trans_400">
						<div class="feature_icon "><img src="images/unidade/egresso.png" alt=""></div>
						<h3 class="feature_title" style="margin-top:70%"><b>Egressos</b></h3>
					</div></a>
				</div>

			</div>
		</div>
	</div>
	</div>
</center>


	<!-- CURSOS-->

	<div class="pose-title">
	<div class="section_title_container text-center col-md-12">
	<h2 class="section_title  txt50"><span style="color:#1e426e">Nossos Cursos</span></h2>
	</div>
	</div>
	<br>

<div class="card-group">
  <div class="card">
  <div class="card-header" style="background-color:#1e426e">
    <span class="txt25" style="color:white"><b>Bacharelado</b></span>
  </div>
  

    <div class="card-body">
	  <h5 class="card-title txt18">{{$bachareladocount}} Cursos</h5>
	

	  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
    
	Verificar
  </a>
  
  
</p>
<div class="collapse" id="collapseExample" >
  <div class="card card-body" style="margin-top:4%;">
    <ul>
	@foreach ($bacharelado as $bacharel) 

  <table class="table">
  <tbody>
    <tr>
      <th>{{$bacharel->nome}}</th> 
    </tr> 
  </tbody>
</table>
		@endforeach
		<ul>
	
  </div>
</div>

    </div>
  </div>

  <div class="card">
  <div class="card-header" style="background-color:#1e426e">
    <span class="txt25" style="color:white"><b>Tecnólogo</b></span>
  </div>
  <div class="card-body">
    
  <h5 class="card-title txt18">{{$tecnologocount}} Cursos</h5>
	 
	  
	  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample1" role="button" aria-expanded="false" aria-controls="collapseExample">
    Verificar
  </a>
  
  
</p>
<div class="collapse" id="collapseExample1" >
  <div class="card card-body" style="margin-top:4%;">
    <ul>
	@foreach ($tecnologo as $tec) 

  <table class="table">
  <tbody>
    <tr>
      <th>{{$tec->nome}}</th> 
    </tr> 
  </tbody>
</table>
		@endforeach
		<ul>
  </div>
</div>

    </div>
  </div>

  <div class="card">
  <div class="card-header" style="background-color:#1e426e">
    <span class="txt25" style="color:white"><b>Licenciatura</b></span>
  </div>
  <div class="card-body">
  <h5 class="card-title txt18">{{$licenciaturacount}} Cursos</h5>
	  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample">
    Verificar
  </a>
  
  
</p>
<div class="collapse" id="collapseExample2" >
  <div class="card card-body" style="margin-top:4%;">
    <ul>
	@foreach ($licenciatura as $linc) 

  <table class="table">
  <tbody>
    <tr>
      <th>{{$linc->nome}}</th> 
    </tr> 
  </tbody>
</table>
		@endforeach
		<ul>
  </div>
</div>

    </div>
  </div>

  <div class="card">
  <div class="card-header" style="background-color:#1e426e">
    <span class="txt25" style="color:white"><b>EAD</b></span>
  </div>
  <div class="card-body">
  <h5 class="card-title txt18">{{$eadcount}} Cursos</h5>
	  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample3" role="button" aria-expanded="false" aria-controls="collapseExample">
    Verificar
  </a>
  
  
</p>
<div class="collapse" id="collapseExample3" >
  <div class="card card-body" style="margin-top:4%;">
    <ul>
	@foreach ($ead as $online) 

  <table class="table">
  <tbody>
    <tr>
      <th>{{$online->nome}}</th> 
    </tr> 
  </tbody>
</table>

		@endforeach
		<ul>
  </div>
</div>



    </div>
  </div>

  <div class="card">
  <div class="card-header" style="background-color:#1e426e">
    <span class="txt25" style="color:white"><b>Pós-Graduação</b></span>
  </div>
  <div class="card-body">
  <h5 class="card-title txt18">{{$poscount}} Cursos</h5>
	  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample6" role="button" aria-expanded="false" aria-controls="collapseExample">
    Verificar
  </a>
  
  
</p>
<div class="collapse" id="collapseExample6" >
  <div class="card card-body" style="margin-top:4%;">
    <ul>
	@foreach ($pos as $pos) 

  <table class="table">
  <tbody>
    <tr>
      <th>{{$pos->nome}}</th> 
    </tr> 
  </tbody>
</table>
		@endforeach
		<ul>
  </div>
</div>



    </div>
  </div>

  <div class="card">
  <div class="card-header" style="background-color:#1e426e">
    <span class="txt25" style="color:white"><b>Sequencial</b></span>
  </div>
  <div class="card-body">
  <h5 class="card-title txt18">{{$seqcount}} &nbsp; Cursos</h5>
	  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample4" role="button" aria-expanded="false" aria-controls="collapseExample">
    Verificar
  </a>
  
  
</p>
<div class="collapse" id="collapseExample4" >
  <div class="card card-body" style="margin-top:4%;">
    <ul>
	@foreach ($sequencial as $seq) 

  <table class="table">
  <tbody>
    <tr>
      <th>{{$seq->nome}}</th> 
    </tr> 
  </tbody>
</table>
  		@endforeach
		<ul>
  </div>
</div>
    </div>
  </div>
  </div>
<br>


<!--EVENTOS Cards-->
	<div class="section_title_container text-center col-md-12 pose-title">
	<h2 class="section_title color-blue txt50 pose-title"><span style="color:#1e426e">Eventos</span></h2>
	</div>
  
<div id="carouselExampleControls2" class="carousel slide " data-ride="carousel" style="margin-top:2%">
  <div class="carousel-inner ">
    <div class="carousel-item active ">

	<!--EVENTO1-->
  <div class="card-deck" >
  <div class=" col-md-3">

  @foreach ($evento1 as $event1)
  <img src="uploads/{{$event1->logomarca}}" style="width:100%"> 
  @endforeach   

     <div class=" card card-body" style="margin-top:3%">  
     @foreach ($evento1 as $event1)
	  <h5 class="card-title txt25" style="color:#1f466f">{{$event1->titulo}}</h5>
    @endforeach

    @foreach ($evento1 as $event1)
      <p class=" txt15" style="color:black"><b>Data</b>: {{$event1->data_evento}}</p>
      @endforeach

      @foreach ($evento1 as $event1)
      <p class=" txt15" style="color:black"><b>Horário</b>: {{$event1->horario}}</p>
      @endforeach

      @foreach ($evento1 as $event1)
      <p class=" txt15" style="color:black"><b>Local</b>: {{$event1->local}}</p>
      @endforeach

      @foreach ($evento1 as $event1)
      <p style="color:black"><b>Sobre o Evento</b>:</p>
     	<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="color:black;background-color:white " readonly>{{$event1->descricao}}</textarea>
       @endforeach

       @foreach ($evento1 as $event1)
       <input type="hidden"  id="inputevent1" value="{{$event1->status}}"> </input>
       @endforeach
	</div>
  
    <div class="card-footer divbutton1"  style="display:none">
<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1" style="margin-left:70%" >Participar</button>    </div>
  </div>

  <!--EVENTO2-->
  <div class=" col-md-3 ">
  @foreach ($evento2 as $event2)
    <img src="uploads/{{$event2->logomarca}}" style="width:100%"> 
    @endforeach  

   <div class=" card card-body" style="margin-top:3%">
   @foreach ($evento2 as $event2)
	  <h5 class="card-title txt25" style="color:#1f466f">{{$event2->titulo}}</h5>
    @endforeach  

    @foreach ($evento2 as $event2)
      <p class=" txt15" style="color:black"><b>Data</b>:{{$event2->data_evento}}</p>
      @endforeach  

      @foreach ($evento2 as $event2)
      <p class=" txt15" style="color:black"><b>Horário</b>: {{$event2->horario}}</p>
      @endforeach  


      @foreach ($evento2 as $event2)
      <p class=" txt15" style="color:black"><b>Local</b>: {{$event2->local}}</p>
      @endforeach  
     <br>
     @foreach ($evento2 as $event2)
     <p style="color:black"><b>Sobre o Evento</b>:</p>
	<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="color:black;background-color:white " readonly>{{$event2->descricao}}</textarea>
  @endforeach  

  @foreach ($evento2 as $event2)
     <input type="hidden" style="color:black" id="inputevent2" value="{{$event2->status}}"> </input>
     @endforeach  

	</div>

    <div class="card-footer divbutton2"  style="display:none">
<button class="btn btn-primary"  data-toggle="modal" data-target="#exampleModal2" style="margin-left:70%">Participar</button>  
  </div>
  </div>

  <!--EVENTO3-->

  <div class=" col-md-3" >
  @foreach ($evento3 as $event3)
    <img src="uploads/{{$event3->logomarca}}" style="width:100%"> 
   @endforeach 
   
     <div class=" card card-body" style="margin-top:3%">
    @foreach ($evento3 as $event3) 
	  <h5 class="card-title txt25" style="color:#1f466f">{{$event3->titulo}}</h5>
	  @endforeach

    @foreach ($evento3 as $event3) 

      <p class=" txt15" style="color:black"><b>Data</b>:{{$event3->data_evento}}</p>
      @endforeach

      @foreach ($evento3 as $event3) 
      <p class=" txt15" style="color:black"><b>Horário</b>: {{$event3->horario}}</p>
     @endforeach

     @foreach ($evento3 as $event3) 
      <p class=" txt15" style="color:black"><b>Local</b>: {{$event3->local}}</p>
     @endforeach  
     <br>
     
     @foreach ($evento3 as $event3) 
     <p style="color:black"><b>Sobre o Evento</b>:</p>
	<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="color:black;background-color:white " readonly>{{$event3->descricao}}</textarea>
  @endforeach   
  
  @foreach ($evento3 as $event3)
      <input type="hidden" style="color:black" id="inputevent3" value="{{$event3->status}}"> </input>
     @endforeach

	</div>
    <div class="card-footer divbutton3"  style="display:none">
<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal3" style="margin-left:70%">Participar</button>    </div>
  </div>

<!--EVENTO4-->
  <div class="col-md-3 ">
  @foreach ($evento4 as $event4)
    <img src="uploads/{{$event4->logomarca}}" style="width:100%"> 
   @endforeach  
   
    <div class=" card card-body" style="margin-top:3%">
    @foreach ($evento4 as $event4) 
	  <h5 class="card-title txt25" style="color:#1f466f">{{$event4->titulo}}</h5>
	  @endforeach

    @foreach ($evento4 as $event4) 

      <p class=" txt15" style="color:black"><b>Data</b>:{{$event4->data_evento}}</p>
      @endforeach

      @foreach ($evento4 as $event4) 
      <p class=" txt15" style="color:black"><b>Horário</b>: {{$event4->horario}}</p>
     @endforeach

     @foreach ($evento4 as $event4) 
      <p class=" txt15" style="color:black"><b>Local</b>: {{$event4->local}}</p>
     @endforeach  
     <br>
     
     @foreach ($evento4 as $event4) 
     <p style="color:black"><b>Sobre o Evento</b>:</p>

	<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="color:black;background-color:white " readonly>{{$event4->descricao}}</textarea>
  @endforeach 

  @foreach ($evento4 as $event4)
      <input type="hidden" style="color:black" id="inputevent4" value="{{$event4->status}}"> </input>
     @endforeach
  </div>
    <div class="card-footer divbutton4"  style="display:none">
<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal4" >Participar</button>    </div>
  </div>

</div>
  </div>

  
    <div class="carousel-item">
	<div class="card-deck">
  <!--EVENTO5-->

  <div class="col-md-3">
  @foreach ($evento5 as $event5)
    <img src="uploads/{{$event5->logomarca}}" style="width:100%"> 
   @endforeach   <div class=" card card-body" style="margin-top:3%">
    @foreach ($evento5 as $event5) 
	  <h5 class="card-title txt25" style="color:#1f466f">{{$event5->titulo}}</h5>
	  @endforeach

    @foreach ($evento5 as $event5) 

      <p class=" txt15" style="color:black"><b>Data</b>:{{$event5->data_evento}}</p>
      @endforeach

      @foreach ($evento5 as $event5) 
      <p class=" txt15" style="color:black"><b>Horário</b>: {{$event5->horario}}</p>
     @endforeach

     @foreach ($evento5 as $event5) 
      <p class=" txt15" style="color:black"><b>Local</b>: {{$event5->local}}</p>
     @endforeach  
     <br>
     
     @foreach ($evento5 as $event5) 
     <p style="color:black"><b>Sobre o Evento</b>:</p>

	<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="color:black;background-color:white " readonly>{{$event5->descricao}}</textarea>
	@endforeach   

  @foreach ($evento5 as $event5)
      <input type="hidden" style="color:black" id="inputevent5" value="{{$event5->status}}"> </input>
     @endforeach

	</div>
    <div class="card-footer divbutton5"  style="display:none">
<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal5" style="margin-left:70%">Participar</button>    </div>
  </div>
<!--EVENTO6-->

  <div class="col-md-3">
  @foreach ($evento6 as $event6)
    <img src="uploads/{{$event6->logomarca}}" style="width:100%"> 
   @endforeach 
   
     <div class=" card card-body" style="margin-top:3%">
    @foreach ($evento6 as $event6) 
	  <h5 class="card-title txt25" style="color:#1f466f">{{$event6->titulo}}</h5>
	  @endforeach

    @foreach ($evento6 as $event6) 

      <p class=" txt15" style="color:black"><b>Data</b>:{{$event6->data_evento}}</p>
      @endforeach

      @foreach ($evento6 as $event6) 
      <p class=" txt15" style="color:black"><b>Horário</b>: {{$event6->horario}}</p>
     @endforeach

     @foreach ($evento6 as $event6) 
      <p class=" txt15" style="color:black"><b>Local</b>: {{$event6->local}}</p>
     @endforeach  
     <br>
     
     @foreach ($evento6 as $event6) 
     <p style="color:black"><b>Sobre o Evento</b>:</p>

	<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="color:black;background-color:white " readonly>{{$event6->descricao}}</textarea>
	@endforeach  

   @foreach ($evento6 as $event6)
      <input type="hidden" style="color:black" id="inputevent6" value="{{$event6->status}}"> </input>
     @endforeach 

	</div>
    <div class="card-footer divbutton6"  style="display:none">
<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal6">Participar</button>    </div>
  </div>

<!--EVENTO7-->

  <div class="col-md-3">
  @foreach ($evento7 as $event7)
    <img src="uploads/{{$event7->logomarca}}" style="width:100%"> 
   @endforeach  
   
    <div class=" card card-body" style="margin-top:3%">
    @foreach ($evento7 as $event7) 
	  <h5 class="card-title txt25" style="color:#1f466f">{{$event7->titulo}}</h5>
	  @endforeach

    @foreach ($evento7 as $event7) 

      <p class=" txt15" style="color:black"><b>Data</b>:{{$event7->data_evento}}</p>
      @endforeach

      @foreach ($evento7 as $event7) 
      <p class=" txt15" style="color:black"><b>Horário</b>: {{$event7->horario}}</p>
     @endforeach

     @foreach ($evento7 as $event7) 
      <p class=" txt15" style="color:black"><b>Local</b>: {{$event7->local}}</p>
     @endforeach  
     <br>
     
     @foreach ($evento7 as $event7) 
     <p style="color:black"><b>Sobre o Evento</b>:</p>

	<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="color:black;background-color:white " readonly>{{$event7->descricao}}</textarea>
	@endforeach   

  @foreach ($evento7 as $event7)
      <input type="hidden" style="color:black" id="inputevent7" value="{{$event7->status}}"> </input>
     @endforeach

	</div>
    <div class="card-footer divbutton7"  style="display:none">
<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal7">Participar</button>    </div>
  </div>

  <!--EVENTO8-->


  <div class="col-md-3">
  @foreach ($evento8 as $event8)
    <img src="uploads/{{$event8->logomarca}}" style="width:100%"> 
   @endforeach    
   
    <div class=" card card-body" style="margin-top:3%">
    @foreach ($evento8 as $event8) 
	  <h5 class="card-title txt25" style="color:#1f466f">{{$event8->titulo}}</h5>
	  @endforeach

    @foreach ($evento8 as $event8) 

      <p class=" txt15" style="color:black"><b>Data</b>:{{$event8->data_evento}}</p>
      @endforeach

      @foreach ($evento8 as $event8) 
      <p class=" txt15" style="color:black"><b>Horário</b>: {{$event8->horario}}</p>
     @endforeach

     @foreach ($evento8 as $event8) 
      <p class=" txt15" style="color:black"><b>Local</b>: {{$event8->local}}</p>
     @endforeach  
     <br>
     
     @foreach ($evento8 as $event8) 
     <p style="color:black"><b>Sobre o Evento</b>:</p>

	<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="color:black;background-color:white " readonly>{{$event8->descricao}}</textarea>
	@endforeach   

  @foreach ($evento8 as $event8)
      <input type="hidden" style="color:black" id="inputevent8" value="{{$event8->status}}"> </input>
     @endforeach

	</div>
    <div class="card-footer divbutton8"  style="display:none">
<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal8">Participar</button>    </div>
  </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev" style="margin-left:-6%">
  <i class="fa fa-arrow-circle-left fa-2x"  aria-hidden="true" style="color:black"></i>  

  </a>
  <a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-slide="next" >
  <i class="fa fa-arrow-circle-right fa-2x" aria-hidden="true" style="color:black;margin-left:70%" ></i>   
  </a>
</div>





	<!-- Blog -->
<div class="container col-md-12 pose-galeria " style="background-color:#1e426e;box-shadow: 10px 5px 5px #363636;">
	<div class="row pose-motivos">
	<div class="col">
	<br><br>
	<div class="section_title_container text-center">
	<h2 class="section_title txt50 "  style="color:white" >Galeria de Fotos</h2>
					</div>
				</div>
			</div>

	<div class="blog  " style="background-color:#1e426e;">
		<div class="container">
			<div class="row">
				<div class="col ">
					<div class="blog_post_container pose-blog pose-news" >

						<!-- Blog Post -->
						<div class="blog_post trans_200 col-md-3" >
							<div class="blog_post_image">
              @foreach ($galeria1 as $gale1)
             <img src="uploads/{{$gale1->img}}" style="width:100%"> 
                @endforeach               
                </div>
							<div class="blog_post_body">
								<div class="blog_post_title txt18" style="color:black">
                @foreach ($galeria1 as $gale1)
                <b>{{$gale1->titulo}}</b>
                @endforeach  
                </div>								
								
							</div>
						</div>

						<!-- Blog Post -->
						<div class="blog_post trans_200 col-md-3">
						<div class="blog_post_image">
              @foreach ($galeria2 as $gale2)
             <img src="uploads/{{$gale2->img}}" style="width:100%"> 
                @endforeach               
                </div>
							<div class="blog_post_body">
								<div class="blog_post_title txt18" style="color:black">
                @foreach ($galeria2 as $gale2)
                <b>{{$gale2->titulo}}</b>
                @endforeach  
                </div>								
								
							</div>
						</div>

						<!-- Blog Post -->
						<div class="blog_post trans_200 col-md-3" >
            <div class="blog_post_image">
              @foreach ($galeria3 as $gale3)
             <img src="uploads/{{$gale3->img}}" style="width:100%"> 
                @endforeach               
                </div>
							<div class="blog_post_body">
								<div class="blog_post_title txt18" style="color:black">
                @foreach ($galeria3 as $gale3)
                <b>{{$gale3->titulo}}</b>
                @endforeach  
                </div>								
								
							</div>
						</div>
							<!-- Blog Post -->
						<div class="blog_post trans_200 col-md-3" >
            <div class="blog_post_image">
              @foreach ($galeria4 as $gale4)
             <img src="uploads/{{$gale4->img}}" style="width:100%"> 
                @endforeach               
                </div>
							<div class="blog_post_body">
								<div class="blog_post_title txt18" style="color:black">
                @foreach ($galeria4 as $gale4)
                <b>{{$gale4->titulo}}</b>
                @endforeach  
                </div>								
								
							</div>
						</div>

							<!-- Blog Post -->
						<div class="blog_post trans_200 col-md-3">
            <div class="blog_post_image">
              @foreach ($galeria5 as $gale5)
             <img src="uploads/{{$gale5->img}}" style="width:100%"> 
                @endforeach               
                </div>
							<div class="blog_post_body">
								<div class="blog_post_title txt18" style="color:black">
                @foreach ($galeria5 as $gale5)
                <b>{{$gale5->titulo}}</b>
                @endforeach  
                </div>								
								
							</div>
						</div>


						<!-- Blog Post -->
						<div class="blog_post trans_200 col-md-3">
						<div class="blog_post_image">
              @foreach ($galeria6 as $gale6)
             <img src="uploads/{{$gale6->img}}" style="width:100%"> 
                @endforeach               
                </div>
							<div class="blog_post_body">
								<div class="blog_post_title txt18" style="color:black">
                @foreach ($galeria6 as $gale6)
                <b>{{$gale6->titulo}}</b>
                @endforeach  
                </div>								
								
							</div>
						</div>

					</div>
				</div>
			</div>
			
		</div>
	</div>
	</div>

	<!--NOTICIAS-->
	<div class="section_title_container text-center col-md-12 ">
	<h2 class="section_title  txt50 pose-title " style="margin-top:5%,color:#1e426e">Acontece na <span style="color:#9e111f">UNIRB</span></h2>
  

	</div>

<div class=" card-group pose-eventos  ">

  <div class="card" >
  @foreach ($noticia1 as $not1)
             <img src="uploads/{{$not1->image}}" style="width:100%"> 
  @endforeach     
  
  <div class="card-body">
	
	@foreach ($noticia1 as $not1) 
      <h5 class="card-title txt25" >{{$not1->titulo}}</h5>
	  @endforeach
    @foreach ($noticia1 as $not1) 
      <p class="card-text txt18 " style="color:black"><b>{{$not1->subtitulo}}</b></p>
	@endforeach

	@foreach ($noticia1 as $not1) 
      <p class="card-text txt15 " style="color:black">{{$not1->data}}</p><br>
	@endforeach

	@foreach ($noticia1 as $not1) 
	<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="color:black;background-color:white " readonly>{{$not1->descricao}}</textarea>
	@endforeach
    </div>

  </div>
  
  <div class="card">
  @foreach ($noticia2 as $not2)
   <img src="uploads/{{$not2->image}}" style="width:100%"> 
  @endforeach     
  
   <div class="card-body" >
	
    @foreach ($noticia2 as $not2) 
      <h5 class="card-title txt25">{{$not2->titulo}}</h5>
	  @endforeach

    @foreach ($noticia2 as $not2) 
      <p class="card-text txt18 " style="color:black"><b>{{$not2->subtitulo}}</b></p>
	@endforeach

  @foreach ($noticia2 as $not2) 
      <p class="card-text txt15 " style="color:black">{{$not2->data}}</p><br>
	@endforeach
  @foreach ($noticia2 as $not2) 
	<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="color:black;background-color:white " readonly>{{$not2->descricao}}</textarea>
	@endforeach  
	 </div>
	 
  </div>


  <div class="card" >
  @foreach ($noticia3 as $not3)
  <img src="uploads/{{$not3->image}}" style="width:100%"> 
  @endforeach     
  
   <div class="card-body">
    @foreach ($noticia3 as $not3) 
      <h5 class="card-title txt25">{{$not3->titulo}}</h5>
	  @endforeach

    @foreach ($noticia3 as $not3) 
      <p class="card-text txt18 " style="color:black"><b>{{$not3->subtitulo}}</b></p>
	@endforeach

  @foreach ($noticia3 as $not3) 
      <p class="card-text txt15 " style="color:black">{{$not3->data}}</p><br>
	@endforeach

	 
  @foreach ($noticia3 as $not3) 
	<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="color:black;background-color:white " readonly>{{$not3->descricao}}</textarea>
	@endforeach  
	 </div>
  </div>

  <div class="card" >
  @foreach ($noticia4 as $not4)
  <img src="uploads/{{$not4->image}}" style="width:100%"> 
  @endforeach      
  
  <div class="card-body">
    @foreach ($noticia4 as $not4) 
      <h5 class="card-title txt25">{{$not4->titulo}}</h5>
	  @endforeach

    @foreach ($noticia4 as $not4) 
      <p class="card-text txt18 " style="color:black"><b>{{$not4->subtitulo}}</b></p>
	@endforeach

  @foreach ($noticia4 as $not4) 
      <p class="card-text txt15 " style="color:black">{{$not4->data}}</p><br>
	@endforeach

	 
  @foreach ($noticia4 as $not4) 
	<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" style="color:black;background-color:white " readonly>{{$not4->descricao}}</textarea>
	@endforeach  
    </div>
  </div>
</div>


<!-- Counter -->

<div class="counter pose-eventos"  >
		<div class="counter_background" style="background-image:url(images/home/counter_background.jpg)"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="counter_content">
						<h2 class="counter_title txt30"> A sua opnião é muito importante!</h2>

						<!-- Milestones -->

						<div class="milestones d-flex flex-md-row flex-column align-items-center justify-content-between">
							
							<!-- Milestone -->
							<div class="milestone">
								<div class="milestone_counter" data-end-value="14" style="color:white">0</div>
								<div class="milestone_text">Cidades</div>
							</div>

							<!-- Milestone -->
							<div class="milestone">
								<div class="milestone_counter" data-end-value="16" style="color:white" >0</div>
								<div class="milestone_text">Unidades</div>
							</div>

							<!-- Milestone -->
							<div class="milestone">
								<div class="milestone_counter" data-end-value="18" style="color:white">0</div>
								<div class="milestone_text">Anos</div>
							</div>

							<!-- Milestone -->
							<div class="milestone">
								<div class="milestone_counter" style="color:white"data-end-value="200" data-sign-after="+">0</div>
								<div class="milestone_text">Cursos</div>
							</div>

						</div>
					</div>

				</div>
			</div>
      

			<div class="counter_form">
				<div class="row fill_height">
					<div class="col fill_height">
          {!! Form:: open(['route'=>'mail.store3', 'method' => 'POST']) !!}   
           <br> <br>   

							<center><div class="counter_form_title" style="font-size:20px">Fale com a nossa equipe</div></center>
							<input type="text" class="counter_input" name="nome3" id="nome3" placeholder="Nome" required="required">
							<input type="tel" class="counter_input"  name="telefone3" id="telefone3" placeholder="Telefone" required="required">
							
							<textarea class="counter_input counter_text_input"  name="mensagem3" id="mensagem3"  placeholder="Mensagem" required="required"></textarea>
							<button type="submit" onclick="fale()" class="counter_form_button">Enviar</button>
              {!!Form:: close()!!}                  
					</div>
				</div>
			</div>

		</div>
	</div>
	<br>



	<!-- MAPS -->
	
	<div id="googleMap" style="width:100%;height:320px" style="overflow:block" class=""></div>

	
	


  {!!Form:: close()!!}                  
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	@include('partials._footer')
</div>

<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=
AIzaSyAF073R745ZFDg7ZNIu9lZUU1lCyGncBig
&callback=myMap"></script>



<script>

$(document).ready(function(){

  $('#telefone').mask('(99) 9 9999-9999');
  $('#cpf').mask(' 999.999.999-99');
  
});
</script>





</body>
</html>