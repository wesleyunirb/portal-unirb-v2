@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row" style="margin-top:2%">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box" style="background-color:#1e426e">
            <div class="inner">
              <h3 style="color:white">{{$unidadescount}}</h3>

              <p style="color:white">Unidades</p>
            </div>
            <div class="icon">
              <i class="ion-ios-home" style="color:white"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box" style="background-color:#9a999e">
            <div class="inner">
              <h3 style="color:white">{{$cursoscount}}</h3>

              <p style="color:white">Cursos</p>
            </div>
            <div class="icon">
            <i class="ion ion-ios-browsers" style="color:white"></i>
             </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box" style="background-color:#9f1322">
            <div class="inner">
              <h3 style="color:white">{{$noticiascount}}</h3>

              <p style="color:white">Notícias</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-book" style="color:white"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box" style="background-color:#25a0bb">
            <div class="inner">
              <h3 style="color:white">{{$eventoscount}}</h3>

              <p style="color:white">Eventos</p>
            </div>
            <div class="icon">
              <i class="ion-ios-calendar" style="color:white"></i>
            </div>
          </div>
        </div>



   
</div>








@endsection
