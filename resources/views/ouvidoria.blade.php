﻿<title>Portal UNIRB | Ouvidoria </title>

@include('partials._head')

<body>
@include('partials._header')

<script>
		function envio()
		{
			
		var email = document.getElementById("email1").value;
		var assunto= document.getElementById("assunto").value;
    var mensagem = document.getElementById("mensagem").value;
	
	
	
			if(email,assunto,mensagem == ""){
		  swal("Erro ao Enviar!", "Por Favor preencha os campos corretamente", "error");
	
	
		}else{
		swal("Enviado com sucesso !", "Obrigado! Entraremos em contato", "success");
		 // Salva a lista alterada
		}
			
		}
		</script>



	<!-- banner -->

	<div id="carouselExampleControls" class="carousel slide pose-slide2" data-ride="carousel" >
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="images/home/banner2.jpg" style="width:100%" class="tam-img1 img-responsive" />
    </div> 
  </div>
    
     <!-- indice -->
    

	<div class="home1" >
		<div class="breadcrumbs_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="breadcrumbs">
							<ul>
								<li><a href="/">Home</a></li>
								<li>Ouvidoria</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>			
    </div>
    <br>

    <center><div class="col-md-12"><p class="text-justify txt18" style="color:black">A <b style="color:#1e426e">OUVIDORIA</b> é um canal de comunicação criado para registrar sugestão,
reclamação, elogio ou denúncia sobre os serviços prestados pela UNIRB.
O nosso objetivo é atender e garantir a satisfação de nossos alunos e
clientes na prevenção e solução e conflitos

Preencha o formulário abaixo e a nossa equipe faz análise do caso
e envia resposta ao cliente o mais breve possível.</p></div><center>
<br><br>


    <div id="login" style="background-color:#1e426e" >
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12"><br>
                    <h3 class="text-center txt18 "><b style="color:black">Preencha o Formulário abaixo</b></h3><br>
                 
                    {!! Form:: open(['route'=>'mail.store2', 'method' => 'POST']) !!}       
                            <div class="form-group" >
    <label for="exampleFormControlSelect1" style="color:black"><b class="txt15">Selecione o tipo de manifestação</b></label>
    <select class="form-control" id="manifestacao" name="manifestacao">
      <option value="Elogio">Elogio</option>
      <option value="Sugestão">Sugestão</option>
      <option value="Reclamação">Reclamação</option>
      <option value="Dúvida">Dúvida</option>
    </select>
  </div>

  <div class="form-group">
    <input type="email" class="form-control" id="email1" name="email1" placeholder="Email" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="nome1" name="nome1" placeholder="Nome Completo" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF" required>
  </div>

 <div class="form-group">
    <label for="exampleFormControlSelect1" style="color:black"><b class="txt15"> Selecione a Unidade</b></label>
    <select class="form-control" id="polo" name="polo">
      <option value="Salvador">Salvador</option>
      <option value="Castro Alves"> Castro Alves</option>
      <option value="Unique"> Unique</option>
      <option value="Orixás Center">Orixas Center</option>
      <option value="Alagoinhas">Alagoinhas</option>
      <option value="Feira de Santana">Feira de Santana</option>
      <option value="Barreiras">Barreiras</option>
      <option value="Parnaíba">Parnaiba</option>
      <option value="Aracaju">Aracaju	</option>
      <option value="Arapiraca">Arapiraca</option>
      <option value="Maceió">Maceio</option>
      <option value="Mossoró">Mossoro</option>
      <option value="Natal">Natal</option>
      <option value="Teresina">Teresina</option>
      <option value="Camaçari">Camaçari</option>
      <option value="Juazeiro">Juazeiro</option>
      <option value="Fortaleza"> Fortaleza</option>
      <option value="Serrinha">Serrinha</option>

     
    </select>
  </div>

  <div class="form-group">

    <input type="text" class="form-control" id="assunto" name="assunto" placeholder="Assunto" required>
  </div>

  <div class="form-group">
    <textarea class="form-control" id="mensagem" name="mensagem" rows="3" placeholder="O que deseja relatar ?" required></textarea>
  </div>



 <div class="form-group">
<button class="btn btn-primary"  onclick="envio()"  ><b>Enviar</b></button>                     
  </div>
                           
  {!!Form:: close()!!}                  
   </div>
   <br><br><br>
                </div>
            </div>
        </div>
    </div>
    
    </div>

	
@include('partials._footer')

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>




</body>
</html>