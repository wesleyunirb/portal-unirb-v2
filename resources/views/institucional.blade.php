﻿<title>Portal UNIRB | Institucional</title>

@include('partials._head')
@include('partials._header')

<script>
$(document).ready(function(){
        $(".valor").on("input", function(){
            var textoDigitado = $(this).val();
            var inputCusto = $(this).attr("custo");
            $("#"+ inputCusto).val(textoDigitado);
        });
    });
</script>



<body>



  
   <!-- banner -->

<div id="carouselExampleControls" class="carousel slide pose-slide1" data-ride="carousel" >
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="images/institucional/banner1.png" style="width:100%" class="tam-img1 img-responsive" />
    </div> 
  </div>

     <!-- indice -->
	<div class="home1">
		<div class="breadcrumbs_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="breadcrumbs">
							<ul>
								<li><a href="/">Home</a></li>
								<li>Institucional</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>			
    </div>


	<!-- Feature -->

	<div class="feature1 pose-unirb">
		<div class="feature_background" ></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 ><b style="color:#1f466f" class="txt50">A UNIRB</b></h2>
						<div class="section_subtitle"><p style="color:black" class="text-justify txt15">O Centro Universitário UNIRB caracteriza-se pela excelência no ensino superior sendo referência regional em cursos de graduação, pós graduação e corpo docente altamente qualificado.</p></div>
					</div>
				</div>
      </div>
      <br><br><br>
			<div class="row feature_row">

				<!-- Feature Content -->
				<div class="col-lg-6 feature_col">
					<div class="feature_content">
						<!-- Accordions -->
						<div class="accordions">
							
							<div class="elements_accordions">

								<div class="accordion_container">
									<div class="accordion d-flex flex-row align-items-center active"><div>Quem Somos</div></div>
									<div class="accordion_panel">
										<p style="color:black" class="text-justify txt15">Fundado em 2002, o Centro Universitário Regional da Bahia – UNIRB conta com 23 unidades de ensino distribuídas entre os estados de Alagoas, Bahia, Ceará, Piauí, Rio Grande do Norte e Sergipe. Com o objetivo de formar cidadãos responsáveis através de ações educativas, a UNIRB se destaca por ser genuinamente brasileira e inovadora, ofertando em seu portfólio mais de 200 opções de curso modalidade presencial e 26 cursos modalidade EAD, nas áreas de humanas, saúde e exatas.</p>
									</div>
								</div>

								<div class="accordion_container">
									<div class="accordion d-flex flex-row align-items-center "><div>Missão</div></div>
									<div class="accordion_panel">
										<p style="color:black" class="text-justify txt15">“Promoção da excelência da educação, contribuindo para a construção de um mundo igualitário,fraterno e libertário, dentro dos princípios sagrados da moral, da ética e da estética.” </p>
									</div>
								</div>

								<div class="accordion_container">
									<div class="accordion d-flex flex-row align-items-center"><div>Visão</div></div>
									<div class="accordion_panel">
										<p  style="color:black" class="text-justify txt3">“Ser reconhecido como um centro de conhecimento científico moderno, arrojado e inovador, formando profissionais multi-especialistas, com visão sistêmica de administração e compreensão do conjunto de atributos que o mercado de trabalho exige, estando hábil a contribuir para com o desenvolvimento da Bahia e do Brasil, tendo como referência de trabalho a excelência do seu projeto pedagógico, as suas práticas administrativas e a qualidade, organização e resultado de todos os serviços prestados.” </p>
									</div>
								</div>

								<div class="accordion_container">
									<div class="accordion d-flex flex-row align-items-center"><div>Valores</div></div>
									<div class="accordion_panel">
										<p style="color:black"  class="text-justify txt15">“Excelência – Ética – Valorização de ser Humano – Competência – Compromisso – Honestidade.” </p>
									</div>
								</div>

								<div class="accordion_container">
									<div class="accordion d-flex flex-row align-items-center"><div>Dirigentes</div></div>
									<div class="accordion_panel">
										<p style="color:black" class="text-justify">Reitor: Carlos Joel Pereira</p>
                    <p style="color:black" class="text-justify">Pró-reitora Administrativo Financeiro: Ailda de Almeida Souza Pereira</p>
									</div>
								</div>

							</div>

						</div>
						<!-- Accordions End -->
					</div>
				</div>

        <!-- Feature Video -->
        <video style="background:black" controls="" preload="none" poster="" width="426" height="280">
        <source src="video/unirb.mp4" type="video/mp4"> 
        <source src="https://s3.amazonaws.com/nandovieira/media/tag-video/video.webm" type="video/webm"> 
       </video>
			
			</div>
		</div>
	</div>

  <div style="background-color:#1e426e"> 

	<div class="section_title_container text-center pose-unidades"><br><br>
  
						<h2 ><b style="color:white" class="txt50 ">Nossas Unidades</b></h2>
						<div class="section_subtitle"><p style="color:white; font-size:16px;" class="text-justify txt15 text-center" >Presente em 15 cidades distribuídas por todo Nordeste do Brasil</p></div>
					</div>
          <br><br>
          <!-- Team -->

	<div class="team "  style="background-color:#1e426e">
		<div class="container pose-txt" >


    <div class="row team_row"  style="margin-top:-5%">
				
				<!-- Team Item SALVADOR -->
				<div class="col-lg-3 col-md-6 team_col" >
					<div class="team_item" >
           <div class="team_image"><img src="images/institucional/sallvador.png" alt="salvador" ></div>
            <div class="team_body" style="background-color:#F5FFFA">
           
            <div class="">
  <div class="card-body">
    <p style="color:black"><b>Centro Universitário - UNIRB | Centro Universitário Regional do Brasil</b></p>
    <p style="color:black"> <b>Endereço:</b> Rua Tamburugy 474, Patamares. - Salvador/BA.</p>
    <p style="color:black"><b>Telefone:</b> (71) 3368-8300</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select name="unidade_id" class="form-control">
    @foreach ($Csalvador as $salvador)
    <option name="Csalvador"  id='evento' class='valor' name='evento' custo='custo1'class='valor' value="{{ $salvador->id }}" selected="selected">{{ $salvador->nome }}</option>
    @endforeach

  </select>  
  <br>
  <div>
  
  </div>
    </div>
</div>
              

      					</div>
						
					</div>
				</div>

 

				<!-- Team Item ALAGOINHAS -->
				<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/alagoinhas.png" alt="" ></div>
          <div class="team_body" style="background-color:#F5FFFA">
          <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB | Faculdade Regional de Alagoinhas</b></p>
    <p style="color:black"> <b>Endereço:</b> Rua Altino Ribeiro Rocha, 100/ Alagoinhas Velha. - Alagoinhas/BA.</p>
    <p style="color:black"><b>Telefone:</b> (75) 3422-8900</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div>
      					</div>
					</div>
				</div>

        

				<!-- Team Item CAMACARI -->
				<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/camacari.png" alt="" ></div>
            <div class="team_body"  style="background-color:#F0F8FF">

            <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB CAMAÇARI</b></p>
    <p style="color:black"> <b>Endereço:</b> Loteamento Jardim Limoeiro, S/Nº, Camaçari/ BA – CEP: 42.802-580</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div> 
						</div>
					</div>
				</div>

				<!-- Team Item FEIRA DE SANTANA-->
				<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/feirasantana.png" alt="" ></div>
            <div class="team_body"  style="background-color:#F0F8FF">

            <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB | FARB - Faculdade Regional da Bahia</b></p>
    <p style="color:black"> <b>Endereço:</b> Avenida Deputado Luis Eduardo Magalhães N. 9899-Subaé Br 324 - Feira de Santana/BA</p>
    <p style="color:black"><b>Telefone:</b> (75) 3616-7493</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div> 

						</div>
					</div>
				</div>

        

				<!-- Team Item BARREIRAS -->
				<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/barreiras.png" alt="" ></div>
                <div class="team_body" style="background-color:#F5FFFA">
                <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB | Faculdade João Calvino </b></p>
    <p style="color:black"> <b>Endereço:</b> Avenida Cleriston Andrade 3507 Br 242 - Loteamento Vila Nova. - Barreiras/BA.</p>
    <p style="color:black"><b>Telefone:</b> (77) 3612-2908</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div> 
      					</div>
						
					</div>
				</div>

				<!-- Team Item PARNAIBA-->
				<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/parnaiba.png" alt="" ></div>
            <div class="team_body"  style="background-color:#F0F8FF">
            <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB | FARB Faculdade Regional Brasileira</b></p>
    <p style="color:black"> <b>Endereço:</b>Rua Esperanza Fontenele De Carvalho Nº 51 - Bairro: Frei Higino - Parnaiba/PI.</p>
    <p style="color:black"><b>Telefone:</b>(86) 3323-4000</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div> 
						</div>
					</div>
				</div>
                
                <!-- Team Item ARACAJU -->
				<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/aracaju.png" alt="" ></div>
            <div class="team_body" style="background-color:#F0F8FF;">
            <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB | Faculdade UNIRB Aracaju</b></p>
    <p style="color:black"> <b>Endereço:</b>Rua Avenida Marechal Candido Mariano Da Silva Rondon S/N - Bairro: Jabotiana. - Aracaju/SE.</p>
    <p style="color:black"><b>Telefone:</b>(79) 3259-4025</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div> 
		</div>
	</div>
   </div>
                
                <!-- Team Item ARAPIRACA-->
				<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/arapiraca.png" alt="" ></div>
            <div class="team_body" style="background-color:#F0F8FF;">
            <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB | FARB - Faculdade Regional Brasileira</b></p>
    <p style="color:black"> <b>Endereço:</b>Rodovia AL 220, s/nº - Bairro: Senador Arnon De Melo Arapiraca/AL</p>
    <p style="color:black"><b>Telefone:</b>(82) 3539-8913</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div> 
		</div>
		  </div>
      </div>
                  <!-- Team Item MACEIO -->
				<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/maceio.png" alt="" ></div>
            <div class="team_body" style="background-color:#F0F8FF;">
            <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB | FARB - Faculdade Regional Brasileira </b></p>
    <p style="color:black"> <b>Endereço:</b>Av. Menino Marcelo, 1600, Antares - Maceió/AL.</p>
    <p style="color:black"><b>Telefone:</b>(82) 3221-5636</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div> 
							
						</div>
					</div>
                </div>
                

                <!-- Team Item MOSSORO -->
				<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/mossoro.png" alt="" ></div>
            <div class="team_body" style="background-color:#F0F8FF;">
            <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB | Faculdade de Ciências e Tecnologia Mater Christi </b></p>
    <p style="color:black"> <b>Endereço:</b>Mossoro/RN.</p>
    <p style="color:black"><b>Telefone:</b>(84) 3314-3321</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div> 
							
						</div>
					</div>
                </div>
                
                <!-- Team Item TERESINA -->
				<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/teresina.png" alt="" ></div>
            <div class="team_body" style="background-color:#F0F8FF;">
            <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB | Faculdade UNIRB - Piauí</b></p>
    <p style="color:black"> <b>Endereço:</b>Avenida MIrtes Melão Nº 700, Gurupi - TERESINA/PI</p>
    <p style="color:black"><b>Telefone:</b>(86) 3304-3003</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div> 
 </div>
 </div>
 </div>
                  <!-- Team Item Serrinha -->
				<!--<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/serrinha.png" alt="" ></div>
            <div class="team_body" style="background-color:#F0F8FF;">
            <div class="">
  <div class="card-body">
    <p style="color:black"><b>falta</b></p>
    <p style="color:black"> <b>Endreço:</b>Avenida Deputado Luis Eduardo Magalhães N. 9899-Subaé Br 324 - Feira de Santana/BA</p>
    <p style="color:black"><b>Telefone:</b>(75) 3616-7493</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Escolha o curso</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div> 	
		</div>
		</div>
     </div>-->

                            <!-- Team Item JUAZEIRO -->
				<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/juazeiro.png" alt="" ></div>
            <div class="team_body" style="background-color:#F0F8FF;">
            <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB JUAZEIRO</b></p>
    <p style="color:black"> <b>Endreço:</b> Juá Garden Shopping, Rodovia BR – 407, nº 5318, Km 05, Distrito Industrial, CEP: 48.909-901, Juazeiro – BA</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div> 
	</div>
	</div>
   </div>

       <!-- Team Item NATAL -->
       <div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/natal.png" alt="" ></div>
            <div class="team_body" style="background-color:#F0F8FF;">
            <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB NATAL</b></p>
    <p style="color:black"> <b>Endreço:</b>Av. Salgado Filho, 2810- Lagoa Nova/ Natal RN, CEP: 59.075-000</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>
  </div>
</div> 
	</div>
	</div>
   </div>
                

                <!-- Team Item FORTELEZA -->
				<div class="col-lg-3 col-md-6 team_col">
					<div class="team_item">
          <div class="team_image"><img src="images/institucional/fortaleza.png" alt="" ></div>
            <div class="team_body" style="background-color:#F0F8FF;">
            <div class="">
  <div class="card-body">
    <p style="color:black"><b>UNIRB CEARA, UNIRB FORTALEZA E FAMIL</b></p>
    <p style="color:black"> <b>Endreço:</b>endereço: Avenida João Pessoa, nº 3884, Damas, Fortaleza - CE, CEP: 60.425-812.</p>
    <br>
    <label for="exampleFormControlSelect1" style="color:black"><b>Cursos Ofertados</b></label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>curso</option>
     
    </select>  </div>
</div> 
	</div>
	</div>
   </div>
       
			</div>
		</div>
    </div>
    </div>


<section id="cursoinfo">
<div class="card" >
@foreach ($infoCSalvador as $curso)
  <div class="card-header">
  <h5 class="modal-title" style="font-size:25px"> Curso: {{ $curso->nome }}</h5>
  </div>
  <div class="card-body">
  <p class="card-text"><b style="color:#384158;font-size:15px">Portaria do MEC:</b> <span style="color:black">{{ $curso->portaria }}</span> </p>
  <p class="card-text"><b style="color:#384158;font-size:15px">Vagas:</b> <span style="color:black">{{ $curso->qtd_vagas }}</span> </p>
  <p class="card-text"><b style="color:#384158;font-size:15px">Turno:</b> <span style="color:black">{{ $curso->turno }}</span> </p>
  <p class="card-text"><b style="color:#384158;font-size:15px">Duração:</b> <span style="color:black">{{ $curso->duracao }}</span> </p>
  <p class="card-text"><b style="color:#384158;font-size:15px">Mensalidade:</b> <span style="color:black">{{ $curso->mensalidade }}</span> </p>
  <a href="{{ $curso->matriz_curricular }}" class="btn btn-primary" target="_blank">Matriz Curricular</a><br><br>

      <p class="text-justify"><b style="color:#384158;font-size:18px">Apresentação:</b><span style="color:black"> {{ $curso->descricao }}</span>
      @endforeach
  </div>
</div>
</section>
    


@include('partials._footer')	
</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>




</body>
</html>