@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Inscritos
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($inscritos, ['route' => ['inscritos.update', $inscritos->id], 'method' => 'patch']) !!}

                        @include('inscritos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection