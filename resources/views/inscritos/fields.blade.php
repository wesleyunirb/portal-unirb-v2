<!-- Nome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nome', 'Nome:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<!-- Cpf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cpf', 'Cpf:') !!}
    {!! Form::text('cpf', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefone', 'Telefone:') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- evento Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('evento_id', 'Selecione a Unidade:') !!}
    <select name="evento_id" class="form-control">
    <option >Evento</option>

    @foreach ($eventos as $event)
    <option name="eventos" value="{{ $event->id }}" >{{ $event->titulo }}</option>
    @endforeach
  </select>
</div>

<!-- Unidade Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('unidade_id', 'Selecione a Unidade:') !!}
    <select name="unidade_id" class="form-control">
    <option >Unidade</option>

    @foreach ($unidades as $uni)
    <option name="unidades" value="{{ $uni->id }}" >{{ $uni->nome }}</option>
    @endforeach
  </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('inscritos.index') }}" class="btn btn-danger">Cancelar</a>
</div>
