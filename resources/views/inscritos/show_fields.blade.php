<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{{ $inscritos->nome }}</p>
</div>

<!-- Cpf Field -->
<div class="form-group">
    {!! Form::label('cpf', 'Cpf:') !!}
    <p>{{ $inscritos->cpf }}</p>
</div>

<!-- Telefone Field -->
<div class="form-group">
    {!! Form::label('telefone', 'Telefone:') !!}
    <p>{{ $inscritos->telefone }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $inscritos->email }}</p>
</div>

<!-- Evento Id Field -->
<div class="form-group">
    {!! Form::label('evento_id', 'Evento Id:') !!}
    <p>{{ $inscritos->evento_id }}</p>
</div>

<!-- Unidade Id Field -->
<div class="form-group">
    {!! Form::label('unidade_id', 'Unidade Id:') !!}
    <p>{{ $inscritos->unidade_id }}</p>
</div>

