<div class="table-responsive">
    <table class="table" id="inscritos-table">
        <thead>
            <tr>
                <th>Nome</th>
        <th>Cpf</th>
        <th>Telefone</th>
        <th>Email</th>
        <th>Evento </th>
        <th>Unidade </th>
                <th colspan="3">Ações</th>
            </tr>
        </thead>
        <tbody>
        @foreach($inscritos as $inscritos)
            <tr>
                <td>{{ $inscritos->nome }}</td>
            <td>{{ $inscritos->cpf }}</td>
            <td>{{ $inscritos->telefone }}</td>
            <td>{{ $inscritos->email }}</td>
            <td>{{ $inscritos->evento_id }}</td>
            <td>{{ $inscritos->unidade_id }}</td>
                <td>
                    {!! Form::open(['route' => ['inscritos.destroy', $inscritos->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('inscritos.show', [$inscritos->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('inscritos.edit', [$inscritos->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
