<!-- Tipo Eventos Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_eventos_id', 'Selecione o tipo do evento:') !!}
    <select name="tipo_eventos_id" class="form-control">
    <option > Tipo de evento</option>

    @foreach ($tipoeventos as $tipoe)
    <option name="tipoeventos" value="{{ $tipoe->id }}" >{{ $tipoe->tipo }}</option>
    @endforeach
  </select>
  </div>

<!-- Unidade Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('unidade_id', 'Selecione a Unidade:') !!}
    <select name="unidade_id" class="form-control">
    <option >Unidade</option>

    @foreach ($unidades as $uni)
    <option name="unidades" value="{{ $uni->id }}" >{{ $uni->nome }}</option>
    @endforeach
  </select>
</div>

<!-- Titulo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('titulo', 'Titulo:') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<!-- Local Field -->
<div class="form-group col-sm-6">
    {!! Form::label('local', 'Local:') !!}
    {!! Form::text('local', null, ['class' => 'form-control']) !!}
</div>

<!-- Descricao Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descricao', 'Descricao:') !!}
    {!! Form::text('descricao', null, ['class' => 'form-control']) !!}
</div>

<!-- Data Evento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data_evento', 'Data Evento:') !!}
    {!! Form::text('data_evento', null, ['class' => 'form-control']) !!}
</div>

<!-- Horario Field -->
<div class="form-group col-sm-6">
    {!! Form::label('horario', 'Horario:') !!}
    {!! Form::text('horario', null, ['class' => 'form-control']) !!}
</div>

<!-- Limite Participantes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('limite_participantes', 'Limite Participantes:') !!}
    {!! Form::text('limite_participantes', null, ['class' => 'form-control']) !!}
</div>

<!-- Data Inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data_inicio', 'Data Inicio:') !!}
    {!! Form::date('data_inicio', null, ['class' => 'form-control','id'=>'data_inicio']) !!}
</div>


<!-- Data Fim Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data_fim', 'Data Fim:') !!}
    {!! Form::date('data_fim', null, ['class' => 'form-control','id'=>'data_fim']) !!}
</div>


<!-- Logomarca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logomarca', 'Logomarca:') !!}
    {!! Form::text('logomarca', null, ['class' => 'form-control']) !!}
</div>

<!-- Programacao Field -->
<div class="form-group col-sm-6">
    {!! Form::label('programacao', 'Programacao:') !!}
    {!! Form::text('programacao', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', 0) !!}
        {!! Form::checkbox('status', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('eventos.index') }}" class="btn btn-default">Cancel</a>
</div>
