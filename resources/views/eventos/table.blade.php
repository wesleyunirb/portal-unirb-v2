<div class="table-responsive">
    <table class="table" id="eventos-table">
        <thead>
            <tr>
                <th>Tipo Eventos Id</th>
        <th>Unidade Id</th>
        <th>Titulo</th>
        <th>Local</th>
        <th>Descricao</th>
        <th>Data Evento</th>
        <th>Horario</th>
        <th>Limite Participantes</th>
        <th>Data Inicio</th>
        <th>Data Fim</th>
        <th>Logomarca</th>
        <th>Programacao</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($eventos as $eventos)
            <tr>
                <td>{{ $eventos->tipo_eventos_id }}</td>
            <td>{{ $eventos->unidade_id }}</td>
            <td>{{ $eventos->titulo }}</td>
            <td>{{ $eventos->local }}</td>
            <td>{{ $eventos->descricao }}</td>
            <td>{{ $eventos->data_evento }}</td>
            <td>{{ $eventos->horario }}</td>
            <td>{{ $eventos->limite_participantes }}</td>
            <td>{{ $eventos->data_inicio }}</td>
            <td>{{ $eventos->data_fim }}</td>
            <td>{{ $eventos->logomarca }}</td>
            <td>{{ $eventos->programacao }}</td>
            <td>{{ $eventos->status }}</td>
                <td>
                    {!! Form::open(['route' => ['eventos.destroy', $eventos->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('eventos.show', [$eventos->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('eventos.edit', [$eventos->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
