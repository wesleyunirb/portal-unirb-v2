<!-- Tipo Eventos Id Field -->
<div class="form-group">
    {!! Form::label('tipo_eventos_id', 'Tipo Eventos Id:') !!}
    <p>{{ $eventos->tipo_eventos_id }}</p>
</div>

<!-- Unidade Id Field -->
<div class="form-group">
    {!! Form::label('unidade_id', 'Unidade Id:') !!}
    <p>{{ $eventos->unidade_id }}</p>
</div>

<!-- Titulo Field -->
<div class="form-group">
    {!! Form::label('titulo', 'Titulo:') !!}
    <p>{{ $eventos->titulo }}</p>
</div>

<!-- Local Field -->
<div class="form-group">
    {!! Form::label('local', 'Local:') !!}
    <p>{{ $eventos->local }}</p>
</div>

<!-- Descricao Field -->
<div class="form-group">
    {!! Form::label('descricao', 'Descricao:') !!}
    <p>{{ $eventos->descricao }}</p>
</div>

<!-- Data Evento Field -->
<div class="form-group">
    {!! Form::label('data_evento', 'Data Evento:') !!}
    <p>{{ $eventos->data_evento }}</p>
</div>

<!-- Horario Field -->
<div class="form-group">
    {!! Form::label('horario', 'Horario:') !!}
    <p>{{ $eventos->horario }}</p>
</div>

<!-- Limite Participantes Field -->
<div class="form-group">
    {!! Form::label('limite_participantes', 'Limite Participantes:') !!}
    <p>{{ $eventos->limite_participantes }}</p>
</div>

<!-- Data Inicio Field -->
<div class="form-group">
    {!! Form::label('data_inicio', 'Data Inicio:') !!}
    <p>{{ $eventos->data_inicio }}</p>
</div>

<!-- Data Fim Field -->
<div class="form-group">
    {!! Form::label('data_fim', 'Data Fim:') !!}
    <p>{{ $eventos->data_fim }}</p>
</div>

<!-- Logomarca Field -->
<div class="form-group">
    {!! Form::label('logomarca', 'Logomarca:') !!}
    <p>{{ $eventos->logomarca }}</p>
</div>

<!-- Programacao Field -->
<div class="form-group">
    {!! Form::label('programacao', 'Programacao:') !!}
    <p>{{ $eventos->programacao }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $eventos->status }}</p>
</div>

