﻿@extends('layouts.app')

@section('content')


<div class="form-group ">
    <a href="{{ route('leads.index') }}" class="btn btn-primary">Voltar</a>
</div>


<div class="container" >

<!-- Submit Field -->

	<div class="row">
        <div class="span12">
    		<div class="" id="loginModal">
              <div class="modal-body">
                <div class="well">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#login" data-toggle="tab">Pesquisar Leads</a></li>
                    <li><a href="#create" data-toggle="tab">Relatório PDF</a></li>
                  </ul>

                 <!--FORM PESQUISA -->
                  <div id="myTabContent" class="tab-content">
                    <div class="tab-pane active in" id="login">
                    {!! Form:: open(['route'=>'relatorio.relatoriolead', 'method' => 'POST']) !!}   

                          <div class="control-group col-md-4" style="margin-top:3%">
                            <!-- Pesquisa -->
                            <label style="color:black"><b>Selecione a Campanha</b></label>
                            <div class="controls">
                        <select name="campanha" id="campanha" class="form-control">
                           <option >Campanha</option>
                           @foreach ($campanha as $camp)
                          <option name="campanha"  id="campanha"value="{{ $camp->id }}" >{{ $camp->nome }}</option>
                          @endforeach
                        </select>                            
                    </div>
                    <div style="margin-left:100%;margin-top:-10%">
                    <button type="submit" class="btn btn-primary">Pesquisar</button>
                    </div>
                    </div>
     
                   <div class="control-group">
                   <!-- table-->
                 <table style="margin-top:12%">
                <div class="table-responsive">
                <table class="table" id="leads-table" style="background-color:white">
                <thead>
                  <tr>
                    <th>Cpf</th>
                    <th>Nome</th>
                    <th>Telefone</th>
                    <th>Email</th>
                    </tr>
                    </thead>
                   <tbody>
                   @foreach($leads as $leads)
                  <tr>
                    <td>{!! $leads->cpf !!}</td>
                    <td>{!! $leads->nome !!}</td>
                    <td>{!! $leads->telefone !!}</td>
                    <td>{!! $leads->email !!}</td>    
                 </tr>
                @endforeach
                 
        </tbody>
    </table>  
    {!!Form:: close()!!}
                      
 </div>
 </div>

    <!--FORM PDF -->
     <div class="tab-pane fade" id="create">
     {!! Form:: open(['route'=>'relatorio.relatoriopdfleads', 'method' => 'POST']) !!}   

     <p style="color:black"><b>Selecione a Campanha</b></p>
     <select name="campanhaid" id="campanhaid" class="form-control">
     <option >Campanha</option>
     @foreach ($campanha as $camp)
    <option name="campanhaid"  id="campanhaid"value="{{ $camp->id }}" >{{ $camp->nome }}</option>
     @endforeach
    </select>   
  <button type="submit"  class="btn btn-primary"><a  style="color:white">Gerar relatorio</a></button>
  {!!Form:: close()!!}

                    </div>
                </div>
              </div>
            </div>
        </div>
	</div>
</div>

</div>


@endsection