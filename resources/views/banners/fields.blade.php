
<!-- Unidade Id Field -->
<div class="form-group col-sm-6">
{!! Form::label('unidade_id', 'Selecione a Unidade:') !!}
    <select name="unidade_id" class="form-control">
    <option name="unidades" >Unidade</option>
    @foreach ($unidades as $uni)
    <option name="unidades" value="{{ $uni->id }}">{{ $uni->nome }}</option>
    @endforeach
  </select>
</div>


<!-- Img Field -->
<div class="form-group col-sm-6">
    {!! Form::label('img', 'Img:') !!}
    {!! Form::text('img', null, ['class' => 'form-control']) !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('banners.index') !!}" class="btn btn-danger">Cancelar</a>
</div>
