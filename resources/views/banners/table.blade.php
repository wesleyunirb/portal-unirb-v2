<div class="table-responsive">
    <table class="table" id="banners-table">
        <thead>
            <tr>
            <th>Unidade Id</th>
                <th>Imagem</th>
        
                <th colspan="3">Ações</th>
            </tr>
        </thead>
        <tbody>
        @foreach($banners as $banners)
            <tr>
            <td>{!! $banners->unidade_id !!}</td>
                <td>{!! $banners->img !!}</td>
            
                <td>
                    {!! Form::open(['route' => ['banners.destroy', $banners->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!--<a href="{!! route('banners.show', [$banners->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                        <a href="{!! route('banners.edit', [$banners->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        <!--{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}-->
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
