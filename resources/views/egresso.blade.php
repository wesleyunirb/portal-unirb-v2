﻿<title>Portal UNIRB | Crédito Estudantil</title>

@include('partials._head')

<body>

@include('partials._header')

<!-- Modal Egressos -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="font-size:20px">Egressos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form>
  <div class="form-group">
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nome Compelto">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Telefone">
  </div>

  <div class="form-group">
    <input type="email" class="form-control" id="exampleInputPassword1" placeholder="Email">
  </div>
 
  <button type="submit" class="btn btn-primary">Enviar</button>
</form>
      </div>
     
    </div>
  </div>
</div>

<!-- Modal Estudantes-->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="font-size:20px">Estudantes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form>
  <div class="form-group">
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nome Compelto">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Telefone">
  </div>

  <div class="form-group">
    <input type="email" class="form-control" id="exampleInputPassword1" placeholder="Email">
  </div>
 
  <button type="submit" class="btn btn-primary">Enviar</button>
</form>
      </div>
      
    </div>
  </div>
</div>

<!-- card egresso e estudantes -->

<!-- Jumbotron -->
<div class="jumbotron p-0">

  <!-- Card image -->
  <div class="view overlay rounded-top" style="margin-top:3%">
    <img src="images/egressos/portalegresso.png" style="width:100%" class="card-img-top img-responsive" class="img-fluid" alt="Sample image">
    <a href="#">
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>

  <!-- Card content -->
  <div class="card-body text-center mb-3">
    <!-- Title -->
    <h3 class="card-title h3 my-4" style="color:black"><strong>Bem-vindo Egresso </strong></h3>
    <!-- Text -->
    
    <p class="card-text py-2" style="color:black">Temos como missão oferecer aos egressos, condições e oportunidades para promover o desenvolvimento de sua carreira, para que encontrem as melhores oportunidades ou para ampliar o seu próprio
    negócio. </p>

    <p class="card-text py-2 text-justify"  style="color:black">O Centro Universitário UNIRB com intuito de reconhecer e apoiar a trajetória profissional dos seus alunos e ex-alunos, criou um setor destinado ao desenvolvimento do estudante e do egresso, para auxiliá-lo na inserção no mercado de trabalho e para networking. </p>
  </div>
</div>


<!-- card egresso e estudantes -->
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title" style="font-size:25px">Egressos</h5>
        <p class="card-text" style="color:black">Canal de comunicação entre profissionais da instituição</p><br>
        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Cadastre-se</a>
      </div>
    </div>
  </div>
  
</div>







<!--footer-->	
@include('partials._footer')

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>


<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="js/courses.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="js/courses.js"></script>


<script src="js/blog.js"></script>
<script src="plugins/masonry/masonry.js"></script>
<script src="plugins/video-js/video.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>


<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/about.js"></script>




</body>
</html>
