<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Leads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cpf')->unique();          
            $table->string('nome');          
            $table->string('telefone');          
            $table->string('email')->unique();          
            $table->bigInteger('campanha_id')->nullable(); 
            $table->foreign('campanha_id')->references('id')->on('campanha');
            $table->bigInteger('unidade_id')->nullable(); 
            $table->foreign('unidade_id')->references('id')->on('unidades');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
