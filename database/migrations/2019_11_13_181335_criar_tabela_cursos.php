<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CriarTabelaCursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('unidade_id')->unsigned();          
            $table->bigInteger('tipo_cursos_id')->unsigned();          
            $table->string('nome',50);
            $table->string('descricao',3000);
            $table->string('portaria',50);
            $table->string('duracao',50);
            $table->string('turno',50);
            $table->string('mensalidade',50);
            $table->string('matriz_curricular');
            $table->string('qtd_vagas');
            $table->boolean('status');
            $table->foreign('unidade_id')->references('id')->on('unidades');
            $table->foreign('tipo_cursos_id')->references('id')->on('tipo_cursos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curso');
    }
}
