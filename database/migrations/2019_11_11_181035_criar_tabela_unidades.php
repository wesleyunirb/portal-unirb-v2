<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CriarTabelaUnidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('calendario');
            $table->string('portal_cademico');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('logradouro');
            $table->string('estado');
            $table->string('cidade');
            $table->string('bairro');
            $table->string('cep');
            $table->string('logomarca');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidades');
    }
}
