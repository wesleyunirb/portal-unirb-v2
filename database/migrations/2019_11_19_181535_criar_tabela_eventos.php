<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CriarTabelaEventos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tipo_eventos_id')->unsigned();          
            $table->bigInteger('unidade_id')->unsigned();          
            $table->string('titulo');
            $table->string('local');
            $table->string('descricao');
            $table->string('data_evento');
            $table->string('horario');

            $table->string('limite_participantes');
            $table->date('data_inicio');
            $table->date('data_fim');
            $table->string('logomarca');
            $table->string('programacao');
            $table->boolean('status');

            $table->foreign('tipo_eventos_id')->references('id')->on('tipo_eventos');
            $table->foreign('unidade_id')->references('id')->on('unidades');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
