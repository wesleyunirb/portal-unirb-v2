<?php

use Illuminate\Database\Seeder;
use App\Unidade;


class UnidadeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unidade::create([
            'nome'      => 'home',
            'calendario'     => 'home',
            'portal_cademico' =>'home',
           'latitude' =>'home',
           'longitude' =>'-12.9590886',
           'logradouro' =>'-38.401274',
            'estado' =>'home',
            'cidade' =>'home',
            'bairro' =>'home',
            'cep' =>'home',
           'logomarca' =>'home'
            
        ]);

        Unidade::create([
            'nome'      => 'Salvador',
            'calendario'     => 'Salvador',
            'portal_cademico' =>'Salvador',
           'latitude' =>'-12.9590886',
           'longitude' =>'-38.401274',
           'logradouro' =>'Salvador',
            'estado' =>'Salvador',
            'cidade' =>'Salvador',
            'bairro' =>'Salvador',
            'cep' =>'Salvador',
           'logomarca' =>'Salvador'
            
        ]);
    }
}
