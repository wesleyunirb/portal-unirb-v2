<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\leads;
use Faker\Generator as Faker;

$factory->define(leads::class, function (Faker $faker) {

    return [
        'cpf' => $faker->word,
        'nome' => $faker->word,
        'telefone' => $faker->word,
        'email' => $faker->word,
        'campanha_id' => $faker->word,
        'unidade_id' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
