<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\banners;
use Faker\Generator as Faker;

$factory->define(banners::class, function (Faker $faker) {

    return [
        'img' => $faker->word,
        'unidade_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
