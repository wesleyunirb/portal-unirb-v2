<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\curso;
use Faker\Generator as Faker;

$factory->define(curso::class, function (Faker $faker) {

    return [
        'unidade_id' => $faker->word,
        'tipo_cursos_id' => $faker->word,
        'nome' => $faker->word,
        'descricao' => $faker->word,
        'portaria' => $faker->word,
        'duracao' => $faker->word,
        'turno' => $faker->word,
        'mensalidade' => $faker->word,
        'matriz_curricular' => $faker->word,
        'qtd_vagas' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
