<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\noticias;
use Faker\Generator as Faker;

$factory->define(noticias::class, function (Faker $faker) {

    return [
        'unidade_id' => $faker->word,
        'titulo' => $faker->word,
        'subtitulo' => $faker->word,
        'data' => $faker->word,
        'descricao' => $faker->word,
        'image' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
