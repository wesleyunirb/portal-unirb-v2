<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\tipo_cursos;
use Faker\Generator as Faker;

$factory->define(tipo_cursos::class, function (Faker $faker) {

    return [
        'tipo' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
