<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\unidades;
use Faker\Generator as Faker;

$factory->define(unidades::class, function (Faker $faker) {

    return [
        'nome' => $faker->word,
        'calendario' => $faker->word,
        'portal_cademico' => $faker->word,
        'latitude' => $faker->word,
        'longitude' => $faker->word,
        'logradouro' => $faker->word,
        'estado' => $faker->word,
        'cidade' => $faker->word,
        'bairro' => $faker->word,
        'cep' => $faker->word,
        'logomarca' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
