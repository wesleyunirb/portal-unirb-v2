<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\eventos;
use Faker\Generator as Faker;

$factory->define(eventos::class, function (Faker $faker) {

    return [
        'tipo_eventos_id' => $faker->word,
        'unidade_id' => $faker->word,
        'titulo' => $faker->word,
        'local' => $faker->word,
        'descricao' => $faker->word,
        'data_evento' => $faker->word,
        'horario' => $faker->word,
        'limite_participantes' => $faker->word,
        'data_inicio' => $faker->word,
        'data_fim' => $faker->word,
        'logomarca' => $faker->word,
        'programacao' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
