<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\galeria;
use Faker\Generator as Faker;

$factory->define(galeria::class, function (Faker $faker) {

    return [
        'img' => $faker->word,
        'titulo' => $faker->word,
        'unidade_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
