<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\lead;
use Faker\Generator as Faker;

$factory->define(lead::class, function (Faker $faker) {

    return [
        'nome' => $faker->word,
        'cpf' => $faker->word,
        'telefone' => $faker->word,
        'email' => $faker->word,
        'evento_id' => $faker->word,
        'evento_tipo_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
